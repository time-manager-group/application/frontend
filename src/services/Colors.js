export const THEME_COLORS = {
    'primary':         '#3b71ca',
    'light-primary':   '#b8daff',
    'secondary':       '#9fa6b2',
    'light-secondary': '#d6d8db',
    'success':         '#14a44d',
    'light-success':   '#c3e6cb',
    'light-success-2':   'rgb(227 255 224)',
    'info':            '#54b4d3',
    'light-info':      '#bee5eb',
    'warning':         '#e4a11b',
    'light-warning':   '#ffeeba',
    'danger':          '#dc4c64',
    'light-danger':    '#f5c6cb',
    'light':           '#fbfbfb',
    'dark':            '#332d2d',
    'light-dark':      '#c6c8ca'
};