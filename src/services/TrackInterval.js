import TrackStore from "../stores/TrackStore";

export default class TrackInterval {
    static interval;

    static initInterval() {
        if (!this.interval) {
            this.interval = setInterval(() => {
                const items = TrackStore.getInstance().items;
                for (const item of items) {
                    if (item.isActive) {
                        item.dateEnd = new Date();
                    }
                }
            }, 1000);
        }
    }
}
