import TimeDropper from './TimeDropper';
import moment from 'moment';

export function getTrackWithMinimalTime(items) {
    let minTrack = null;
    items.map((item) => {
        if (!minTrack || (getTimeSeconds(item.dateStart) < getTimeSeconds(minTrack.dateStart))) {
            minTrack = item;
        }
    });

    return minTrack;
}

export function getTrackWithMaximalTime(items) {
    let maxTrack = null;
    items.map((item) => {
        if (!maxTrack || (getTimeSeconds(item.dateEnd) > getTimeSeconds(maxTrack.dateEnd))) {
            maxTrack = item;
        }
    });

    return maxTrack;
}

export function typeSumter(items, type) {
    let sumTime = 0;
    items.map((item) => {
        if (item.type === type) {
            sumTime += new TimeDropper(item.dateStart, item.dateEnd).getDiffSeconds();
        }
    });

    return sumTime;
}

/**
 *
 * @param date {Date}
 * @param seconds {number}
 * @return {Date}
 */
export function createDateTimeWithAppendTime(date, seconds) {
    return new Date(date.getTime() + seconds * 1000);
}

/**
 * @param date {Date}
 * @return {number}
 */
export function getTimeSeconds(date) {
    return date.getTime() / 1000;
}

/**
 * @param dateFirst {Date}
 * @param dateSecond {Date}
 * @return {boolean}
 */
export function isSameDay(dateFirst, dateSecond) {
    return moment(dateFirst).format('YYYY.MM.DD') === moment(dateSecond).format('YYYY.MM.DD');
}

/**
 *
 * @param date {Date|number}
 * @param withDate {boolean}
 * @return {string}
 */
export function getFormattedDate(date, withDate = false) {
    const dateString = moment(date).format('YYYY.MM.DD');
    const timeString = moment(date).format('HH:mm:ss');
    if (!withDate) {
        return timeString;
    }

    return `${dateString} ${timeString}`;
}

export async function sleep(ms) {
    await new Promise((resolve) => {
        setTimeout(() => resolve(), ms);
    })
}