import {AxiosInstance, AxiosError} from 'axios';

/** @typedef {function(AxiosError): void} FailCallable */

/**
 * Базовый класс для API с сервером с сервисами.
 */
export class BackendApi {
    /**
     * @protected
     * @type {AxiosInstance}
     */
    axios;
    /**
     * @private
     */
    __BACKEND_SUBDOMAIN = 'back.';


    /**
     * @var {FailCallable[]}
     * @private
     */
    __failCallables;

    /**
     * @param callback {FailCallable}
     * @return void
     */
    addHandleFail(callback) {
        this.__failCallables.push(callback);
    }

    constructor() {
        const tzid   = Intl.DateTimeFormat().resolvedOptions().timeZone;
        // eslint-disable-next-line no-restricted-globals
        const origin = location.origin;

        const host = origin.replace(document.domain, this.__BACKEND_SUBDOMAIN + document.domain);

        this.axios = require('axios').create({
            baseURL: 'http://' + this.__BACKEND_SUBDOMAIN + 'time-manager.ru.172.20.128.2.nip.io'
            //  baseURL: host
        });

        this.axios.interceptors.response.use(
            response => {
                return response;
            },
            error => {
                this.__handleFail(error);
                return Promise.reject(error);
            }
        );

        // noinspection JSUnresolvedVariable
        this.axios.defaults.headers.common['TimeZone'] = tzid;

        this.__failCallables = [];
    }

    /**
     * Генерация рандомного hash id.
     *
     * @param  length {int}
     * @returns {string}
     * @protected
     */
    makeId(length = 5) {
        const result           = [];
        const characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        const charactersLength = characters.length;
        for (let i = 0; i < length; i++) {
            result.push(characters.charAt(Math.floor(Math.random() *
                charactersLength)));
        }
        return result.join('');
    }

    /**
     * @param error {AxiosError}
     * @private
     */
    __handleFail(error) {
        for (const callback of this.__failCallables) {
            callback(error);
        }
    }
}