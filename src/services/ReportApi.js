import {BackendApi} from './BackendApi';


export default class ReportApi extends BackendApi {
    /**
     * @param handleSuccess {$Call}
     * @param handleFail {$Call}
     * @param params {{
     * }}
     * @param payload {{}}
     */
    sendDailyReport(params = {}, payload = {}, handleSuccess = null, handleFail = null) {
        this.axios.post('report/send-daily-report', {}, {params: {}})
            .then(function(response) {
                handleSuccess(response.data);
            })
            .catch(function(error) {
                if (handleFail) {
                    handleFail(error);
                }
            });
    }

    /**
     * @param handleSuccess {$Call}
     * @param handleFail {$Call}
     * @param params {{
     * }}
     * @param payload {{}}
     */
    getMetrics(params = {}, payload = {}, handleSuccess = null, handleFail = null) {
        this.axios.get('report/metrics')
            .then(function(response) {
                handleSuccess(response.data);
            })
            .catch(function(error) {
                if (handleFail) {
                    handleFail(error);
                }
            });
    }
}