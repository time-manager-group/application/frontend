export function downloadFile(url) {
    const link = document.createElement('a');
    link.href  = url;
    console.log(url);
    link.setAttribute('download', null);
    document.body.appendChild(link);
    link.click();
    link.parentNode.removeChild(link);
}

export function uploadFile(id, callable = () => {
}) {
    document.getElementById(id)?.remove();
    const fileInput    = document.createElement('input');
    const removeUpload = () => {
        document.getElementById(id)?.remove();
    }

    fileInput.type   = 'file';
    fileInput.hidden = true;
    fileInput.id     = id;
    document.body.appendChild(fileInput);
    fileInput.click();
    fileInput.addEventListener('change', function(e) {
        if (e.target.files.length > 0 && e.target.files[0] !== null) {
            callable(e.target.files[0], removeUpload);
        }
    });
}