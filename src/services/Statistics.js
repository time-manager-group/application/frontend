import {BackendApi} from './BackendApi';

/**
 * Api сервис взаимодействия с backend сервисом statistics.
 */
export default class Statistics extends BackendApi {
    /**
     * Получить рейтинг треков.
     *
     * @param handleSuccess {$Call}
     * @param handleFail {$Call}
     * @param filter {{
     * types:{
     *
     * },
     * dateFrom: Moment,
     * dateTo: Moment,
     * searchText: string,
     * limit: number|null,
     * }}
     */
    getTrackRating(handleSuccess, handleFail = null, filter = null) {
        const $this = this;

        this.axios.get('statistics/track-rating', {params: filter})
            .then(function(response) {
                handleSuccess(response.data);
            })
            .catch(function(error) {
                if (handleFail) {
                    handleFail();
                }

                $this.__initCrashEvent(error);
            });
    }

    /**
     * Получить суммарный результат.
     * @param filter {{
     * types:{
     *
     * },
     * dateFrom: Moment,
     * dateTo: Moment,
     * searchText: string,
     * }}
     * @param handleSuccess {$Call}
     * @param handleFail {$Call}
     */
    getSumAll(handleSuccess, handleFail = null, filter = null) {
        const $this = this;

        this.axios.get('statistics/track-rating/sum-all', {params: filter})
            .then(function(response) {
                handleSuccess(response.data);
            })
            .catch(function(error) {
                if (handleFail) {
                    handleFail();
                }

                $this.__initCrashEvent(error);
            });
    }
}
