export class TrackToTask {
    /**
     * @type {[{
     *     name:string,
     *     type:string
     * }]}
     *
     * @private
     */
    __trackList;

    /**
     *
     * @param trackList
     */
    constructor(trackList) {
        this.__trackList = trackList;

    }

    /**
     *
     * @returns {[{
     *     name:string,
     *     types:[string]
     * }]}
     */
    groupByName() {
        let tasks = [];

        const clearFill = (value) => {
            return value.trim().replace(/\s+/gui, ' ').toLowerCase();
        }

        const getTask = (taskList, name) => {
            for (const taskListElement of taskList) {
                if (clearFill(taskListElement.name)
                    === clearFill(name)) {
                    return taskListElement;
                }
            }
        };

        this.__trackList.map((item) => {
            let task = getTask(tasks, item.name);

            if (!task) {
                let newTask = {
                    name: item.name,
                    types: [item.type]
                };

                tasks.push(newTask);

                task = newTask;
            }

            if (!task.types.includes(item.type)) {
                task.types.push(item.type);
            }

        });

        return tasks;
    }

    /**
     *
     * @returns {[{
     *     name:string,
     *     type:string
     * }]}
     */
    groupByNameByType() {
        let tasks = [];

        this.groupByName().map((task) => {
            task.types.map((type) => {
                tasks.push({
                    name: task.name,
                    type: type
                })
            });
        });

        return tasks;
    }

}
