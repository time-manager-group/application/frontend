import Formatter from "./Formatter";

export default class TrackGrouper {
    static getGroupElement(group, item) {
        for (const groupElement of group) {
            if (Formatter.clearFill(groupElement.name)
                === Formatter.clearFill(item.name)) {
                return groupElement;
            }
        }

        const newGroupElement = {
            name: item.name,
            type: item.type,
            sumTime: 0,
            isOpen: false,
            isActive: false,
            tracks: []
        };

        group.push(newGroupElement);

        return newGroupElement;
    }

    static getTaskByName(group, name) {
        for (const groupElement of group) {
            if (Formatter.clearFill(groupElement.name)
                === Formatter.clearFill(name)) {

                return groupElement;
            }
        }

        return null;
    }

    static cloneTrack(track) {
        return {
            id: track.id,
            name: track.name,
            dateStart: new Date(track.dateStart),
            dateEnd: new Date(track.dateEnd),
            type: track.type,
            isActive: track.isActive
        };
    }

    static equalsTaskName(taskNameOne, taskNameTwo) {
        return Formatter.clearFill(taskNameOne) === Formatter.clearFill(taskNameTwo);
    }
}
