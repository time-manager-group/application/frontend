export default class Formatter {
    static clearFill(value) {
        return value.trim().replace(/\s+/gui, ' ').toLowerCase();
    }

    static makeDate(format) {
        const date = new Date(format);
        date.setTime(date.getTime() - date.getTimezoneOffset() * 60 * 1000);
        return date;
    }
}
