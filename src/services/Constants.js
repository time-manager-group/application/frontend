export default class Constants {
    static RATING_BACKGROUND_COLORS = new Map()
        .set('useless', '#17a2b1')
        .set('undefined', '#6c757d')
        .set('useful', '#28a745');
    static RATING_BORDER_COLORS     = new Map()
        .set('useless', '#117884')
        .set('undefined', '#51585e')
        .set('useful', '#1b7330');
    static RATING_TYPE_NAMES        = new Map()
        .set('useless', 'Бесполезные задачи')
        .set('undefined', 'Неопределенные задачи')
        .set('useful', 'Полезные задачи');
}
