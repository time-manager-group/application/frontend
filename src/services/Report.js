import {BackendApi} from "./BackendApi";
import EventToasterStore from "../stores/EventToasterStore";
import Formatter from "./Formatter";

/**
 * Api сервис взаимодействия с backend сервисом report.
 */
export default class Report extends BackendApi {
    /**
     * Получить оптимальное имя для нового отчета.
     *
     * @param handleSuccess {$Call}
     * @param handleFail {$Call}
     */
    getNameForCreate(handleSuccess, handleFail = null) {
        const $this = this;

        this.axios.get('report/nameForCreate', {})
            .then(function(response) {
                handleSuccess(response.data.name);
            })
            .catch(function(error) {
                if (handleFail) {
                    handleFail();
                }

                $this.__initCrashEvent(error);
            });
    }

    /**
     * Получить id последнего просматриваемого отчета.
     *
     * @param handleSuccess {$Call}
     * @param handleFail {$Call}
     */
    getLast(handleSuccess, handleFail = null) {
        const $this = this;

        this.axios.get('report/last', {})
            .then(function(response) {
                handleSuccess(response.data.id);
            })
            .catch(function(error) {
                if (handleFail) {
                    handleFail();
                }

                $this.__initCrashEvent(error);
            });
    }

    /**
     * Получить id активного отчета.
     *
     * @param handleSuccess {$Call}
     * @param handleFail {$Call}
     */
    getActive(handleSuccess, handleFail = null) {
        const $this = this;

        this.axios.get('report/active', {})
            .then(function(response) {
                handleSuccess(response.data.id);
            })
            .catch(function(error) {
                if (handleFail) {
                    handleFail();
                }

                $this.__initCrashEvent(error);
            });
    }

    /**
     * Создать новый отчет.
     *
     * @param name {string}
     * @param handleSuccess {$Call}
     * @param handleFail {$Call}
     */
    create(name, handleSuccess = null, handleFail = null) {
        const $this = this;

        this.axios.post('report/', {
                name: name
            })
            .then(function(response) {
                if (handleSuccess) {
                    handleSuccess(response.data.id);
                }
            })
            .catch(function(error) {
                if (handleFail) {
                    handleFail();
                }

                $this.__initCrashEvent(error);
            });
    }

    /**
     * Получить список отчетов.
     *
     * @param handleSuccess {$Call}
     * @param handleFail {$Call}
     * @param params {{
     *          filter: {}|null,
     *          sort: {
     *              by:string,
     *              order:string
     *              }|null,
     *           pagination:
     *           {
     *              page: number|null,
     *              per_page: number|null
     *            }|null
     *      }|null}
     */
    getReportList(handleSuccess, handleFail = null, params = null) {
        const $this = this;
        this.axios.get(`report/`, {params: params ? {...params.filter, ...params.sort, ...params.pagination} : null})
            .then(function(response) {
                handleSuccess(response.data);
            })
            .catch(function(error) {
                if (handleFail) {
                    handleFail();
                }

                $this.__initCrashEvent(error);
            });
    }

    /**
     * Получить список треков отчета.
     *
     * @param reportId {number}
     * @param handleSuccess {$Call}
     * @param handleFail {$Call}
     */
    getTrackList(reportId, handleSuccess, handleFail = null) {
        const $this = this;

        this.axios.get(`report/${reportId}/track`, {})
            .then(function(response) {
                for (const item of response.data) {
                    item.dateStart = Formatter.makeDate(item.dateStart);
                    item.dateEnd   = Formatter.makeDate(item.dateEnd);
                }

                handleSuccess(response.data);
            })
            .catch(function(error) {
                if (handleFail) {
                    handleFail();
                }

                $this.__initCrashEvent(error);
            });
    }

    /**
     * Создать трек в отчете.
     *
     * @param reportId {number}
     * @param track {*}
     * @param handleSuccess {$Call}
     * @param handleFail {$Call}
     */
    createTrack(
        reportId,
        track,
        handleSuccess = null,
        handleFail    = null
    ) {
        const $this = this;

        this.axios.post(`report/${reportId}/track`, track)
            .then(function(response) {
                if (handleSuccess) {
                    handleSuccess(response.data);
                }
            })
            .catch(function(error) {
                if (handleFail) {
                    handleFail();
                }

                $this.__initCrashEvent(error);
            });
    }

    /**
     * Создать трек в отчете.
     *
     * @param reportId {number}
     * @param trackId {number}
     * @param track {*}
     * @param handleSuccess {$Call}
     * @param handleFail {$Call}
     */
    updateTrack(
        reportId,
        trackId,
        track,
        handleSuccess = null,
        handleFail    = null
    ) {
        const $this = this;

        this.axios.put(`report/${reportId}/track/${trackId}`, track)
            .then(function(response) {
                if (handleSuccess) {
                    handleSuccess(response.data);
                }
            })
            .catch(function(error) {
                if (handleFail) {
                    handleFail();
                }

                $this.__initCrashEvent(error);
            });
    }

    /**
     * Удалить трек из отчета.
     *
     * @param reportId {number}
     * @param trackId {number}
     * @param handleSuccess {$Call}
     * @param handleFail {$Call}
     */
    deleteTrack(
        reportId,
        trackId,
        handleSuccess = null,
        handleFail    = null
    ) {
        const $this = this;

        this.axios.delete(`report/${reportId}/track/${trackId}`)
            .then(function(response) {
                if (handleSuccess) {
                    handleSuccess(response.data);
                }
            })
            .catch(function(error) {
                if (handleFail) {
                    handleFail();
                }

                $this.__initCrashEvent(error);
            });
    }

    /**
     * Получить файл экспорта отчета.
     *
     * @param reportId {number}
     */
    getExportFileUrl(reportId) {
        return this.axios.defaults.baseURL + `/report/export/${reportId}`;
    }

    /**
     * Удалить трек из отчета.
     *
     * @param file {File}
     * @param reportIdForUpdate {number|null}
     * @param handleSuccess {$Call}
     * @param handleFail {$Call}
     */
    importReport(
        file,
        reportIdForUpdate = null,
        handleSuccess     = null,
        handleFail        = null
    ) {
        const $this = this;

        const formData = new FormData();
        formData.append('reportFile', file);

        this.axios.post(`report/import`, formData, {params: {reportIdForUpdate}})
            .then(function(response) {
                if (handleSuccess) {
                    handleSuccess(response.data);
                }
            })
            .catch(function(error) {
                if (handleFail) {
                    handleFail();
                }

                $this.__initCrashEvent(error);
            });
    }

    /**
     * Выбросить ошибку в тостер.
     *
     * @param error {string}
     * @private
     */
    __initCrashEvent(error) {
        EventToasterStore.getInstance().appendEvent({
            title: 'Сервис отчетов недоступен',
            text:  'Невозможно получить ответ от сервиса отчетов, ' + error
        });
    }
}
