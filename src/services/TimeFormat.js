/**
 * Формат тайма.
 */
export class TimeFormat {
    __SECOND_1 = 'секунда';
    __SECOND_2 = 'секунды';
    __SECOND_3 = 'секунд';
    __MINUT_1 = 'минута';
    __MINUT_2 = 'минуты';
    __MINUT_3 = 'минут';
    __HOUR_1 = 'час';
    __HOUR_2 = 'часа';
    __HOUR_3 = 'часов';

    /**
     * @type {number}
     * @private
     */
    __diffSeconds;

    /**
     *
     * @param diffSeconds {number}
     */
    constructor(diffSeconds) {
        this.__diffSeconds = diffSeconds;
    }


    /**
     *
     */
    format() {
        // noinspection JSCheckFunctionSignatures
        const hours = parseInt(this.__diffSeconds / 3600);
        // noinspection JSCheckFunctionSignatures
        const minutes = parseInt((this.__diffSeconds % 3600) / 60);
        // noinspection JSCheckFunctionSignatures
        const seconds = parseInt((this.__diffSeconds % 3600) % 60);

        let format = '';

        if (this.__diffSeconds === 0) {
            format = this.__getSeconds(0);
        } else {
            if (hours) {
                format += this.__getHours(hours) + ' ';
            }
            if (minutes) {
                format += this.__getMinutes(minutes) + ' ';
            }
            if (seconds) {
                format += this.__getSeconds(seconds) + ' ';
            }
        }


        return format.trim().replace(/\s+/gui, ' ');
    }

    /**
     * @return string
     */
    formatShort() {
        // noinspection JSCheckFunctionSignatures
        const hours = parseInt(this.__diffSeconds / 3600);
        // noinspection JSCheckFunctionSignatures
        const minutes = parseInt((this.__diffSeconds % 3600) / 60);
        // noinspection JSCheckFunctionSignatures
        const seconds = parseInt((this.__diffSeconds % 3600) % 60);

        let format = '';

        if (this.__diffSeconds === 0) {
            format = this.__getSeconds(0);
        } else {
            if (hours) {
                format += hours + 'ч ';
            }
            if (minutes) {
                format += minutes + 'м ';
            }
            if (seconds) {
                format += seconds + 'с ';
            }
        }


        return format.trim().replace(/\s+/gui, ' ');
    }

    /**
     *
     * @param seconds
     * @returns {string}
     * @private
     */
    __getSeconds(seconds) {
        return this.__getDescTime(seconds, this.__SECOND_1, this.__SECOND_2, this.__SECOND_3);
    }

    /**
     *
     * @param minutes
     * @returns {string}
     * @private
     */
    __getMinutes(minutes) {
        return this.__getDescTime(minutes, this.__MINUT_1, this.__MINUT_2, this.__MINUT_3);
    }

    /**
     *
     * @param hours
     * @returns {string}
     * @private
     */
    __getHours(hours) {
        return this.__getDescTime(hours, this.__HOUR_1, this.__HOUR_2, this.__HOUR_3);
    }

    /**
     *
     * @param value  {string|number}
     * @param text1  {string}
     * @param text2  {string}
     * @param text3  {string}
     * @returns {string}
     * @private
     */
    __getDescTime(value, text1, text2, text3) {
        value = value + '';

        const valueArray = (value.split('')).reverse();
        // noinspection JSCheckFunctionSignatures
        const oneNumber = parseInt(valueArray[0] ?? 0);
        // noinspection JSCheckFunctionSignatures
        const twoNumber = parseInt(valueArray[1] ?? 0);
        if (twoNumber === 0 || twoNumber > 1) {
            if (oneNumber === 0 || oneNumber > 4) {
                return value + ' ' + text3;
            }

            if (oneNumber === 1) {
                return value + ' ' + text1;
            }

            return value + ' ' + text2;
        }

        return value + ' ' + text3;
    }
}

export default TimeFormat;
