/**
 * Преобразователь тайма.
 */
import TimeFormat from './TimeFormat';

export class TimeDropper {
    /**
     * @type {number}
     * @private
     */
    __diffSeconds;
    /**
     * @type {number}
     * @private
     */
    __diffSecondsWithoutAbs;

    /**
     *
     * @param dateStart {Date}
     * @param dateEnd {Date}
     */
    constructor(dateStart, dateEnd) {
        this.__diffSecondsWithoutAbs =
            Math.round(dateEnd.getTime() / 1000) - Math.round(dateStart.getTime() / 1000);

        this.__diffSeconds = Math.abs(
            this.__diffSecondsWithoutAbs
        );

    }

    /**
     *
     * @returns {number}
     */
    getDiffSeconds() {
        return this.__diffSeconds;
    }

    /**
     *
     * @returns {number}
     */
    getDiffSecondsWithoutAbs() {
        return this.__diffSecondsWithoutAbs;
    }

    /**
     *
     */
    format() {
        return new TimeFormat(this.__diffSeconds).format();
    }

}

export default TimeDropper;
