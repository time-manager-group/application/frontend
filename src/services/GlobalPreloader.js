import ApplicationStore from "../stores/ApplicationStore";
import PreloaderStore from "../stores/PreloaderStore";

export default class GlobalPreloader {
    static showPreloader() {
        PreloaderStore.getInstance().show(true);
        ApplicationStore.getInstance().show(false);
    }

    static hidePreloader() {
        PreloaderStore.getInstance().show(false);
        ApplicationStore.getInstance().show(true);
    }
}
