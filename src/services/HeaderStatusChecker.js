import HeaderStatusStore from "../stores/HeaderStatusStore";

export default class HeaderStatusChecker {
    /**
     *
     * @param items {[{
     *     isActive:boolean
     * }]}
     */
    static checkStatus(items) {
        const headerStatusStore = HeaderStatusStore.getInstance();

        if (items.length > 0) {
            let isActive = false;

            for (const item of items) {
                if (item.isActive) {
                    isActive = true;
                    break;
                }
            }

            if (isActive) {
                headerStatusStore.setActive();
            } else {
                headerStatusStore.setStopped();
            }
        } else {
            headerStatusStore.setEmpty();
        }
    }
}
