import {BackendApi} from "./BackendApi";
import EventToasterStore from "../stores/EventToasterStore";

/**
 * Api к сервису по по поиску треков времени.
 */
export default class TrackSearch extends BackendApi {
    actualRequestId;
    handle;
    /**
     * @var {[*]}
     */
    lastResult;

    constructor() {
        super();
        this.lastResult = [];
    }

    setHandleSuccess(callback) {
        this.handle = callback;
    }

    getLastResult() {
        return this.lastResult;
    }

    find(query, items) {
        const requestId = this.makeId();
        this.actualRequestId = requestId;

        let excluded = [];
        const $this = this;

        for (const item of items) {
            excluded.push(item.name);
        }

        this.axios.post('search/track', {
            query: query,
            excluded: excluded,
        })
            .then(function (response) {
                // хитрая схема, для того чтобы получать только актуальные ответы.
                if ($this.actualRequestId === requestId) {
                    $this.lastResult = response.data;
                    $this.handle(response.data);
                }
            })
            .catch(function (error) {
                $this.lastResult = [];
                $this.handle([]);
                $this.__initCrashEvent(error);
            });
    }

    /**
     * Выбросить ошибку в тостер.
     *
     * @param error {string}
     * @private
     */
    __initCrashEvent(error) {
        EventToasterStore.getInstance().appendEvent({
            title: 'Сервис поиска недоступен',
            text: 'Невозможно получить ответ от сервиса поиска, ' + error,
        });
    }
}
