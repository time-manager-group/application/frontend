import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import NotFoundPage from './components/NotFoundPage';
import {Route, BrowserRouter as Router, Routes} from 'react-router-dom';
import Statistics from './components/Statistics/WorkSpace';
import ExportImport from './components/ExportImport/WorkSpace';
import Note from './components/Note/WorkSpace';
import Singleton from './components/Note/components/Singleton';
import EventToasterStore from './stores/EventToasterStore';
import ReportApi from './services/ReportApi';


const eventToasterStore = Singleton.getInstance('event-toaster-store', () => {
    return EventToasterStore.getInstance();
});

Singleton.register('common-report-api', () => {
    const instance = new ReportApi();
    instance.addHandleFail((error) => {
        eventToasterStore.appendEvent({
            title: 'Report service is unavailable',
            text:  'Unable to get response from Report service, ' + error
        });
    })
    return instance;
});


ReactDOM.render(
    <React.StrictMode>
        <Router>
            <Routes>
                <Route index element={<App/>}/>
                <Route path="/statistics" element={<Statistics/>}/>
                <Route path="/export-import" element={<ExportImport/>}/>
                <Route path="/note" element={<Note/>}/>
                <Route path="*" element={<NotFoundPage/>}/>
            </Routes>
        </Router>
    </React.StrictMode>,
    document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();