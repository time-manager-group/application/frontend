import './App.css';
import './styles/Global.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import React from "react";
import Preloader from "./components/Preloader";
import Application from "./components/Application";
import ApplicationStore from "./stores/ApplicationStore";
import PreloaderStore from "./stores/PreloaderStore";
import CreateReportModal from "./components/CreateReportModal";
import ReportModalStore from "./stores/ReportModalStore";
import WorkSpaceLoader from "./scripts/WorkSpaceLoader";
import EventToaster from "./components/EventToaster";
import EventToasterStore from "./stores/EventToasterStore";
import TrackInterval from "./services/TrackInterval";
import Singleton from './components/Note/components/Singleton';
import ReportApi from './services/ReportApi';

/**
 * Стартовая точка входа.
 */
export default class App extends React.Component {
    componentDidMount() {
        new WorkSpaceLoader().load();
        TrackInterval.initInterval();
    }

    render() {
        const applicationStore = ApplicationStore.getInstance();
        const preloaderStore   = PreloaderStore.getInstance();
        const reportModalStore = ReportModalStore.getInstance();
        const eventToasterStore = Singleton.getInstance('event-toaster-store');

        return (
            <>
                <Preloader
                    store={preloaderStore}
                />
                <CreateReportModal
                    store={reportModalStore}
                />
                <Application
                    store={applicationStore}
                />
                <EventToaster
                    store={eventToasterStore}
                />
            </>
        );
    }
}