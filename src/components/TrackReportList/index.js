import React from 'react';
import {Accordion, Button, Card, Collapse, ListGroup} from 'react-bootstrap';
import "react-datetime/css/react-datetime.css";

/**
 *
 */
export class TrackReportList extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            trackList: props.trackList,
            isOpen:    props.isOpen
        };

        this.myRef           = React.createRef();
        this.handleAccordion = this.handleAccordion.bind(this);
    }

    // noinspection JSCheckFunctionSignatures
    componentWillReceiveProps(props) {
        // noinspection JSUnresolvedVariable
        this.setState({
            trackList: props.trackList,
            isOpen:    props.setIsOpen ? props.isOpen : this.state.isOpen
        });
    }

    handleAccordion() {
        this.state.isOpen = !this.state.isOpen;

        this.setState(this.state);
    }

    render() {

        return (
            <div>
                <Accordion ref={this.myRef}>
                    <Card>
                        <Card.Header>
                            <Accordion.Toggle
                                as={Button}
                                variant="link"
                                eventKey="1"
                                onClick={this.handleAccordion}
                            >
                                {!this.state.isOpen ? 'Показать' : 'Скрыть'}
                            </Accordion.Toggle>
                        </Card.Header>
                        <Collapse in={this.state.isOpen}>
                            <Card.Body>
                                <ListGroup variant="flush">
                                    {
                                        this.state.trackList.map(item => (item))
                                    }
                                </ListGroup></Card.Body>
                        </Collapse>
                    </Card>
                </Accordion>
            </div>
        )
    }
}

export default TrackReportList;
