import React from 'react';

import {Form} from 'react-bootstrap';
import classes from '../TrackField/style.module.css';

/**
 *
 */
export class TrackField extends React.Component {
    /**
     *
     * @param props {
     * {
     *  number:number
     *  value:string
     *  onChange:callback
     *  }
     * }
     */
    constructor(props) {
        super(props);

        this.state = {
            beforeValue: '',
            value:       props.value
        };

        this.isShiftAltDown = false;
        this.handleChange   = this.handleChange.bind(this);
        this.handlePress    = this.handlePress.bind(this);
        this.handleBlur     = this.handleBlur.bind(this);
        this.handleFocus    = this.handleFocus.bind(this);

        this.valueBefore = null;
        this.inputRef    = React.createRef();
    }

    // noinspection JSCheckFunctionSignatures
    componentWillReceiveProps(props) {
        this.state.value = props.value;

        this.setState(this.state);
    }

    clearFill(value) {
        return value.trim().replace(/\s+/gui, ' ');
    }

    handlePress(event) {
        if (event.hasOwnProperty('key') && event.key === 'Enter') {
            if (this.props.hasOwnProperty('onPress')) {
                this.props.onPress(event, this.props?.id)
            }
            if (event.hasOwnProperty('stopOnBlur') && event.stopOnBlur) {
                event.preventDefault()
                return;
            }
            event.target.blur();
        }
    }

    handleFocus(event) {
        this.state.beforeValue = event.target.value;
        this.valueBefore       = this.state.value;

        this.setState(this.state);

        if (this.props.hasOwnProperty('onFocus')) {
            this.props.onFocus(event, this.props?.id)
        }
    }

    handleBlur(event) {
        if (this.isShiftAltDown === true) {
            this.isShiftAltDown = false;
            return;
        }

        this.state.value = this.clearFill(event.target.value);

        if (!this.state.value) {
            this.state.value = this.state.beforeValue;
        } else {
            this.state.beforeValue = this.state.value;
        }

        if (this.state.value !== this.valueBefore) {
            this.props.onChange(this.state.value, this.props?.id);
            this.props.onChangeFixed(this.state.value, this.valueBefore, this.props?.id);
        }

        this.setState(this.state);

        if (this.props.hasOwnProperty('onBlur')) {
            this.props.onBlur(event, this.props?.id)
        }
    }

    handleChange(event) {
        this.state.value = event.target.value;
        this.props.onChange(this.state.value, this.props?.id);
        this.setState(this.state);
    }

    render() {
        return (
            <Form.Control
                className={classes.plain_field}
                plaintext
                type="text"
                readOnly={this.props?.readOnly || false}
                onMouseDown={(e) => {
                    if (this.props.hasOwnProperty('onButtonAction')) {
                        this.props.onButtonAction(e, this.props?.id)
                    }
                }}
                value={this.state.value}
                onChange={this.handleChange}
                onKeyPress={this.handlePress}
                ref={this.inputRef}
                onKeyDown={(e) => {
                    this.isShiftAltDown = [16, 18].includes(e.keyCode);
                    if (e.keyCode === 8 && e.target.value === '') {
                        if (this.props.hasOwnProperty('onEmpty')) {
                            this.props.onEmpty(e, this.props?.id)
                        }
                    }
                    if (this.props.hasOwnProperty('onButtonAction')) {
                        this.props.onButtonAction(e, this.props?.id)
                    }
                }}//Shift+Alt для firefox = срабатывает blur - отключить его
                onBlur={this.handleBlur}
                onFocus={this.handleFocus}
                style={this.props?.style || undefined}
            />
        );
    }
}

export default TrackField;