import React from 'react';

import classes from './style.module.css';
import {Alert, Badge, ProgressBar, Table} from "react-bootstrap";
import TimeFormat from "../../services/TimeFormat";
import '../../styles/Animation.css';
import {typeSumter} from '../../services/TimeHelper';

/**
 *
 */
const TrackProgress = (props) => {
    const typeCurrentActive = (items, type) => {
        let currentActive = false;
        items.map((item) => {
            if (item.type === type) {
                if (item.isActive) {
                    currentActive = true;
                }
            }
        });

        return currentActive;
    };

    const calcProgress = (get, set) => {
        return (set / get) * 100;
    };

    const textWasted = (get, set) => {
        return get < set ? 'перевыполнено' : 'осталось';
    };

    const animateWasted = (get, set) => {
        return get < set ? 'animate-pulse' : '';
    };

    let sumUseless = typeSumter(props.reportState, 'useless');
    let sumUndefined = typeSumter(props.reportState, 'undefined');
    let sumUseful = typeSumter(props.reportState, 'useful');

    let progressUselessAnimated = typeCurrentActive(props.reportState, 'useless');
    let progressUsefulAnimated = typeCurrentActive(props.reportState, 'useful');
    let progressUndefinedAnimated = typeCurrentActive(props.reportState, 'undefined');

    let progressUseless = calcProgress(props.limitSecondsAll, sumUseless);
    let progressUseful = calcProgress(props.limitSecondsAll, sumUseful);
    let progressUndefined = calcProgress(props.limitSecondsAll, sumUndefined);

    let sumAll = sumUseless + sumUndefined + sumUseful;

    let sumAllWasted = Math.abs(props.limitSecondsAll - sumAll);

    let sumUselessUndefined = sumUseless + sumUndefined;
    let sumUselessUseful = sumUseless + sumUseful;
    let sumUndefinedUseful = sumUndefined + sumUseful;

    let sumUselessUndefinedWasted = Math.abs(props.limitSecondsAll - sumUselessUndefined);
    let sumUselessUsefulWasted = Math.abs(props.limitSecondsAll - sumUselessUseful);
    let sumUndefinedUsefulWasted = Math.abs(props.limitSecondsAll - sumUndefinedUseful);

    let sumUselessUndefinedWastedLimitSecondsUseless = Math.abs(props.limitSecondsUseless - sumUselessUndefined);
    let sumUndefinedUsefulWastedLimitSecondsUseful = Math.abs(props.limitSecondsUseful - sumUndefinedUseful);

    let progressUselessLimitSecondsUseless = calcProgress(props.limitSecondsUseless, sumUseless);
    let progressUndefinedLimitSecondsUseless = calcProgress(props.limitSecondsUseless, sumUndefined);

    let progressUndefinedLimitSecondsUseful = calcProgress(props.limitSecondsUseful, sumUndefined);
    let progressUsefulLimitSecondsUseful = calcProgress(props.limitSecondsUseful, sumUseful);

    return (
        <>
            <Table borderless={true} className={classes['mb']} size="sm">
                <tbody>
                <tr>
                    <td>
                        <ProgressBar>
                            <ProgressBar striped animated={progressUselessAnimated} variant="info" now={progressUseless} key={1}/>
                            <ProgressBar striped animated={progressUndefinedAnimated} variant="secondary" now={progressUndefined} key={2}/>
                            <ProgressBar striped animated={progressUsefulAnimated} variant="success" now={progressUseful} key={3}/>
                        </ProgressBar>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div className={classes['table-group-container']}>
                            <Alert
                                className={classes['main-alert'] + ' ' + classes['table-container'] + ' ' + classes['mb'] + ' ' + animateWasted(props.limitSecondsAll, sumAll)}
                                variant={
                                    'info'
                                }
                            > <b>
                                {props.limitSecondsAll - sumAll > 0 ? 'Осталось' : 'Перевыполнено'} всего: {new TimeFormat(sumAllWasted).format()}</b>
                            </Alert><Alert
                            className={classes['main-alert'] + ' ' + classes['table-container'] + ' ' + classes['mb']}
                            variant={
                                'success'
                            }
                        ><b> Затрачено всего: {new TimeFormat(sumAll).format()}</b>
                        </Alert>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div className={classes['table-group-container']}>
                            <Table size="sm" className={classes['table-container'] + ' ' + classes['mb']}>
                                <tbody>
                                <tr>
                                    <td>
                                        <div className={classes['table-group-container']}>
                                            <Badge
                                                className={classes['table-container']}
                                                variant={
                                                    'warning'
                                                }
                                            >Бесполезное и неопределенное время затрачено:<br/>
                                                {new TimeFormat(sumUselessUndefined).format()}
                                            </Badge>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div className={classes['table-group-container']}>
                                            <Badge
                                                className={classes['table-container'] + ' ' + animateWasted(props.limitSecondsAll, sumUselessUndefined)}
                                                variant={
                                                    'warning'
                                                }
                                            >Бесполезное и неопределенное время {textWasted(props.limitSecondsAll, sumUselessUndefined)}:<br/>
                                                {new TimeFormat(sumUselessUndefinedWasted).format()}
                                            </Badge>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <ProgressBar>
                                            <ProgressBar striped animated={progressUselessAnimated} variant="info" now={progressUseless} key={1}/>
                                            <ProgressBar striped animated={progressUndefinedAnimated} variant="secondary" now={progressUndefined} key={2}/>
                                        </ProgressBar>
                                    </td>
                                </tr>
                                </tbody>
                            </Table>
                            <Table size="sm" className={classes['table-container'] + ' ' + classes['mb']}>
                                <tbody>
                                <tr>
                                    <td>
                                        <div className={classes['table-group-container']}>
                                            <Badge
                                                className={classes['table-container']}
                                                variant={
                                                    'primary'
                                                }
                                            >Бесполезное и полезное время затрачено:<br/>
                                                {new TimeFormat(sumUselessUseful).format()}
                                            </Badge>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div className={classes['table-group-container']}>
                                            <Badge
                                                className={classes['table-container'] + ' ' + animateWasted(props.limitSecondsAll, sumUselessUseful)}
                                                variant={
                                                    'primary'
                                                }
                                            >Бесполезное и полезное время {textWasted(props.limitSecondsAll, sumUselessUseful)}:<br/>
                                                {new TimeFormat(sumUselessUsefulWasted).format()}
                                            </Badge>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <ProgressBar>
                                            <ProgressBar striped animated={progressUselessAnimated} variant="info" now={progressUseless} key={1}/>
                                            <ProgressBar striped animated={progressUsefulAnimated} variant="success" now={progressUseful} key={2}/>
                                        </ProgressBar>
                                    </td>
                                </tr>
                                </tbody>
                            </Table>
                            <Table size="sm" className={classes['table-container'] + ' ' + classes['mb']}>
                                <tbody>
                                <tr>
                                    <td>
                                        <div className={classes['table-group-container']}>
                                            <Badge
                                                className={classes['table-container']}
                                                variant={
                                                    'dark'
                                                }
                                            >Неопределенное и полезное время затрачено:<br/>
                                                {new TimeFormat(sumUndefinedUseful).format()}
                                            </Badge>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div className={classes['table-group-container']}>
                                            <Badge
                                                className={classes['table-container'] + ' ' + animateWasted(props.limitSecondsAll, sumUndefinedUseful)}
                                                variant={
                                                    'dark'
                                                }
                                            >Неопределенное и полезное время {textWasted(props.limitSecondsAll, sumUndefinedUseful)}:<br/>
                                                {new TimeFormat(sumUndefinedUsefulWasted).format()}
                                            </Badge>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <ProgressBar>
                                            <ProgressBar striped animated={progressUndefinedAnimated} variant="secondary" now={progressUndefined} key={1}/>
                                            <ProgressBar striped animated={progressUsefulAnimated} variant="success" now={progressUseful} key={2}/>
                                        </ProgressBar>
                                    </td>
                                </tr>
                                </tbody>
                            </Table>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div className={classes['table-group-container']}>
                            <Table size="sm" className={classes['table-container'] + ' ' + classes['mb']}>
                                <tbody>
                                <tr>
                                    <td>
                                        <div className={classes['table-group-container']}>
                                            <Badge
                                                className={classes['table-container'] + ' ' + animateWasted(props.limitSecondsUseless, sumUselessUndefined)}
                                                variant={
                                                    'info'
                                                }
                                            >Бесполезное и неопределенное время {textWasted(props.limitSecondsUseless, sumUselessUndefined)}:<br/>
                                                {new TimeFormat(sumUselessUndefinedWastedLimitSecondsUseless).format()}
                                            </Badge>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <ProgressBar>
                                            <ProgressBar striped animated={progressUselessAnimated} variant="info" now={progressUselessLimitSecondsUseless} key={1}/>
                                            <ProgressBar striped animated={progressUndefinedAnimated} variant="secondary" now={progressUndefinedLimitSecondsUseless} key={2}/>
                                        </ProgressBar>
                                    </td>
                                </tr>
                                </tbody>
                            </Table>
                            <Table size="sm" className={classes['table-container'] + ' ' + classes['mb']}>
                                <tbody>
                                <tr>
                                    <td>
                                        <div className={classes['table-group-container']}>
                                            <Badge
                                                className={classes['table-container'] + ' ' + animateWasted(props.limitSecondsUseful, sumUndefinedUseful)}
                                                variant={
                                                    'success'
                                                }
                                            >Неопределенное и полезное время {textWasted(props.limitSecondsUseful, sumUndefinedUseful)}:<br/>
                                                {new TimeFormat(sumUndefinedUsefulWastedLimitSecondsUseful).format()}
                                            </Badge>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <ProgressBar>
                                            <ProgressBar striped animated={progressUndefinedAnimated} variant="secondary" now={progressUndefinedLimitSecondsUseful} key={1}/>
                                            <ProgressBar striped animated={progressUsefulAnimated} variant="success" now={progressUsefulLimitSecondsUseful} key={2}/>
                                        </ProgressBar>
                                    </td>
                                </tr>
                                </tbody>
                            </Table>
                        </div>
                    </td>
                </tr>
                </tbody>
            </Table>
        </>
    );
}

export default TrackProgress;
