import React from 'react';
import {FormControl, InputGroup, ListGroup} from 'react-bootstrap';
import Button from 'react-bootstrap/Button';
import TrackTypes from '../TrackTypes';
import classes from './style.module.css';
import {TrackToTask} from "../../services/TrackToTask";
import TrackSearch from "../../services/TrackSearch";

/**
 *
 */
export class TrackInput extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            value: '',
            type: 'undefined',
            searchItems: []
        };
        this.handleChange = this.handleChange.bind(this);
        this.handlePress = this.handlePress.bind(this);
        this.eventClick = this.eventClick.bind(this);
        this.handleChangeType = this.handleChangeType.bind(this);

        this.handleBlur = this.handleBlur.bind(this);
        this.handleFocus = this.handleFocus.bind(this);

        this.handleKeyDown = this.handleKeyDown.bind(this);
        this.handleSearchClick = this.handleSearchClick.bind(this);
        this.handleSearchApi = this.handleSearchApi.bind(this);

        this.searchItems = new TrackToTask(props.reportState.concat(props.taskTemplateList))
            .groupByNameByType();

        this.searchBackend = new TrackSearch();
        this.searchBackend.setHandleSuccess(this.handleSearchApi);

        this.realSearchItems = [];
    }

    // noinspection JSCheckFunctionSignatures
    componentWillReceiveProps(props) {
        this.searchItems = new TrackToTask(props.reportState.concat(props.taskTemplateList))
            .groupByNameByType();
    }

    handleFocus(event) {
        this.handleChange(event);
    }

    handleBlur() {
        this.setState({searchItems: []});
    }

    handleKeyDown(event) {
        let currentI = -1;
        const searchItems = [];
        let value = this.state.value;
        let type = this.state.type;

        this.state.searchItems.map((searchItem, i) => {
            if (searchItem.current) {
                currentI = i;
            }

            searchItem.current = false;
            searchItems.push(searchItem);
        });

        if (!searchItems.length) {
            return;
        }

        if (event.key === 'ArrowDown') {
            if ((currentI + 1) >= searchItems.length) {
                currentI = -1;
            }

            searchItems[currentI + 1].current = true;
            value = searchItems[currentI + 1].name;
            type = searchItems[currentI + 1].type;
        } else if (event.key === 'ArrowUp') {
            if (currentI < 1) {
                currentI = searchItems.length;
            }

            searchItems[currentI - 1].current = true;
            value = searchItems[currentI - 1].name;
            type = searchItems[currentI - 1].type;
        }

        this.setState({
            value: value,
            type: type,
            searchItems: searchItems
        });
    }

    handleSearchClick(item) {
        this.props.onGreet({
            value: item.name,
            type: item.type,
        });

        this.state.value = '';
        this.state.type = 'undefined';
    }

    handleChange(event) {
        const searchItems = [];

        const searchValue = event.target.value.toLowerCase()
            .trim()
            .replace(/([^a-zА-Я\-\s])+/uig, '')
            .replace(/\s+/uig, ' ');

        // eslint-disable-next-line array-callback-return
        this.searchItems.map((item) => {
            if (searchItems.length <= this.props.maxCount) {
                const searchedValue = item.name.toLowerCase()
                    .trim()
                    .replace(/([^a-zА-Я\-\s])+/uig, '')
                    .replace(/\s+/uig, ' ');

                if (searchedValue.includes(searchValue)) {
                    item.current = false;
                    searchItems.push(item);
                }
            }
        });

        this.realSearchItems = searchItems;

        if (searchItems.length < this.props.maxCount) {
            this.searchBackend.find(event.target.value, searchItems);
        }

        this.setState({
            value: event.target.value,
            searchItems: this.mergeSearchApiResult(this.searchBackend.getLastResult())
        });
    }

    mergeSearchApiResult(items) {
        let searchItems = []

        for (const searchItem of this.realSearchItems) {
            searchItems.push(searchItem);
        }

        if (searchItems.length < this.props.maxCount) {
            const slice = this.props.maxCount - searchItems.length;

            const data = items.slice(0, slice);

            for (const dataElement of data) {
                searchItems.push({
                    name: dataElement.name,
                    type: dataElement.type,
                });
            }
        }

        return searchItems;
    }

    handleSearchApi(items) {
        let searchItems = this.mergeSearchApiResult(items);

        this.setState({value: this.state.value, searchItems: searchItems});
    }

    handlePress(event) {
        const value = this.clearFill(this.state.value);

        if (event.hasOwnProperty('key') && event.key === 'Enter' && value) {
            this.props.onGreet(Object.assign({}, this.state));
            this.state.value = '';
            this.state.type = 'undefined';
            this.state.searchItems = [];
        }
    }

    handleChangeType(type) {
        this.setState({type: type});
    }

    clearFill(value) {
        return value.trim().replace(/\s+/gui, ' ');
    }

    eventClick() {
        const value = this.clearFill(this.state.value);

        if (value) {
            this.props.onGreet(Object.assign({}, this.state));
            this.state.value = '';
            this.state.type = 'undefined';
        }
    }

    render() {
        const variants = new Map();

        variants.set('useless', 'info');
        variants.set('undefined', 'secondary');
        variants.set('useful', 'success');

        const buttonStart = (<Button variant="secondary" onClick={this.eventClick}>Начать</Button>);
        return (
            <div>
                <InputGroup className="mb-3">
                    <div className={classes['input-container']}
                         onBlur={this.handleBlur}
                    >
                        <FormControl className={classes['input'] + ' ' + classes['input-active']}
                                     placeholder="Наименование задачи"
                                     aria-label="Наименование задачи"
                                     aria-describedby="basic-addon2"
                                     onChange={this.handleChange}
                                     onClick={this.handleChange}
                                     onKeyPress={this.handlePress}
                                     value={this.state.value}
                                     onFocus={this.handleFocus}
                                     onKeyDown={this.handleKeyDown}
                        />
                        <ListGroup className={classes['list-group']}>
                            {
                                this.state.searchItems.map((searchItem) =>
                                    <ListGroup.Item className={classes['list-group-item']}>
                                        <Button
                                            data-search-item={true}
                                            className={classes['item-button']}
                                            title={'Запустить'}
                                            size={'sm'}
                                            variant={'outline-' + variants.get(searchItem.type)}
                                            active={searchItem.current}
                                            onMouseDown={() => this.handleSearchClick(searchItem)}
                                        >
                                            {searchItem.name}
                                        </Button>
                                    </ListGroup.Item>
                                )
                            }
                        </ListGroup>
                    </div>
                    <InputGroup.Append className={classes['button-group']}>
                        <TrackTypes
                            focus={this.state.type}
                            afterGroup={buttonStart}
                            onChange={this.handleChangeType}
                        />
                    </InputGroup.Append>
                </InputGroup>
            </div>
        )
    }
}

export default TrackInput;
