import React from 'react';

import "react-datetime/css/react-datetime.css";
import Datetime from 'react-datetime';

import {Calendar} from "react-bootstrap-icons";
import Button from "react-bootstrap/Button";
import {ButtonGroup} from "react-bootstrap";
import classes from './style.module.css';

/**
 *
 */
export class TrackTime extends React.Component {
    /**
     *
     * @param props {
     * {
     *  date:Date,
     *  onChange:{any}
     *  }
     * }
     */
    constructor(props) {
        super(props);

        this.dateChange = this.dateChange.bind(this);
        this.timeChange = this.timeChange.bind(this);
        this.handleShowDate = this.handleShowDate.bind(this);
        this.handleFocus = this.handleFocus.bind(this);
        this.handleBlur = this.handleBlur.bind(this);

        this.timeBefore = null;

        this.state = {
            date: props.date,
            showDate: false,
        };

        this.runtimePropsUpdate = true;
    }

    handleFocus() {
        this.runtimePropsUpdate = false;
        this.timeBefore = new Date(this.state.date);
    }

    handleBlur() {
        this.runtimePropsUpdate = true;
        if (this.state.date instanceof Object) {
            this.props.onChangeFixed(this.state.date, this.timeBefore);
        }
    }

    // noinspection JSCheckFunctionSignatures
    componentWillReceiveProps(props) {
        if (this.runtimePropsUpdate) {
            this.setState({
                date: props.date,
            });
        }
    }

    handleShowDate() {
        this.setState({showDate: !this.state.showDate});
    }

    /**
     *
     * @param value {Moment}
     */
    dateChange(value) {
        if (value instanceof Object) {
            if (this.props.onChange(value.toDate())) {
                this.state.date.setFullYear(parseInt(value.format('YYYY')));
                this.state.date.setMonth(parseInt(value.format('M')) - 1);
                this.state.date.setDate(parseInt(value.format('D')));
                this.setState(this.state);
            }
        }
    }

    /**
     *
     * @param value {Moment}
     */
    timeChange(value) {
        if (value instanceof Object) {
            if (this.props.onChange(value.toDate())) {
                // eslint-disable-next-line react/no-direct-mutation-state
                this.state.date = value.toDate();
                this.setState(this.state);
            }


        }
    }

    render() {
        return (
            <ButtonGroup>
                <Button size="sm"
                        title={'Дата'}
                        variant={'outline-secondary'}
                        className={classes['datetime-date-button']}
                        onClick={this.handleShowDate}
                        active={this.state.showDate}
                >
                    <Calendar/>
                </Button>
                <Datetime className={classes['datetime-date'] + ' ' + (!this.state.showDate ? classes['datetime-date-hide'] : '')}
                          value={this.state.date}
                          dateFormat="DD.MM.YYYY"
                          timeFormat={false}
                          onChange={this.dateChange}
                          onOpen={this.handleFocus}
                          onClose={this.handleBlur}
                />
                <Datetime className={classes['datetime-time']}
                          value={this.state.date}
                          dateFormat={false}
                          timeFormat="HH:mm:ss"
                          onChange={this.timeChange}
                          onOpen={this.handleFocus}
                          onClose={this.handleBlur}
                />
            </ButtonGroup>
        )
    }
}

export default TrackTime;
