import React from 'react';
import {Table} from 'react-bootstrap';
import "react-datetime/css/react-datetime.css";
import TrackItemLight from "../TrackItemLight";

/**
 *
 */
const TrackListLight = (props) => {
    return (
        <Table striped bordered hover size="sm">
            <thead>
            <tr>
                <th>#</th>
                <th>Трек</th>
                <th>Старт</th>
                <th>Стоп</th>
                <th>Затрачено</th>
                <th>Тип трека</th>
                <th>Действие</th>
            </tr>
            </thead>
            <tbody>
            {
                props.items.map((item, i) =>
                    <TrackItemLight
                        number={i + 1}
                        name={item.name}
                        dateStart={item.dateStart}
                        dateEnd={item.dateEnd}
                        isActive={item.isActive}
                        type={item.type}
                        id={item.id}
                    />
                )
            }
            </tbody>
        </Table>
    )
}

export default TrackListLight;
