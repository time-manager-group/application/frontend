import React, {useEffect} from 'react';
import Navigation from "../Navigation";
import TrackRuntime from "../TrackRuntime";
import {observer} from "mobx-react";
import classes from './style.module.css';
import TrackWorkSpacePreloader from "../TrackWorkSpacePreloader";
import TrackWorkSpacePreloaderStore from "../../stores/TrackWorkSpacePreloaderStore";
import TrackStore from "../../stores/TrackStore";

/**
 * Область всего приложения.
 */
const Application = observer(({store}) => {
    useEffect(() => {
        document.title = 'Tracker';
    }, []);

    const trackWorkSpacePreloaderStore = TrackWorkSpacePreloaderStore.getInstance();
    const trackStore = TrackStore.getInstance();
   // trackStore.show(false);
  //  trackWorkSpacePreloaderStore.show();
    return (
        <div className={store.isShow ? 'opacity-show' : classes['hide']}>
            <Navigation/>
            <TrackWorkSpacePreloader
                store={trackWorkSpacePreloaderStore}
            />
            <TrackRuntime
                store={trackStore}
            />
        </div>
    )
});


export default Application;