import React from 'react';

import {Button} from "react-bootstrap";
import classes from './style.module.css';
import {TrackToTask} from "../../services/TrackToTask";

/**
 *
 */
const TrackTasks = (props) => {

    const variants = new Map();

    variants.set('useless', 'info');
    variants.set('undefined', 'secondary');
    variants.set('useful', 'success');

    let tasks = new TrackToTask(
        [...props.reportState].reverse().concat(props.taskTemplateList)
    )
        .groupByName();

    return (
        <div className={classes['mb']}>
            {(
                tasks.map((itemTask) => itemTask.types.map((itemType) =>
                        <Button
                            title={'Запустить'}
                            size={'sm'}
                            variant={'outline-' + variants.get(itemType)}
                            className={classes['table-container']}
                            onClick={() => {
                                props.onClick(
                                    {
                                        value: itemTask.name,
                                        type: itemType
                                    }
                                )
                            }}
                        >
                            {itemTask.name}
                        </Button>
                    )
                )
            )}
        </div>
    );
}

export default TrackTasks;
