import React, {PureComponent} from 'react';

import '../../styles/TableItem.css';
import TimeDropper from '../../services/TimeDropper';
import TrackTime from '../TrackTime';
import TrackField from '../TrackField';
import TrackRemoveButton from '../TrackRemoveButton';
import Button from 'react-bootstrap/Button';
import {StopFill} from 'react-bootstrap-icons';
import TrackType from '../TrackTypes';
import EventToasterStore from "../../stores/EventToasterStore";

/**
 *
 */
export class TrackItem extends PureComponent {
    /**
     *
     * @param props {
     * {
     *  number:number
     *  name:string
     *  dateStart:Date,
     *  dateEnd:Date,
     *  isActive:boolean
     *  type:string
     *  item:any
     *  onChange:callback
     *  handleRemove:callback
     *  }
     * }
     */
    constructor(props) {
        super(props);

        this.state = {
            isActive: props.isActive,
            name: props.name,
            dateStart: props.dateStart,
            dateEnd: props.dateEnd,
            type: props.type,
            currentTime: new TimeDropper(
                props.dateStart,
                props.dateEnd)
                .format()
        };

        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleChangeStart = this.handleChangeStart.bind(this);
        this.handleChangeEnd = this.handleChangeEnd.bind(this);
        this.handleStop = this.handleStop.bind(this);
        this.handleRemove = this.handleRemove.bind(this);
        this.handleChangeType = this.handleChangeType.bind(this);
    }

    // noinspection JSCheckFunctionSignatures
    componentWillReceiveProps(props) {
        this.state = {
            isActive: props.isActive,
            name: props.name,
            dateStart: props.dateStart,
            dateEnd: props.dateEnd,
            type: props.type,
            currentTime: new TimeDropper(
                props.dateStart,
                props.dateEnd)
                .format()
        };

        this.setState(this.state);

        if (props.isActive) {
            this.initInterval();
        } else {
            this.unsetInterval();
        }

    }

    handleNameChange(value) {
        this.state.name = value;
        this.props.onChange(this.props.item, this.state);
        this.setState(this.state);
    }

    handleChangeStart(date) {
        if (new TimeDropper(
            date,
            this.state.dateEnd).getDiffSecondsWithoutAbs() < 0) {
            this.validateTimeEvent();

            return;
        }

        this.state.dateStart = date;
        this.state.currentTime = new TimeDropper(
            this.state.dateStart,
            this.state.dateEnd)
            .format();

        this.props.onChange(this.props.item, this.state);
        this.setState(this.state);
    }

    handleChangeEnd(date) {
        if (new TimeDropper(
            this.state.dateStart,
            date).getDiffSecondsWithoutAbs() < 0) {
            this.validateTimeEvent();

            return;
        }
        this.state.dateEnd = date;
        this.state.currentTime = new TimeDropper(
            this.state.dateStart,
            this.state.dateEnd)
            .format();

        this.props.onChange(this.props.item, this.state);
        this.setState(this.state);
    }

    validateTimeEvent() {
        EventToasterStore.getInstance().appendEvent(
            {
                title: 'Ошибка изменения границ трека "' + this.state.name + '"',
                text: 'Невозможно изменить дату трека в отрицательную разность.'
            }
        );
    }

    initInterval() {
        if (!this.interval) {
            this.interval = setInterval(() => {
                this.state.dateEnd = new Date();
                this.state.currentTime = new TimeDropper(
                    this.state.dateStart,
                    this.state.dateEnd)
                    .format();

                this.props.onChange(this.props.item, this.state);
                return this.setState(this.state);
            }, 1000);
        }
    }

    unsetInterval() {
        clearInterval(this.interval);
        this.interval = null;
    }

    componentDidMount() {
        if (this.props.isActive) {
            this.initInterval();
        }
    }

    componentWillUnmount() {
        if (this.props.isActive) {
            this.unsetInterval();
        }
    }

    handleStop() {
        this.unsetInterval();
        this.state.isActive = false;
        this.props.onChange(this.props.item, this.state);
        this.setState(this.state);
    }

    handleRemove() {
        this.handleStop();
        this.props.handleRemove(this.props.item);
    }

    handleChangeType(type) {
        this.state.type = type;
        this.props.onChange(this.props.item, this.state);
    }

    render() {
        let item;

        if (this.props.isActive) {
            item = (<tr className="primary">
                <td>
                    {this.props.number}
                </td>
                <td>
                    <TrackField
                        value={this.state.name}
                        onChange={this.handleNameChange}
                    />
                </td>
                <td>
                    <TrackTime date={this.state.dateStart} onChange={this.handleChangeStart}/>
                </td>
                <td colSpan={2}>
                    {this.state.currentTime}
                </td>
                <td>
                    <TrackType
                        focus={this.state.type}
                        onChange={this.handleChangeType}
                    />
                </td>
                <td>
                    <Button
                        variant="secondary"
                        size="sm"
                        title={'Остановить'}
                        onClick={this.handleStop}
                    >
                        <StopFill/>
                    </Button>{' '}
                    <TrackRemoveButton
                        number={this.state.number}
                        name={this.state.name}
                        dateStart={this.state.dateStart}
                        dateEnd={this.state.dateEnd}
                        handleRemove={this.handleRemove}
                        item={this.props.item}
                    />
                </td>
            </tr>)
        } else {
            item = (
                <tr>
                    <td>
                        {this.props.number}
                    </td>
                    <td>
                        <TrackField
                            value={this.props.name}
                            onChange={this.handleNameChange}
                        />
                    </td>
                    <td>
                        <TrackTime date={this.state.dateStart} onChange={this.handleChangeStart}/>
                    </td>
                    <td>
                        <TrackTime date={this.state.dateEnd} onChange={this.handleChangeEnd}/>
                    </td>
                    <td>
                        {this.state.currentTime}
                    </td>
                    <td>
                        <TrackType
                            focus={this.state.type}
                            onChange={this.handleChangeType}
                        />
                    </td>
                    <td>
                        <TrackRemoveButton
                            number={this.props.number}
                            name={this.props.name}
                            dateStart={this.state.dateStart}
                            dateEnd={this.state.dateEnd}
                            handleRemove={this.handleRemove}
                        />
                    </td>
                </tr>
            )
        }

        return (
            item
        );
    }
}

export default TrackItem;
