import '../../styles/TableItem.css';
import Button from 'react-bootstrap/Button';
import {Modal, Spinner} from 'react-bootstrap';
import {Form} from 'react-bootstrap';
import {observer} from "mobx-react";
import classes from './style.module.css';

/**
 * Форма сознания нового отчета.
 */
const CreateReportModal = observer(({store}) => {
    const handleClose = () => {
        store.close()
    };

    const handleCreate = () => {
        store.make();
    };

    return (
        <Modal className={classes['modal']} show={store.isShow} onHide={handleClose}>
            <Modal.Header closeButton={!store.isRequiredStep}>
                <Modal.Title>Новый отчет</Modal.Title>
            </Modal.Header>
            <div className={classes['preloader'] + ' ' + (store.isReady ? classes['preloader-hidden'] : '')}>
                <div className={classes['preloader-container']}>
                    <Spinner animation="border" variant="secondary"/>
                </div>
            </div>
            <div className={!store.isReady ? classes['body-hidden'] : ''}>
                <Modal.Body>
                    <Form.Group>
                        <Form.Label>Введите наименование отчета</Form.Label>
                        <Form.Control
                            type="text"
                            placeholder="Наименование отчета"
                            value={store.reportName}
                            onChange={
                                (event) => store.reportNameChange(event.target.value)
                            }
                        />
                    </Form.Group>
                </Modal.Body>
                <Modal.Footer>
                    <Button hidden={store.isRequiredStep} variant="secondary" onClick={handleClose}>
                        Отмена
                    </Button>
                    <Button variant="primary" disabled={!store.isPermissionCreateReport} onClick={handleCreate}>
                        Создать
                    </Button>
                </Modal.Footer>
            </div>
        </Modal>
    )
});

export default CreateReportModal;
