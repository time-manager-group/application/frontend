import React, {memo, useState} from 'react';

import '../../styles/TableItem.css';
import Button from 'react-bootstrap/Button';
import {X} from 'react-bootstrap-icons';
import {Badge, Modal, Spinner} from 'react-bootstrap';
import moment from 'moment/moment';
import TimeDropper from '../../services/TimeDropper';
import classes from './style.module.css';
import TrackStore from "../../stores/TrackStore";
import ReportStore from "../../stores/ReportStore";

/**
 *
 */
const TrackRemoveButton = (props) => {
    const [show, setShow] = useState(false);
    const [blocked, setBlocked] = useState(false);
    const store = TrackStore.getInstance();

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    const handleRemove = () => {
        setBlocked(true);
        // не передаю свойством потому, что id отчета,т.к. при выборе
        // другого отчета не устанавливается на новый.
        const reportId = ReportStore.getInstance().getSelectedReportId();

        store.removeTrack(reportId, props.id, () => {
            props.handleRemove();
            setBlocked(false);
            setShow(false);
        }, () => {
            setBlocked(false);
            setShow(false);
        });
    };


    const start = moment(props.dateStart).format('HH:mm:ss');
    const end = moment(props.dateEnd).format('HH:mm:ss')
    const time = new TimeDropper(props.dateStart, props.dateEnd).format();


    return (
        <>
            <Button
                variant="danger"
                size="sm"
                title={'Удалить'}
                onClick={handleShow}
            >
                <X/>
            </Button>

            <Modal show={show} onHide={handleClose}>
                <Modal.Header closeButton>
                    <Modal.Title>Удаление трека</Modal.Title>
                </Modal.Header>
                <Modal.Body>Трек <Badge variant="dark">{props.number}</Badge> <b>
                    <Badge variant="secondary">{props.name} </Badge> {start} - {end}
                </b> <Badge variant="primary">
                    {time}
                </Badge> - удалить его?
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleClose}>
                        Отмена
                    </Button>
                    <Button variant="primary" onClick={handleRemove} disabled={blocked}>
                        {blocked ? (<Spinner className={classes['preloader-spinner-style']} animation="border" variant="dark"/>) :
                            'Удалить'}
                    </Button>
                </Modal.Footer>
            </Modal>
        </>
    )

}

export default memo(TrackRemoveButton);
