import React from 'react';
import {ListGroup, Button} from 'react-bootstrap';
import TrackField from '../../TrackField';
import classes from './style.module.css';
import {Draggable} from 'react-beautiful-dnd';
import {EyeSlashFill} from "react-bootstrap-icons";
import {P_CREATE, P_INTERNAL_DND, P_READ, P_TO_EXTERNAL_DND, P_UPDATE} from '../components/Policies';
import {THEME_COLORS} from '../../../services/Colors';


/**
 * @param props {{id:string, name:string, variant:string, items: OriginRow[], visibleLimit?: number|undefined|null, provided: {placeholder: JSX.Element|null},
 * store: NoteStore,
 * actionManager: ActionManager,
 * cursorManager: {
 *     setInputRef: function(string|number|null, RefObject<TrackField>): void,
 *     setCurrentSelection: function(number|null): void,
 *     setCurrentIndex: function(string|number|null): void,
 * }}}
 * @returns {JSX.Element}
 * @constructor
 */
const Group = ({
                   id,
                   name,
                   variant,
                   items,
                   visibleLimit,
                   provided,
                   store,
                   actionManager,
                   cursorManager
               }) => {

    const label = (mapIndex) => {
        return (
            <h4 className={classes.no_select}
                style={{marginBottom: "2px", minWidth: `${1.5 + (Math.max(((items.length + '').length) - 2, 0) * 0.5)}em`}}>
                {mapIndex + 1}.
            </h4>
        );
    };

    return (<ListGroup.Item variant={variant} className={classes.group_body}>
            <div className={classes.group_input}>
                <TrackField value={name} id={id} readOnly={true} onChange={() => {
                }} onChangeFixed={() => {
                }}/>
            </div>
            {store.checkPolicyByGroup(id, P_CREATE) && (
                <Button disabled={!(items.length === 0 || items[0].name !== '')} variant="secondary" size="lg" className={classes.add_btn} onClick={() => {
                    const newCurrentRow = store.addNewRowToFirst(id);
                    if (!newCurrentRow) {
                        return;
                    }
                    cursorManager.setCurrentSelection(null);
                    cursorManager.setCurrentIndex(newCurrentRow.id);
                }}>
                    +
                </Button>)}
            <ListGroup>
                {
                    !store.checkPolicyByGroup(id, P_READ) ?
                        (<div style={{textAlign: "center"}}><EyeSlashFill></EyeSlashFill></div>)
                        : items.slice(0, visibleLimit ? visibleLimit : undefined)
                            .map((mapRow, mapIndex) => (
                                <Draggable key={mapRow.id} draggableId={mapRow.id} index={mapIndex}
                                           isDragDisabled={mapRow.name === '' || function() {
                                               if (store.checkPolicyByGroup(id, P_TO_EXTERNAL_DND)) {
                                                   return false;
                                               }
                                               return !store.checkPolicyByGroup(id, P_INTERNAL_DND)
                                           }()}>
                                    {(provided) => (
                                        <div
                                            ref={provided.innerRef}
                                            {...provided.draggableProps}
                                            {...provided.dragHandleProps}
                                            style={{...provided.draggableProps.style, display: "flex", alignItems: "flex-end"}}
                                        >
                                            {label(mapIndex)}
                                            <ListGroup.Item className={classes.point_input}>
                                                <TrackField
                                                    ref={(el) => cursorManager.setInputRef(mapRow.id, el)}
                                                    id={mapRow.id}
                                                    value={mapRow.name}
                                                    readOnly={!(store.checkPolicyByGroup(id, P_UPDATE) || (store.checkPolicyByGroup(id, P_CREATE) && mapRow?.isNew))}
                                                    onBlur={(e, id) => store.clearRowsByRow(id)}
                                                    onChange={(value, id) => store.changeRowName(id, value)}
                                                    onChangeFixed={(value, valueBefore, id) => store.changeRowName(id, value, true)}
                                                    onEmpty={(e, id) => {
                                                        const newCurrentRow = store.deleteRow(id);
                                                        if (newCurrentRow) {
                                                            cursorManager.setCurrentSelection(newCurrentRow.name.length);
                                                            cursorManager.setCurrentIndex(newCurrentRow.id);
                                                        }
                                                        e.preventDefault();
                                                    }}
                                                    onPress={(e, id) => {
                                                        const newCurrentRow = store.addNewRowToAfter(id);
                                                        if (!newCurrentRow) {
                                                            return;
                                                        }
                                                        newCurrentRow.id === id ? (e.stopOnBlur = true) : cursorManager.setCurrentSelection(null)
                                                        cursorManager.setCurrentIndex(newCurrentRow.id);
                                                    }}
                                                    onButtonAction={(e, id) => {
                                                        const newCurrentRow = actionManager.handleByHotKey(e, id);
                                                        if (!newCurrentRow) {
                                                            return;
                                                        }
                                                        newCurrentRow.newSection !== null && cursorManager.setCurrentSelection(newCurrentRow.newSection);
                                                        cursorManager.setCurrentIndex(newCurrentRow.row.id);
                                                    }}
                                                    style={{...(mapRow?.isNew ? {backgroundColor: THEME_COLORS['light-success-2']} : {})}}
                                                />
                                            </ListGroup.Item>
                                        </div>
                                    )}
                                </Draggable>
                            ))
                }
                {provided.placeholder}
                {(store.checkPolicyByGroup(id, P_READ) && visibleLimit < items.length) ? (<h3>...</h3>) : ''}
            </ListGroup>
            {items.length > 0 && store.checkPolicyByGroup(id, P_CREATE) && (
                <Button disabled={!(items.length === 0 || items[items.length - 1].name !== '')} variant="secondary" size="lg" className={classes.add_btn}
                        onClick={() => {
                            const newCurrentRow = store.addNewRowToLast(id);
                            if (!newCurrentRow) {
                                return;
                            }
                            cursorManager.setCurrentSelection(null);
                            cursorManager.setCurrentIndex(newCurrentRow.id);
                        }}>
                    +
                </Button>)
            }
        </ListGroup.Item>
    )
};

export default Group;