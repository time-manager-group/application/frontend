import React from 'react';
import {Modal, Spinner} from "react-bootstrap";
import {observer} from 'mobx-react';


/**
 * Главный прелодер.
 */
const Preloader = observer(({store}) => {
    return (
        <Modal show={store.isShow} onHide={() => {
        }}>
            <Modal.Header>
                <Modal.Title>Loading data...</Modal.Title>
            </Modal.Header>
            <Modal.Body> <Spinner
                as="span"
                animation="border"
                size="sm"
                role="status"
                aria-hidden="true"
            /> Loading...</Modal.Body>
        </Modal>
    )
});


export default Preloader;