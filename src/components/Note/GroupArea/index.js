import React, {useEffect, useState} from 'react';

import {DragDropContext, Droppable} from 'react-beautiful-dnd';
import {P_FROM_EXTERNAL_DND, P_INTERNAL_DND, P_TO_EXTERNAL_DND} from '../components/Policies';
import ActionManager from '../components/ActionManager';
import classes from './style.module.css';
import Group from '../Group';
import {observer} from 'mobx-react';
import {toJS} from 'mobx';

const GroupArea = observer(({store}) => {
          const groups = toJS(store.groups);
          const grid   = toJS(store.grid);

          const mapGroup = {};
          for (const [index, group] of groups.entries()) {
              mapGroup[group.id] = {...group, index};
          }

          const actionManager                           = new ActionManager(store);
          const inputRefs                               = [];
          const [currentIndex, setCurrentIndex]         = useState('');
          const [currentSelection, setCurrentSelection] = useState(null);
          const [currentDragRow, setCurrentDragRow]     = useState(null);
          const handleDragStart                         = (result) => {
              setCurrentDragRow(result.draggableId);
          };

          const onDragEnd = (result) => {
              const {source, destination} = result;
              // Если элемент был перемещен на то же место
              if (!destination) {
                  setCurrentDragRow(null);
                  return;
              }
              store.moveRow(
                  {
                      groupId: source.droppableId,
                      index:   source.index
                  },
                  {
                      groupId: destination.droppableId,
                      index:   destination.index
                  })
              setCurrentDragRow(null);
          };

          // noinspection JSUnusedGlobalSymbols
          const cursorManager = {
              setInputRef:         (id, ref) => inputRefs[id] = ref,
              setCurrentSelection: (position) => setCurrentSelection(position),
              setCurrentIndex:     (id) => setCurrentIndex(id)
          };

          useEffect(() => {
              if (currentIndex && inputRefs[currentIndex]) {
                  inputRefs[currentIndex].inputRef.current.focus();
                  const inputElement = inputRefs[currentIndex].inputRef.current;
                  inputElement.setSelectionRange(currentSelection, currentSelection);
                  //Костыль для `слегка` отложенного выставления курсора
                  setTimeout(() => {
                      inputElement.setSelectionRange(currentSelection, currentSelection);
                  }, 0);
                  setCurrentSelection(null)
                  setCurrentIndex(null)
              }
          }, [groups, currentIndex]);

          const isDropDisabled = (groupId) => {
              if (!currentDragRow) {
                  return true;
              }
              const row = store.getRow(currentDragRow);

              if (row.groupId !== groupId && !store.checkPolicyByGroup(row.groupId, P_TO_EXTERNAL_DND)
              ) {
                  return true;
              }
              if (row.groupId !== groupId && !store.checkPolicyByGroup(groupId, P_FROM_EXTERNAL_DND)
              ) {
                  return true;
              }
              if (row.groupId !== groupId && !store.checkRelations(row.groupId, groupId)
              ) {
                  return true;
              }
              if (row.groupId === groupId && !store.checkPolicyByGroup(groupId, P_INTERNAL_DND)
              ) {
                  return true;
              }
          }

          const makeGroupProps = ({id, name, variant, items, visibleLimit}, provided) => {
              return {id, name, variant, items, visibleLimit, provided, store, actionManager, cursorManager};
          };

          return (
              <DragDropContext onDragEnd={onDragEnd} onDragStart={handleDragStart}>
                  <table className={classes.grid}>
                      {
                          grid.rows.map((row) => (
                              <tr>
                                  {
                                      row.columns.map((row) => {
                                          const group = row?.groupId ? mapGroup[row.groupId] : null;
                                          return (
                                              <td colSpan={row?.colSpan} rowSpan={row?.rowSpan}
                                                  style={{
                                                      ...(row?.height ? {
                                                          height: `${row.height * 130}px`
                                                      } : {height: "100%"})
                                                  }}>
                                                  {group ? <React.Fragment key={group.id}>
                                                      <Droppable droppableId={group.id} index={group.index} isDropDisabled={isDropDisabled(group.id)}>
                                                          {(provided) => (
                                                              <div {...provided.droppableProps} ref={provided.innerRef} className={classes.group_provided_body}>
                                                                  <Group {...makeGroupProps(group, provided)}/>
                                                              </div>
                                                          )}
                                                      </Droppable>
                                                  </React.Fragment> : ''}
                                              </td>
                                          );
                                      })
                                  }
                              </tr>
                          ))
                      }
                  </table>
              </DragDropContext>
          )
      })
;

export default GroupArea;