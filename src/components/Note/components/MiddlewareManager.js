export const MW_OUTBOX = 'outbox';

export default class MiddlewareManager {
    /**
     * @var {NoteStorage}
     * @private
     */
    __storage;

    /**
     * @param storage {NoteStorage}
     */
    constructor(storage) {
        this.__storage = storage;
    }

    /**
     * @param store {NoteStore}
     * @param groupId {string}
     * @param rowId {string|null}
     * @param action {string}
     * @param args {*}
     * @return boolean
     */
    executeBefore(store, groupId, rowId, action, args = null) {
        return this.__execute(store, groupId, rowId, action, args, 'before');
    }

    /**
     * @param store {NoteStore}
     * @param groupId {string}
     * @param rowId {string|null}
     * @param action {string}
     * @param args {*}
     * @return boolean
     */
    executeAfter(store, groupId, rowId, action, args = null) {
        return this.__execute(store, groupId, rowId, action, args, 'after');
    }

    /**
     * @param store {NoteStore}
     * @param groupId {string}
     * @param rowId {string|null}
     * @param action {string}
     * @param args {*}
     * @return boolean
     * @param position {"before"|"after"}
     * @private
     */
    __execute(store, groupId, rowId, action, args, position) {
        const group = store.getGroup(groupId);
        for (const middleware of group?.middlewares ?? []) {
            switch (middleware.type) {
                case MW_OUTBOX:
                    if (!this.__middlewareOutBox(middleware?.args, store, group, rowId, action, position, args)) {
                        return false;
                    }
                    break;
            }
        }
        return true;
    }

    // noinspection JSUnusedLocalSymbols
    /**
     * @param args {*}
     * @param store {NoteStore}
     * @param group {FoundedGroup}
     * @param rowId {string|null}
     * @param action {string}
     * @param actionArgs {*}
     * @param position {"before"|"after"}
     * @return boolean
     * @private
     */
    __middlewareOutBox(args, store, group, rowId, action, position, actionArgs) {
        if (position !== 'after') {
            return true;
        }

        const peakCount = Number(args?.peakCount);
        if (peakCount < 0) {
            return true;
        }

        const moveCount = Math.max(Number(args?.moveCount), 1);

        if (group.items.filter(row => !row?.isNew).length > peakCount) {
            const groups = store.groups;

            const newItems     = [...group.items];
            let itemsForMoving = [];
            switch (args.positionFrom) {
                case "last":
                    itemsForMoving = newItems.splice(-1, moveCount);
                    break
                case "first":
                    itemsForMoving = newItems.splice(0, moveCount);
                    itemsForMoving.reverse();
                    break
                default:
                    return true;
            }
            groups[group.index].items = newItems;
            store.fillGroups(groups);
            for (const itemForMoving of itemsForMoving) {
                this.__storage.moveRowToBackup({groupId: group.id, rowId: itemForMoving.id});
            }

            return true;
        }

        return true;
    }
}