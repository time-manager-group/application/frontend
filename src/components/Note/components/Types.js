/** @typedef {{
 id: string,
 name: string,
 items: OriginRow[],
 middlewares: any[],
 polities: string[],
 index: number
 }|null} FoundedGroup */
/** @typedef {{id: string, name: string, isNew?: boolean, groupId: string, index:number}} FoundedRow */
/** @typedef {{
 id: string,
 name: string,
 middlewares: any[],
 items: OriginRow[],
 polities: string[],
 actions: any[]
 }} OriginGroup */
/** @typedef {{id: string, name: string, isNew?: boolean}} OriginRow */
/** @typedef {{row: OriginRow, newSection: number|null}} CurrentRow */