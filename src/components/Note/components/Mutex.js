export class Mutex {
    constructor() {
        this.queue = [];
        this.locked = false;
    }

    lock() {
        const unlockNext = () => {
            this.locked = false;
            if (this.queue.length > 0) {
                const nextResolve = this.queue.shift();
                nextResolve(unlockNext);
            }
        };

        return new Promise(resolve => {
            if (this.locked) {
                this.queue.push(resolve);
            } else {
                this.locked = true;
                resolve(unlockNext);
            }
        });
    }
}