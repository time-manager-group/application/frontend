import {P_CREATE, P_DELETE, P_FROM_EXTERNAL_MOVE, P_INTERNAL_SWAP, P_TO_EXTERNAL_MOVE, P_UPDATE} from './Policies';

export const A_MOVE_TO_GROUP = 'move_to_group';
export const A_SWAP_INTERNAL = 'swap_internal';

const A_SYSTEM_REMOVE        = 'system_remove';
const A_SYSTEM_FOCUS_TO_BACK = 'system_focus_to_back';
const A_SYSTEM_FOCUS_TO_NEXT = 'system_focus_to_next';

/** @typedef {{
 name:    string,
 type:    string,
 args: {},
 hotKeys: HotKey[]
 }} Action */
/** @typedef {MouseEvent|KeyboardEvent|{nativeEvent:MouseEvent|KeyboardEvent}} KeyEvent */
/** @typedef {{
 isCtrl?: boolean,
 isShift?: boolean,
 isAlt?: boolean,
 keys?: string[],
 mouseButtons?: number[],
 }} HotKey */

export default class ActionManager {
    constructor(store) {
        /**
         * @typedef {Object.<string, Object.<string, Action>>}
         */
        this.hotKeysActions = {};
        /**
         * @type {NoteStore}
         */
        this.__store = store;
        for (const group of this.__store.getGroups()) {
            if (group.hasOwnProperty('actions')) {
                for (const action of [...ActionManager.__getSystemActions(), ...group.actions]) {
                    if (action.hotKeys) {
                        for (const hotKey of action.hotKeys) {
                            if (!this.hotKeysActions.hasOwnProperty(group.id)) {
                                this.hotKeysActions[group.id] = {};
                            }
                            this.hotKeysActions[group.id][getHotKeyHash(hotKey)] = action;
                        }
                    }
                }
            }
        }
    }

    /**
     * @private
     * @returns {Action[]}
     */
    static __getSystemActions() {
        return [
            {
                name:    'Remove',
                type:    A_SYSTEM_REMOVE,
                args:    {},
                hotKeys: [
                    makeEqualKey({keys: ['Backspace']}),
                    makeEqualKey({keys: ['Delete']})
                ]
            },
            {
                name:    'Focus to back',
                type:    A_SYSTEM_FOCUS_TO_BACK,
                args:    {},
                hotKeys: [
                    makeEqualKey({keys: ['ArrowUp']}),
                    makeEqualKey({keys: ['ArrowLeft']})
                ]
            },
            {
                name:    'Focus to next',
                type:    A_SYSTEM_FOCUS_TO_NEXT,
                args:    {},
                hotKeys: [
                    makeEqualKey({keys: ['ArrowDown']}),
                    makeEqualKey({keys: ['ArrowRight']})
                ]
            }
        ];
    }

    /**
     * @param e {KeyEvent}
     * @param id {string}
     * @return {CurrentRow|null}
     */
    handleByHotKey(e, id) {
        const row = this.__store.getRow(id);
        if (!row) {
            return null;
        }
        const {groupId} = row;
        if (!this.hotKeysActions.hasOwnProperty(groupId)) {
            return null;
        }
        const hash = getHotKeyHashByEvent(e);
        if (!this.hotKeysActions[groupId][hash]) {
            return null;
        }

        const action = this.hotKeysActions[groupId][hash];

        switch (action.type) {
            case A_SYSTEM_REMOVE:
                return this.__removeRow(e, row);
            case A_SYSTEM_FOCUS_TO_BACK:
                return this.__focusToBack(e, row);
            case A_SYSTEM_FOCUS_TO_NEXT:
                return this.__focusToNext(e, row);
            case A_MOVE_TO_GROUP:
                return this.__moveToGroup(e, row, action.args);
            case A_SWAP_INTERNAL:
                return this.__swapInternal(e, row, action.args);
        }

        return null;
    }

    /**
     * @param e {KeyEvent}
     * @param row {FoundedRow}
     * @return {CurrentRow|null}
     * @private
     */
    __removeRow(e, row) {
        if (!(this.__store.checkPolicyByGroup(row.groupId, P_DELETE)
            && !this.__store.checkPolicyByGroup(row.groupId, P_UPDATE)
            && !row.isNew)) {
            return null;
        }

        const currentRow = this.__store.deleteRow(row.id);
        if (!currentRow) {
            return null;
        }
        return {row: currentRow, newSection: currentRow.name.length};
    }

    /**
     * @param e {KeyEvent}
     * @param row {FoundedRow}
     * @param args {{id: string, position: string}}
     * @return {CurrentRow|null}
     * @private
     */
    __moveToGroup(e, row, args) {
        if (!e.target.value) {
            return null;
        }
        if (!this.__store.checkPolicyByGroup(row.groupId, P_TO_EXTERNAL_MOVE)) {
            return null;
        }
        if (!this.__store.checkPolicyByGroup(args.id, P_FROM_EXTERNAL_MOVE)) {
            return null;
        }
        if (!this.__store.checkRelations(row.groupId, args.id)) {
            return null;
        }

        let destinationIndex = null;
        switch (args.position) {
            case 'first':
                destinationIndex = 0;
                break;
            case 'last':
                destinationIndex = this.__store.getGroup(args.id).items.length;
                break;
        }

        this.__store.moveRow(
            {
                groupId: row.groupId,
                index:   row.index
            },
            {
                groupId: args.id,
                index:   destinationIndex
            });

        e.preventDefault();

        const {items: newSourceItems} = this.__store.getGroup(row.groupId);
        if (newSourceItems.length === 0) {
            return null;
        }
        if (newSourceItems.length === 1) {
            const newCurrentRow = newSourceItems[0];
            return {row: newCurrentRow, newSection: newCurrentRow.name.length};
        }
        if (row.index === newSourceItems.length) { // Если это был последний элемент - то выбираем предпоследний для фокуса
            const newCurrentRow = newSourceItems[newSourceItems.length - 1];
            return {row: newCurrentRow, newSection: newCurrentRow.name.length};
        } else { // Иначе ставим того, кто попадает на индекс по прошлому элементу
            const newCurrentRow = newSourceItems[row.index];
            return {row: newCurrentRow, newSection: newCurrentRow.name.length};
        }
    }

    /**
     * @param e {KeyEvent}
     * @param row {FoundedRow}
     * @param args {{with: string}}
     * @return {CurrentRow|null}
     * @private
     */
    __swapInternal(e, row, args) {
        if (!e.target.value) {
            return null;
        }
        if (!this.__store.checkPolicyByGroup(row.groupId, P_INTERNAL_SWAP)) {
            return null;
        }

        const {items: itemRows} = this.__store.getGroupByRow(row.id);
        const cleanedItemRows   = itemRows.filter(function(item) {
            return item.name !== '';
        });

        let sourceRow        = row;
        let sourceIndex      = null;
        let destinationIndex = null;

        switch (args.with) {
            case 'after':
                for (const [key, itemRow] of cleanedItemRows.entries()) {
                    if (itemRow.id === row.id && (key + 1 === cleanedItemRows.length)) {
                        return {row, newSection: null};
                    }
                }
                for (const [key, itemRow] of itemRows.entries()) {
                    if (itemRow.id === row.id) {
                        sourceIndex      = key;
                        destinationIndex = key + 1;
                        break;
                    }
                }
                break;
            case 'before':
                for (const [key, itemRow] of cleanedItemRows.entries()) {
                    if (itemRow.id === row.id && key === 0) {
                        return {row, newSection: null};
                    }

                }
                for (const [key, itemRow] of itemRows.entries()) {
                    if (itemRow.id === row.id) {
                        sourceIndex      = key;
                        destinationIndex = key - 1;
                        break;
                    }
                }
                break;
        }

        this.__store.moveRow(
            {
                groupId: sourceRow.groupId,
                index:   sourceIndex
            },
            {
                groupId: sourceRow.groupId,
                index:   destinationIndex
            });

        return {row: sourceRow, newSection: null};
    }

    /**
     * @param e {KeyEvent}
     * @param row {FoundedRow}
     * @return {CurrentRow|null}
     * @private
     */
    __focusToBack(e, row) {
        if (!isKeyboard(e)) {
            return null;
        }
        if (isSelectionText(e)) {
            return null;
        }
        let newSelection = null;
        switch (e.key) {
            case 'ArrowUp':
                newSelection = e.target.selectionStart;
                break;
            case 'ArrowLeft':
                if (e.target.selectionStart !== 0) {
                    return null;
                }
                newSelection = row.name.length;
                break;
            default:
                return null;
        }

        const {items: currentItems} = this.__store.getGroup(row.groupId);

        if (row.index === 0 && row.name === '') { //Первый пустой
            return null;
        }
        if (row.index === 0) { //Первый не пустой
            if (!this.__store.checkPolicyByGroup(row.groupId, P_CREATE)) {
                return null;
            }
            return {row: this.__store.addNewRowToFirst(row.groupId), newSection: null}
        }

        return {row: currentItems[row.index - 1], newSection: newSelection};
    }

    /**
     * @param e {KeyEvent}
     * @param row {FoundedRow}
     * @return {CurrentRow|null}
     * @private
     */
    __focusToNext(e, row) {
        if (!isKeyboard(e)) {
            return null;
        }
        if (isSelectionText(e)) {
            return null;
        }
        let newSelection = null;
        switch (e.key) {
            case 'ArrowDown':
                newSelection = e.target.selectionStart;
                break;
            case 'ArrowRight':
                if (e.target.selectionStart !== row.name.length) {
                    return null;
                }
                newSelection = 0;
                break;
            default:
                return null;
        }

        const {items: currentItems} = this.__store.getGroup(row.groupId);

        if (row.name === '' && (row.index === currentItems.length - 1)) { //Последний пустой
            return null;
        }
        if ((row.index === currentItems.length - 1)) { //Последний не пустой
            if (!this.__store.checkPolicyByGroup(row.groupId, P_CREATE)) {
                return null;
            }
            return {row: this.__store.addNewRowToLast(row.groupId), newSection: null}
        }

        return {row: currentItems[row.index + 1], newSection: newSelection};
    }
}

/**
 * @param e {KeyEvent}
 * @return {boolean}
 */
const isSelectionText = (e) => {
    return e.target.selectionStart !== e.target.selectionEnd;
}

/**
 *
 * @param e {KeyEvent}
 * @returns {string}
 */
const getHotKeyHashByEvent = (e) => {
    return getHotKeyHash({
        isCtrl:  e.ctrlKey,
        isAlt:   e.altKey,
        isShift: e.shiftKey,
        ...(isKeyboard(e) ? {keys: [e.code]} : {mouseButtons: [e.button]})
    });
}


/**
 * @param e {KeyEvent}
 * @return {boolean}
 */
const isKeyboard = (e) => {
    return e.nativeEvent instanceof KeyboardEvent;
}

/**
 * @param hotKey {HotKey}
 * @return {HotKey}
 */
const makeEqualKey = (hotKey) => {
    const equalHotKey = {isCtrl: false, isShift: false, isAlt: false, keys: [], mouseButtons: []};
    for (const param in hotKey) {
        equalHotKey[param] = hotKey[param];
    }
    return equalHotKey;
}

/**
 * @param hotKey {HotKey}
 * @return {string}
 */
const getHotKeyHash = (hotKey) => {
    const key = [];
    if (hotKey.hasOwnProperty('isCtrl') && hotKey.isCtrl) {
        key.push('ctrl');
    }
    if (hotKey.hasOwnProperty('isShift') && hotKey.isShift) {
        key.push('shift');
    }
    if (hotKey.hasOwnProperty('isAlt') && hotKey.isAlt) {
        key.push('alt');
    }
    if (hotKey.hasOwnProperty('keys') && (hotKey.keys.length)) {
        key.push('keyboard');
        let keyCodes = hotKey.keys.slice();
        keyCodes.sort((a, b) => a - b);
        for (const keyCode of keyCodes) {
            key.push(keyCode + '');
        }
    }
    if (hotKey.hasOwnProperty('mouseButtons') && (hotKey.mouseButtons.length)) {
        key.push('mouse');
        let mouseButtons = hotKey.mouseButtons.slice();
        mouseButtons.sort((a, b) => a - b);
        for (const mouseButton of mouseButtons) {
            key.push(mouseButton + '');
        }
    }

    return key.join('_');
}