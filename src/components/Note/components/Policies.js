export const P_ALL                = '*';
export const P_READ               = 'read';
export const P_CREATE             = 'create';
export const P_UPDATE             = 'update';
export const P_DELETE             = 'delete';
export const P_INTERNAL_SWAP      = 'internal_swap';
/**
 *     | |__| |
 * =>  | |__| |
 *     | |__| |
 */
export const P_FROM_EXTERNAL_MOVE = 'from_external_move';
/**
 *     | |__| |
 *     | |__| | =>
 *     | |__| |
 */
export const P_TO_EXTERNAL_MOVE   = 'to_external_move';
export const P_INTERNAL_DND       = 'internal_dnd';
/**
 *     | |__| |
 * =>  | |__| |
 *     | |__| |
 */
export const P_FROM_EXTERNAL_DND  = 'from_external_dnd';
/**
 *     | |__| |
 *     | |__| | =>
 *     | |__| |
 */
export const P_TO_EXTERNAL_DND    = 'to_external_dnd';