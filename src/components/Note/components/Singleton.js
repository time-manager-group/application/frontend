export class Singleton {
    /**
     *
     * @type {{}}
     * @private
     */
    static __instances = {};

    /**
     * @param key {string}
     * @param initCallback { function():any }
     */
    static getInstance(key, initCallback = null) {
        if (!this.__instances.hasOwnProperty(key)) {
            if (initCallback) {
                this.__instances[key] = initCallback();
            } else {
                throw Error(`No registered service: ${key}`);
            }
        }
        return this.__instances[key];
    }

    static register(key, initCallback) {
        this.getInstance(key, initCallback);
    }
}

export default Singleton