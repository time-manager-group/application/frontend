import React, {useEffect} from 'react';


import NoteStore from '../stores/NoteStore';
import {P_ALL, P_FROM_EXTERNAL_DND, P_FROM_EXTERNAL_MOVE, P_INTERNAL_DND, P_INTERNAL_SWAP, P_READ, P_TO_EXTERNAL_DND, P_TO_EXTERNAL_MOVE, P_UPDATE} from '../components/Policies';
import {A_MOVE_TO_GROUP, A_SWAP_INTERNAL} from '../components/ActionManager';
import './style.module.css';
import GroupArea from '../GroupArea';
import Navigation from '../Navigation';
import MiddlewareManager, {MW_OUTBOX} from '../components/MiddlewareManager';
import NoteStorageManager from '../services/NoteStorageManager';
import QueueSyncWorker from '../services/QueueSyncWorker';
import Queue from '../services/Queue';
import NoteApiController from '../services/NoteApiController';
import StorageStatStore from '../stores/StorageStatStore';
import Preloader from '../Preloader';
import Singleton from '../components/Singleton';
import NoteApi from '../services/NoteApi';
import EventToaster from '../../EventToaster';
import EventToasterStore from '../../../stores/EventToasterStore';
import PreloaderStore from '../stores/PreloaderStore';


const WorkSpace = () => {
          useEffect(() => {
              document.title = 'Note';
          }, []);

          const eventToasterStore = Singleton.getInstance('event-toaster-store', () => {
              return new EventToasterStore();
          });

          const fastPlan    = '066736c4-8503-4b35-b2f3-e92b218fd465';
          const success     = '5cb1dc6c-8d98-4be1-a0a1-68ac725199e2';
          const meOnReview  = '35a21638-b478-46fb-80b2-382224be1846';
          const backLog     = '10eecb64-b39f-44db-a1ba-d075378fb6b0';
          const locked      = 'ce2a9d94-2ac2-4ef7-9171-d573a27821bb';
          const release     = '472ab5f2-399a-48b1-961c-a9672bcdc5fd';
          const older       = 'e3f43197-ac79-4943-8664-1f54bbf95494';
          const discussion  = 'bb1ec804-006d-42fe-b6b6-ed5776307e61';
          const createTask  = '6a36fa70-995f-40b4-86ec-350a8ec20805';
          const createdTask = 'b42109ca-864c-467a-9ce7-16ab7c1b51af';

          /**
           * @type {NoteApi}
           */
          const client = Singleton.getInstance('note-api', () => {
              const instance = new NoteApi();
              instance.addHandleFail((error) => {
                  eventToasterStore.appendEvent({
                      title: 'Note service is unavailable',
                      text:  'Unable to get response from Note service, ' + error
                  });
              })
              return instance;
          });

          /**
           * @type {StorageStatStore}
           */
          const storageStatStore = Singleton.getInstance('note-storage-stat-store', () => {
              return new StorageStatStore();
          });

          const storage = Singleton.getInstance('note-storage', () => {
              const controller = new NoteApiController(client);
              const queue      = new Queue();
              const worker     = new QueueSyncWorker(queue,
                  {
                      minDelay:                 300,
                      delayCoefficient:         1,
                      delayCoefficientMultiple: 50,
                      usePeakDelayOnProcess:    true,
                      maxCalcDelay:             800
                  });
              return new NoteStorageManager(client, queue, worker, controller, storageStatStore);
          });

          const swapInternal = [
              {
                  name:    "Поменять с предыдущим местами",
                  type:    A_SWAP_INTERNAL,
                  args:    {
                      with: 'before'
                  },
                  hotKeys: [
                      {
                          isCtrl:   true,
                          keys:     ['ArrowUp'],
                          keyCodes: [38]
                      }
                  ]
              },
              {
                  name:    "Поменять со следующим местами",
                  type:    A_SWAP_INTERNAL,
                  args:    {
                      with: 'after'
                  },
                  hotKeys: [
                      {
                          isCtrl: true,
                          keys:   ['ArrowDown']
                      }
                  ]
              }
          ];

          const moveToSuccess = [
              {
                  name:    "Выполнено",
                  type:    A_MOVE_TO_GROUP,
                  args:    {
                      id:       success,
                      position: 'first'
                  },
                  hotKeys: [
                      {
                          isCtrl: true,
                          keys:   ['ArrowRight']
                      },
                      {
                          isCtrl:       true,
                          mouseButtons: [0]
                      }
                  ]
              }
          ];

          const moveToFast = [
              {
                  name:    "Переместить в группу Быстрый план",
                  type:    A_MOVE_TO_GROUP,
                  args:    {
                      id:       fastPlan,
                      position: 'last'
                  },
                  hotKeys: [
                      {
                          isCtrl: true,
                          keys:   ['ArrowLeft']
                      }
                  ]
              }
          ];

          const moveToDisc = [
              {
                  name:    "На обсуждение",
                  type:    A_MOVE_TO_GROUP,
                  args:    {
                      id:       discussion,
                      position: 'last'
                  },
                  hotKeys: [
                      {
                          isAlt: true,
                          keys:  ['KeyD']
                      }
                  ]
              }
          ];

          const moveToMeOnReview = [
              {
                  name:    "Вернуть мне на ревью",
                  type:    A_MOVE_TO_GROUP,
                  args:    {
                      id:       meOnReview,
                      position: 'first'
                  },
                  hotKeys: [
                      {
                          isAlt: true,
                          keys:  ['KeyM']
                      }
                  ]
              }
          ];

          const moveToRelease = [
              {
                  name:    "Релиз",
                  type:    A_MOVE_TO_GROUP,
                  args:    {
                      id:       release,
                      position: 'last'
                  },
                  hotKeys: [
                      {
                          isAlt: true,
                          keys:  ['KeyR']
                      }
                  ]
              }
          ];


          const moveToLock = [
              {
                  name:    "Заблокировано",
                  type:    A_MOVE_TO_GROUP,
                  args:    {
                      id:       locked,
                      position: 'last'
                  },
                  hotKeys: [
                      {
                          isAlt: true,
                          keys:  ['KeyL']
                      }
                  ]
              }
          ];

          const moveToBacklog = [
              {
                  name:    "В бэклог",
                  type:    A_MOVE_TO_GROUP,
                  args:    {
                      id:       backLog,
                      position: 'last'
                  },
                  hotKeys: [
                      {
                          isAlt: true,
                          keys:  ['KeyB']
                      }
                  ]
              }
          ];

          const moveToOlder = [
              {
                  name:    "В долгий ящик",
                  type:    A_MOVE_TO_GROUP,
                  args:    {
                      id:       older,
                      position: 'last'
                  },
                  hotKeys: [
                      {
                          isAlt: true,
                          keys:  ['KeyO']
                      }
                  ]
              }
          ];

          const moveToCreateTask = [
              {
                  name:    "В бэклог создания задач",
                  type:    A_MOVE_TO_GROUP,
                  args:    {
                      id:       createTask,
                      position: 'last'
                  },
                  hotKeys: [
                      {
                          isAlt: true,
                          keys:  ['KeyC']
                      }
                  ]
              }
          ];

          const moveToCreatedTask = [
              {
                  name:    "Выполнено",
                  type:    A_MOVE_TO_GROUP,
                  args:    {
                      id:       createdTask,
                      position: 'last'
                  },
                  hotKeys: [
                      {
                          isCtrl: true,
                          keys:   ['ArrowRight']
                      },
                      {
                          isCtrl:       true,
                          mouseButtons: [0]
                      }
                  ]
              }
          ];


          const mMvOutbox = [
              {
                  name: "Бэкап дальних строк",
                  type: MW_OUTBOX,
                  args: {
                      peakCount:    50,
                      moveCount:    10,
                      positionFrom: 'last'
                  }
              }
          ];

          const groups = [
              {
                  id:          fastPlan,
                  name:        "Быстрый план",
                  variant:     "primary",
                  middlewares: [],
                  polities:    [P_ALL],
                  actions:     [
                      ...moveToSuccess,
                      ...moveToRelease,
                      ...moveToLock,
                      ...moveToDisc,
                      ...moveToMeOnReview,
                      ...moveToBacklog,
                      ...moveToOlder,
                      ...moveToCreateTask,
                      ...swapInternal
                  ],
                  items:       []
              },
              {
                  id:           success,
                  name:         "Выполнено",
                  variant:      "success",
                  visibleLimit: 10,
                  middlewares:  [...mMvOutbox],
                  polities:     [P_READ, P_UPDATE, P_FROM_EXTERNAL_DND, P_FROM_EXTERNAL_MOVE, P_TO_EXTERNAL_MOVE],
                  actions:      [...moveToFast, ...moveToMeOnReview],
                  items:        []
              },
              {
                  id:          meOnReview,
                  name:        "Мне на ревью",
                  variant:     "dark",
                  middlewares: [],
                  polities:    [P_ALL],
                  actions:     [...moveToSuccess, ...swapInternal],
                  items:       []
              },
              {
                  id:          backLog,
                  name:        "Бэклог",
                  variant:     "secondary",
                  middlewares: [],
                  polities:    [P_ALL],
                  actions:     [...moveToFast, ...moveToSuccess, ...moveToOlder, ...moveToCreateTask, ...swapInternal],
                  items:       []
              },
              {
                  id:          locked,
                  name:        "Заблокировано | Тестируется | На ревью",
                  variant:     "danger",
                  middlewares: [],
                  polities:    [P_READ, P_FROM_EXTERNAL_DND, P_TO_EXTERNAL_DND, P_INTERNAL_DND, P_INTERNAL_SWAP, P_FROM_EXTERNAL_MOVE, P_TO_EXTERNAL_MOVE],
                  actions:     [...moveToSuccess, ...moveToRelease, ...moveToFast, ...moveToDisc, ...swapInternal],
                  items:       []
              },
              {
                  id:          release,
                  name:        "Ждут релиз",
                  variant:     "warning",
                  middlewares: [],
                  polities:    [P_READ, P_FROM_EXTERNAL_DND, P_TO_EXTERNAL_DND, P_INTERNAL_DND, P_INTERNAL_SWAP, P_FROM_EXTERNAL_MOVE, P_TO_EXTERNAL_MOVE],
                  actions:     [...moveToSuccess, ...moveToLock, ...moveToFast, ...swapInternal],
                  items:       []
              },
              {
                  id:          older,
                  name:        "Долгий ящик",
                  variant:     "muted",
                  middlewares: [],
                  polities:    [P_ALL],
                  actions:     [...moveToBacklog, ...moveToCreateTask, ...moveToFast],
                  items:       []
              },
              {
                  id:          discussion,
                  name:        "На обсуждение",
                  variant:     "info",
                  middlewares: [],
                  polities:    [P_ALL],
                  actions:     [...moveToSuccess, ...moveToFast, ...moveToLock, ...swapInternal],
                  items:       []
              },
              {
                  id:          createTask,
                  name:        "Создать задачи",
                  variant:     "primary",
                  middlewares: [],
                  polities:    [P_ALL],
                  actions:     [...moveToCreatedTask, ...swapInternal],
                  items:       []
              }, {
                  id:           createdTask,
                  name:         "Созданные задачи",
                  variant:      "success",
                  visibleLimit: 5,
                  middlewares:  [...mMvOutbox],
                  polities:     [P_READ, P_UPDATE, P_FROM_EXTERNAL_DND, P_FROM_EXTERNAL_MOVE, P_TO_EXTERNAL_MOVE],
                  actions:      [
                      {
                          name:    "В бэклог создания задач",
                          type:    A_MOVE_TO_GROUP,
                          args:    {
                              id:       createTask,
                              position: 'last'
                          },
                          hotKeys: [
                              {
                                  isCtrl: true,
                                  keys:   ['ArrowLeft']
                              }
                          ]
                      }
                  ],
                  items:        []
              }
          ];

          const relations = [
              {
                  fromId: fastPlan,
                  toId:   success
              },
              {
                  fromId: success,
                  toId:   fastPlan
              },
              {
                  fromId: fastPlan,
                  toId:   locked
              },
              {
                  fromId: locked,
                  toId:   fastPlan
              },
              {
                  fromId: fastPlan,
                  toId:   release
              },
              {
                  fromId: release,
                  toId:   fastPlan
              },
              {
                  fromId: locked,
                  toId:   success
              }, {
                  fromId: release,
                  toId:   success
              },
              {
                  fromId: locked,
                  toId:   release
              },
              {
                  fromId: release,
                  toId:   locked
              },
              {
                  fromId: fastPlan,
                  toId:   discussion
              },
              {
                  fromId: discussion,
                  toId:   fastPlan
              },
              {
                  fromId: discussion,
                  toId:   success
              },
              {
                  fromId: discussion,
                  toId:   locked
              },
              {
                  fromId: locked,
                  toId:   discussion
              },
              {
                  fromId: meOnReview,
                  toId:   success
              },
              {
                  fromId: success,
                  toId:   meOnReview
              },
              {
                  fromId: fastPlan,
                  toId:   meOnReview
              },
              {
                  fromId: fastPlan,
                  toId:   backLog
              },
              {
                  fromId: backLog,
                  toId:   fastPlan
              },
              {
                  fromId: older,
                  toId:   fastPlan
              },
              {
                  fromId: fastPlan,
                  toId:   older
              },
              {
                  fromId: backLog,
                  toId:   older
              },
              {
                  fromId: older,
                  toId:   backLog
              },
              {
                  fromId: backLog,
                  toId:   createTask
              },
              {
                  fromId: older,
                  toId:   createTask
              },
              {
                  fromId: createTask,
                  toId:   createdTask
              },
              {
                  fromId: createdTask,
                  toId:   createTask
              },
              {
                  fromId: fastPlan,
                  toId:   createTask
              }
          ];

          const grid = {
              rows: [
                  {
                      columns: [{groupId: fastPlan, rowSpan: 2}, {groupId: release}, {groupId: success, rowSpan: 3}]
                  },
                  {
                      columns: [{groupId: locked}]
                  },
                  {
                      columns: [{groupId: meOnReview}, {groupId: discussion}]
                  },
                  {
                      columns: [{groupId: backLog, colSpan: 3}]
                  },
                  {
                      columns: [{groupId: createTask, colSpan: 2}, {groupId: createdTask, colSpan: 1}]
                  },
                  {
                      columns: [{groupId: older, colSpan: 3}]
                  }

              ]
          };

          const store = Singleton.getInstance('note-store', () => {
              const store = new NoteStore(storage, new MiddlewareManager(storage));
              store.fillGroups(groups);
              store.fillRelations(relations);
              store.fillGrid(grid);
              return store;
          });

          /**
           * @type {PreloaderStore}
           */
          const preloaderStore = Singleton.getInstance('note-preloader-store', () => {
              return new PreloaderStore();
          });

          if (preloaderStore.isShow === null) {
              preloaderStore.setIsShow(true);
              storage.getGroups({}, (payload) => {
                  store.fillRowsByGroup(payload.data);
                  setTimeout(() => {
                      preloaderStore.setIsShow(false);
                  }, 100);
              });
          }

          Singleton.getInstance('note-cron-stat-storages', () => {
              const setAllStatStorages = () => {
                  client.getStatStorages({}, (payload) => {
                      storageStatStore.setStatByStorages(payload.data);
                  });
              }
              setAllStatStorages();
              setInterval(() => {
                  setAllStatStorages();
              }, 10000);
          });

          return (<>
              <Navigation store={storageStatStore}/>
              <GroupArea store={store}/>
              <Preloader store={preloaderStore}/>
              <EventToaster
                  store={eventToasterStore}
              />
          </>)
      }
;

export default WorkSpace;