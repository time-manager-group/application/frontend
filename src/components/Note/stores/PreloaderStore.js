import {makeAutoObservable} from "mobx";


export default class PreloaderStore {
    /**
     * @private
     */
    constructor() {
        this.isShow = null;
        makeAutoObservable(this);
    }

    setIsShow(isShow = true) {
        this.isShow = isShow;
    }
}