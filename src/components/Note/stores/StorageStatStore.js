import {makeAutoObservable} from 'mobx';

export default class StorageStatStore {
    constructor() {
        /**
         * @type {number}
         */
        this.allMessageCount = 0;
        /**
         * @type {boolean}
         */
        this.isBroken = false;
        /**
         *
         * @type {*[]}
         */
        this.statByStorages = [];

        makeAutoObservable(this);
    }

    /**
     * @observable
     * @param count {number}
     */
    setAllMessageCount(count) {
        this.allMessageCount = count;
    }

    /**
     * @observable
     * @param is {boolean}
     */
    setIsBroken(is) {
        this.isBroken = is;
    }

    /**
     * @param statByStorages {*[]}
     */
    setStatByStorages(statByStorages) {
        this.statByStorages = statByStorages;
    }
}