import {makeAutoObservable} from "mobx";
import {addNewRow} from '../components/DataHelper';
import {P_CREATE, P_DELETE, P_UPDATE} from '../components/Policies';

export default class NoteStore {
    /**
     * @var {NoteStorageManager}
     * @private
     */
    __storage;
    /**
     * @var {MiddlewareManager}
     * @private
     */
    __middlewareManager;

    /**
     * @param storage {NoteStorageManager}
     * @param middlewareManager {MiddlewareManager}
     */
    constructor(storage, middlewareManager) {
        /**
         * @type {OriginGroup[]}
         */
        this.groups = [];
        this.relations = [];
        this.grid      = {};
        makeAutoObservable(this);
        this.__storage           = storage;
        this.__middlewareManager = middlewareManager;
    }

    /**
     * @observable
     * @param groups
     */
    fillGroups(groups) {
        this.groups = groups;
    }

    /**
     * @observable
     * @param groups
     */
    fillRowsByGroup(groups) {
        for (const group of groups) {
            const foundedGroup = this.getGroup(group.id);
            if (!foundedGroup) {
                continue;
            }
            this.groups[foundedGroup.index].items = group.items;
        }
    }

    /**
     * @observable
     * @param relations
     */
    fillRelations(relations) {
        this.relations = relations;
    }

    /**
     * @observable
     * @param grid
     */
    fillGrid(grid) {
        this.grid = grid;
    }

    /**
     * @returns {OriginGroup[]}
     */
    getGroups() {
        return this.groups;
    }

    /**
     * @param id {string}
     * @returns {FoundedRow|null}
     */
    getRow(id) {
        for (const group of this.groups) {
            for (const [index, row] of group.items.entries()) {
                if (row.id === id) {
                    // noinspection JSValidateTypes
                    return {...row, groupId: group.id, index};
                }
            }
        }
        return null;
    }


    /**
     * @param groupId {string}
     * @param rowIndex {number}
     * @returns {FoundedRow|null}
     */
    getRowByIndex(groupId, rowIndex) {
        for (const group of this.groups) {
            for (const [index, row] of group.items.entries()) {
                if (index === rowIndex) {
                    // noinspection JSValidateTypes
                    return {...row, groupId: group.id, index};
                }
            }
        }
        return null;
    }

    /**
     * @param id {string}
     * @returns {FoundedGroup|null}
     */
    getGroup(id) {
        for (const [index, group] of this.groups.entries()) {
            if (group.id === id) {
                // noinspection JSValidateTypes
                return {...group, index};
            }
        }
        return null;
    }

    /**
     * @param id {string}
     * @returns {FoundedGroup|null}
     */
    getGroupByRow(id) {
        for (const [index, group] of this.groups.entries()) {
            for (const row of group.items) {
                if (row.id === id) {
                    // noinspection JSValidateTypes
                    return {...group, index};
                }
            }
        }
        return null;
    }

    /**
     * @param fromGroupId {string}
     * @param toGroupId {string}
     * @returns {boolean}
     */
    checkRelations(fromGroupId, toGroupId) {
        for (const relation of this.relations) {
            if ((relation.fromId === '*' || relation.fromId === fromGroupId) && (relation.toId === '*' || relation.toId === toGroupId)) {
                return true;
            }
        }
        return false;
    }

    /**
     * @param id {string}
     * @param policy {string}
     * @returns {boolean}
     */
    checkPolicyByGroup(id, policy) {
        for (const group of this.groups) {
            if (group.id === id) {
                return group.polities.includes(policy) || group.polities.includes('*');
            }
        }
        return false;
    }

    /**
     * @observable
     * @param source {{groupId:number|string,index:number|null}}
     * @param destination {{groupId:number|string,index:number|null}}
     */
    moveRow(source, destination) {
        if (!this.__middlewareManager.executeBefore(this, source.groupId, this.getRowByIndex(source.groupId, source.index)?.id, 'moveRow', {source, destination})) {
            return;
        }
        if (!this.__middlewareManager.executeBefore(this, destination.groupId, this.getRowByIndex(destination.groupId, source.index)?.id, 'moveRow', {source, destination})) {
            return;
        }

        if (source.index === null || destination.index === null) {
            return
        }

        const sourceGroupIndex = this.groups.findIndex(group => group.id === source.groupId);
        const destGroupIndex   = this.groups.findIndex(group => group.id === destination.groupId);

        const sourceRow = this.groups[sourceGroupIndex]?.items[source.index];

        const newGroups = [...this.groups];
        // Перемещение внутри одной группы
        if (sourceGroupIndex === destGroupIndex) {
            const group     = newGroups[sourceGroupIndex];
            const newItems  = Array.from(group.items);
            const [removed] = newItems.splice(source.index, 1);
            newItems.splice(destination.index, 0, removed);

            newGroups[sourceGroupIndex] = {
                ...group,
                items: newItems
            };

            this.groups = newGroups;
        } else {
            const sourceGroup = newGroups[sourceGroupIndex];
            const destGroup   = newGroups[destGroupIndex];
            let sourceItems   = Array.from(sourceGroup.items);
            let destItems     = Array.from(destGroup.items);
            const [removed]   = sourceItems.splice(source.index, 1);

            destItems.splice(destination.index, 0, removed);

            const allowSkipRemoveRow = (item, allowPolicy) => {
                if (allowPolicy && !item.name) {
                    return true;
                }
                return !allowPolicy && item?.isNew && !item.name;
            }

            sourceItems = sourceItems.filter((item) => !allowSkipRemoveRow(item, this.checkPolicyByGroup(source.groupId, P_DELETE)));
            if (sourceItems.length === 0) {
                if (this.checkPolicyByGroup(source.groupId, P_CREATE)) {
                    addNewRow(sourceItems);
                }
            }

            destItems = destItems.filter((item) => !allowSkipRemoveRow(item, this.checkPolicyByGroup(destination.groupId, P_DELETE)));
            if (destItems.length === 0) {
                if (this.checkPolicyByGroup(destination.groupId, P_CREATE)) {
                    addNewRow(destItems);
                }
            }

            newGroups[sourceGroupIndex] = {
                ...sourceGroup,
                items: sourceItems
            };

            newGroups[destGroupIndex] = {
                ...destGroup,
                items: destItems
            };

            this.groups = newGroups;
        }

        const movedRow = this.getRow(sourceRow.id);
        if (movedRow?.name && movedRow?.isNew) {
            this.unsetIsNew(sourceRow.id);
            this.__storage.addRow({
                groupId:    movedRow.groupId,
                afterRowId: movedRow.index ? this.groups[destGroupIndex].items[movedRow.index - 1].id : null
            }, this.groups[destGroupIndex].items[movedRow.index]);
        } else {
            this.__storage.moveRow({
                groupId:  this.groups[sourceGroupIndex].id,
                rowId:      movedRow.id,
                toGroupId:  this.groups[destGroupIndex].id,
                afterRowId: movedRow.index ? this.groups[destGroupIndex].items[movedRow.index - 1].id : null
            });
        }

        this.__middlewareManager.executeAfter(this, source.groupId, this.getRowByIndex(source.groupId, source.index)?.id, 'moveRow', {source, destination});
        this.__middlewareManager.executeAfter(this, destination.groupId, this.getRowByIndex(destination.groupId, source.index)?.id, 'moveRow', {source, destination});
    }

    /**
     * @observable
     * @param id {string}
     */
    clearRowsByRow(id) {
        const row = this.getRow(id);

        if (!this.__middlewareManager.executeBefore(this, row.groupId, id, 'clearRowsByRow', {id})) {
            return;
        }

        const {id: groupId, index: groupIndex, items: itemRows} = this.getGroupByRow(id);
        const oldRowIds                                         = this.groups[groupIndex].items.filter(row => !row?.isNew).map(row => row.id);

        const newRows = [];
        for (const [key, itemRow] of itemRows.entries()) {
            if (!itemRow.name && !itemRow?.isNew && !this.checkPolicyByGroup(groupId, P_DELETE)) {
                newRows.push(itemRow);
                continue;
            }
            if (itemRow.name || (key - 1) === row.index || (key + 1) === row.index) {
                newRows.push(itemRow);
            }
        }

        if (newRows.length === 0 && this.checkPolicyByGroup(row.groupId, P_CREATE)) {
            delete row.groupId;
            delete row.index;
            // noinspection JSValidateTypes
            this.groups[groupIndex].items = [{...row, name: '', isNew: true}];
        } else {
            this.groups[groupIndex].items = newRows;
        }


        if (row?.name && row?.isNew) {
            this.unsetIsNew(id);
            this.__storage.addRow({
                groupId:    row.groupId,
                afterRowId: row.index ? this.groups[groupIndex].items[row.index - 1].id : null
            }, this.groups[groupIndex].items[row.index]);
        }

        const newRowIds     = newRows.filter(row => !row?.isNew).map((row) => row.id);
        const deletedRowIds = oldRowIds.filter(x => !newRowIds.includes(x));
        for (const deletedRowId of deletedRowIds) {
            this.__storage.deleteRow({
                groupId: groupId,
                rowId:   deletedRowId
            });
        }

        this.__middlewareManager.executeAfter(this, groupId, id, 'clearRowsByRow', {id});
    }

    /**
     * @observable
     * @param id {string}
     * @returns {OriginRow|null}
     */
    deleteRow(id) {
        const row = this.getRow(id);

        if (!this.__middlewareManager.executeBefore(this, row.groupId, id, 'deleteRow', {id})) {
            return row;
        }

        if (!row?.isNew && !this.checkPolicyByGroup(row.groupId, P_DELETE)) {
            return row;
        }

        const {id: groupId, index: groupIndex, items: itemRows} = this.getGroupByRow(id);

        const newRows     = [];
        let newCurrentRow = null;
        for (const [key, itemRow] of itemRows.entries()) {
            if (key !== row.index) {
                newRows.push(itemRow);
            }
            if ((row.index === 0 && key === 1) || key === row.index - 1) {
                newCurrentRow = itemRow;
            }
        }
        if (newRows.length === 0 && this.checkPolicyByGroup(row.groupId, P_CREATE)) {
            delete row.groupId;
            delete row.index;
            // noinspection JSValidateTypes
            this.groups[groupIndex].items = [{...row, name: '', isNew: true}];
        } else {
            this.groups[groupIndex].items = newRows;
        }

        if (newCurrentRow?.id !== id && !row?.isNew) {
            this.__storage.deleteRow({
                groupId: groupId,
                rowId:   id
            });
        }

        this.__middlewareManager.executeAfter(this, groupId, id, 'deleteRow', {id});

        return newCurrentRow;
    }

    /**
     * @observable
     * @param groupId {string}
     * @returns {OriginRow|null}
     */
    addNewRowToFirst(groupId) {
        const {index: groupIndex, items: itemRows} = this.getGroup(groupId);

        if (!this.__middlewareManager.executeBefore(this, groupId, null, 'addNewRowToFirst', {groupId})) {
            return null;
        }

        const newRows = [];
        const newRow  = addNewRow(newRows);
        for (const itemRow of itemRows) {
            newRows.push(itemRow);
        }

        this.groups[groupIndex].items = newRows;

        this.__middlewareManager.executeAfter(this, groupId, newRow.id, 'addNewRowToFirst', {groupId});

        return newRow;
    }

    /**
     * @observable
     * @param groupId {string}
     * @returns {OriginRow|null}
     */
    addNewRowToLast(groupId) {
        const {index: groupIndex, items: itemRows} = this.getGroup(groupId);

        if (!this.__middlewareManager.executeBefore(this, groupId, null, 'addNewRowToLast', {groupId})) {
            return null;
        }

        const newRows = [];
        for (const itemRow of itemRows) {
            newRows.push(itemRow);
        }
        const newRow = addNewRow(newRows);

        this.groups[groupIndex].items = newRows;

        this.__middlewareManager.executeAfter(this, groupId, newRow.id, 'addNewRowToLast', {groupId});

        return newRow;
    }

    /**
     * @observable
     * @param id {string}
     * @returns {OriginRow|null}
     */
    addNewRowToAfter(id) {
        const row = this.getRow(id);

        if (!this.__middlewareManager.executeBefore(this, row.groupId, id, 'addNewRowToAfter', {id})) {
            return null;
        }

        if (row.name === '') {
            return row;
        }
        if (!this.checkPolicyByGroup(row.groupId, P_CREATE)) {
            return row;
        }

        const {index: groupIndex, items: itemRows} = this.getGroupByRow(id);

        const newRows     = [];
        let newCurrentRow = null;
        for (const [key, itemRow] of itemRows.entries()) {
            newRows.push(itemRow);
            if (key === row.index) {
                newCurrentRow = addNewRow(newRows);
            }
        }

        this.groups[groupIndex].items = newRows;

        this.__middlewareManager.executeBefore(this, row.groupId, newCurrentRow.id, 'addNewRowToAfter', {id});

        return newCurrentRow ? newCurrentRow : row;
    }

    /**
     * @observable
     * @param id {string}
     * @param name {string}
     * @param fixed {boolean}
     */
    changeRowName(id, name, fixed = false) {
        const row = this.getRow(id);

        if (!this.__middlewareManager.executeBefore(this, row.groupId, id, 'changeRowName', {id, name, fixed})) {
            return;
        }

        if (!(this.checkPolicyByGroup(row.groupId, P_UPDATE) || row.isNew)) {
            return;
        }
        for (const [groupIndex, group] of this.groups.entries()) {
            for (const [rowIndex, row] of group.items.entries()) {
                if (row.id === id) {
                    this.groups[groupIndex].items[rowIndex].name = name;
                    if (fixed && name && row?.isNew) {
                        this.unsetIsNew(id);
                        this.__storage.addRow({
                            groupId:    group.id,
                            afterRowId: rowIndex ? group.items[rowIndex - 1].id : null
                        }, row);
                    } else if (fixed && !row?.isNew) {
                        this.__storage.changeRow({
                            groupId: group.id,
                            rowId:   row.id
                        }, row);
                    }
                    return;
                }
            }
        }

        this.__middlewareManager.executeAfter(this, row.groupId, id, 'changeRowName', {id, name, fixed});
    }

    /**
     * @param id {string}
     */
    unsetIsNew(id) {
        for (const [groupIndex, group] of this.groups.entries()) {
            for (const [rowIndex, row] of group.items.entries()) {
                if (row.id === id) {
                    delete row.isNew;
                    this.groups[groupIndex].items[rowIndex] = row;
                    return;
                }
            }
        }
    }
}