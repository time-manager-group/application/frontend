import {BackendApi} from '../../../services/BackendApi';

export default class NoteApi extends BackendApi {
    /**
     * @param params {{}|null}
     * @param handleSuccess {$Call}
     * @param handleFail {$Call}
     */
    getGroups(params = null, handleSuccess = null, handleFail = null) {
        this.axios.get('note/list-groups', {params: params ? {noteId: params.noteId} : null})
            .then(function(response) {
                handleSuccess(response.data);
            })
            .catch(function(error) {
                if (handleFail) {
                    handleFail(error);
                }
            });
    }

    /**
     * @param handleSuccess {$Call}
     * @param handleFail {$Call}
     * @param params {{
     *     groupId: string,
     *     afterRowId: string|null
     * }}
     * @param payload {OriginRow}
     */
    addRow(params, payload, handleSuccess = null, handleFail = null) {
        this.axios.post('note/add-row-to-after', {
                id:   payload.id,
                name: payload.name
            }, {params: {groupId: params.groupId, afterRowId: params.afterRowId}})
            .then(function(response) {
                handleSuccess(response.data);
            })
            .catch(function(error) {
                if (handleFail) {
                    handleFail(error);
                }
            });
    }

    /**
     * @param params {{
     *     groupId: string,
     *     rowId: string
     * }}
     * @param payload {OriginRow}
     * @param handleSuccess {$Call}
     * @param handleFail {$Call}
     */
    changeRow(params, payload, handleSuccess = null, handleFail = null) {
        this.axios.post('note/change-row', {
                name: payload.name
            }, {params: {groupId: params.groupId, rowId: params.rowId}})
            .then(function(response) {
                handleSuccess(response.data);
            })
            .catch(function(error) {
                if (handleFail) {
                    handleFail(error);
                }
            });
    }

    /**
     * @param params {{
     *     groupId: string,
     *     rowId: string
     * }}
     * @param handleSuccess {$Call}
     * @param handleFail {$Call}
     */
    deleteRow(params, handleSuccess = null, handleFail = null) {
        this.axios.post('note/delete-row', null, {params: {groupId: params.groupId, rowId: params.rowId}})
            .then(function(response) {
                handleSuccess(response.data);
            })
            .catch(function(error) {
                if (handleFail) {
                    handleFail(error);
                }
            });
    }

    /**
     * @param params {{
     *     groupId: string,
     *     rowId: string,
     *     toGroupId: string,
     *     afterRowId: string|null
     * }}
     * @param handleSuccess {$Call}
     * @param handleFail {$Call}
     */
    moveRow(params, handleSuccess = null, handleFail = null) {
        this.axios.post('note/move-row', null, {
                params: {
                    groupId:    params.groupId,
                    rowId:      params.rowId,
                    toGroupId:  params.toGroupId,
                    afterRowId: params.afterRowId
                }
            })
            .then(function(response) {
                handleSuccess(response.data);
            })
            .catch(function(error) {
                if (handleFail) {
                    handleFail(error);
                }
            });
    }

    /**
     * @param params {{
     *     groupId: string,
     *     rowId: string,
     * }}
     * @param handleSuccess {$Call}
     * @param handleFail {$Call}
     */
    moveRowToBackup(params, handleSuccess = null, handleFail = null) {
        this.axios.post('note/move-row-to-backup', null, {
                params: {
                    groupId: params.groupId,
                    rowId:   params.rowId
                }
            })
            .then(function(response) {
                handleSuccess(response.data);
            })
            .catch(function(error) {
                if (handleFail) {
                    handleFail(error);
                }
            });
    }

    /**
     * @param params {{}|null}
     * @param handleSuccess {$Call}
     * @param handleFail {$Call}
     */
    getStatStorages(params, handleSuccess = null, handleFail = null) {
        this.axios.get('note/stat-storages')
            .then(function(response) {
                handleSuccess(response.data);
            })
            .catch(function(error) {
                if (handleFail) {
                    handleFail(error);
                }
            });
    }
}