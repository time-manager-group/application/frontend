import {ADD_ROW, CHANGE_ROW, DELETE_ROW, MOVE_ROW, MOVE_ROW_TO_BACKUP} from './NoteApiController';

export default class NoteStorageManager {
    /**
     * @var {NoteApi}
     * @private
     */
    __api
    /**
     * @var {Queue}
     * @private
     */
    __queue

    /**
     * @param api {NoteApi}
     * @param queue {Queue}
     * @param worker {QueueSyncWorker}
     * @param controller {NoteApiController}
     * @param statStore {StorageStatStore}
     */
    constructor(api, queue, worker, controller, statStore) {
        this.__api   = api;
        this.__queue = queue;
        this.__queue.addHandlePush(async() => await worker.process());

        worker.addHandleFetch(async(message, queue) => {
            try {
                statStore.setAllMessageCount(queue.getAllMessageCount());
                const data = await new Promise((resolve, reject) => controller.execute(resolve, reject, message.command, message.args, message.payload));
                queue.ack(message.id, data);
                statStore.setAllMessageCount(queue.getAllMessageCount());
            } catch (e) {
                queue.nack(message.id, e);
                if (message.returnedCount > 2) {
                    queue.lock();
                    statStore.setIsBroken(true);
                }
            }
        });
    }

    /**
     * @param params {{}|null}
     * @param handleSuccess {$Call}
     * @param handleFail {$Call}
     */
    getGroups(params = null, handleSuccess = null, handleFail = null) {
        this.__api.getGroups(params, handleSuccess, handleFail)
    }

    /**
     * @param handleSuccess {$Call}
     * @param handleFail {$Call}
     * @param params {{
     *     groupId: string,
     *     afterRowId: string|null
     * }}
     * @param payload {OriginRow}
     */
    addRow(params, payload, handleSuccess = null, handleFail = null) {
        const id = this.__queue.push(ADD_ROW, params, payload);
    }

    /**
     * @param params {{
     *     groupId: string,
     *     rowId: string
     * }}
     * @param payload {OriginRow}
     * @param handleSuccess {$Call}
     * @param handleFail {$Call}
     */
    changeRow(params, payload, handleSuccess = null, handleFail = null) {
        const id = this.__queue.push(CHANGE_ROW, params, payload);
    }

    /**
     * @param params {{
     *     groupId: string,
     *     rowId: string
     * }}
     * @param handleSuccess {$Call}
     * @param handleFail {$Call}
     */
    deleteRow(params, handleSuccess = null, handleFail = null) {
        const id = this.__queue.push(DELETE_ROW, params, null);
    }

    /**
     * @param params {{
     *     groupId: string,
     *     rowId: string,
     *     toGroupId: string,
     *     afterRowId: string|null
     * }}
     * @param handleSuccess {$Call}
     * @param handleFail {$Call}
     */
    moveRow(params, handleSuccess = null, handleFail = null) {
        const id = this.__queue.push(MOVE_ROW, params, null);
    }

    /**
     * @param params {{
     *     groupId: string,
     *     rowId: string,
     * }}
     * @param handleSuccess {$Call}
     * @param handleFail {$Call}
     */
    moveRowToBackup(params, handleSuccess = null, handleFail = null) {
        const id = this.__queue.push(MOVE_ROW_TO_BACKUP, params, null);
    }
}