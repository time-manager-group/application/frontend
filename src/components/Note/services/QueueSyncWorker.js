/** @typedef {function(QueueMessage, Queue): void} FetchCallable */
import {Mutex} from '../components/Mutex';
import {sleep} from '../../../services/TimeHelper';

/** @typedef {{
 * minDelay?: number,
 * delayCoefficient?: number|float,
 * delayCoefficientMultiple?: number|float,
 * usePeakDelayOnProcess?: boolean,
 * maxCalcDelay?: number,
 * }} QueueSyncWorkerParams */

export default class QueueSyncWorker {
    /**
     * @var {FetchCallable[]}
     * @private
     */
    __fetchCallables;

    /**
     * @var {Mutex}
     * @private
     */
    __mutex
    /**
     * @var {Queue}
     * @private
     */
    __queue

    /**
     * @var {QueueSyncWorkerParams}
     * @private
     */
    __params

    /**
     * @var number|null
     * @private
     */
    __calculatedDelay

    /**
     *
     * @param queue {Queue}
     * @param params {QueueSyncWorkerParams}
     */
    constructor(queue, params = {}) {
        this.__mutex           = new Mutex();
        this.__queue           = queue;
        this.__params          = params;
        this.__fetchCallables  = [];
        this.__calculatedDelay = 0;
    }

    /**
     * @param callback {FetchCallable}
     * @return void
     */
    addHandleFetch(callback) {
        this.__fetchCallables.push(callback);
    }

    /**
     * @return integer|null
     */
    async process() {
        const unlock      = await this.__mutex.lock();
        let countMessages = 0;
        while (true) {
            const message = this.__queue.getLast();
            if (!message) {
                unlock();
                if (this.__params?.usePeakDelayOnProcess) {
                    this.__calculatedDelay = 0;
                }
                return countMessages;
            }
            // noinspection JSCheckFunctionSignatures
            await this.__handleFetch(message);
            countMessages++;
        }
    }

    /**
     * @param message {QueueMessage}
     * @private
     */
    async __handleFetch(message) {
        for (const callback of this.__fetchCallables) {

            await callback(message, this.__queue);

            let delay              = Number(this.__params?.minDelay);
            let multiple           = Math.min(Number(this.__params?.delayCoefficientMultiple), 1);
            delay                  = this.__params?.delayCoefficient ? (delay + (this.__queue.getAllMessageCount() * this.__params?.delayCoefficient) * multiple) : delay;
            delay                  = Math.max(Number(this.__params?.maxCalcDelay), delay);
            this.__calculatedDelay = this.__params?.usePeakDelayOnProcess ? Math.max(delay, this.__calculatedDelay) : delay;

            if (this.__calculatedDelay > 0) {
                await sleep(this.__calculatedDelay);
            }
        }
    }
}