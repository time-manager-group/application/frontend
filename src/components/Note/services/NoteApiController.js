export const ADD_ROW            = 'add-row';
export const CHANGE_ROW         = 'change-row';
export const DELETE_ROW         = 'delete-row';
export const MOVE_ROW           = 'move-row';
export const MOVE_ROW_TO_BACKUP = 'move-row-to-backup';

export default class NoteApiController {
    /**
     * @var {NoteApi}
     * @private
     */
    __api

    /**
     * @param api {NoteApi}
     */
    constructor(api) {
        this.__api = api;
    }

    /**
     * @param handleSuccess {$Call}
     * @param handleFail {$Call}
     * @param action {ADD_ROW|CHANGE_ROW|DELETE_ROW|MOVE_ROW|MOVE_ROW_TO_BACKUP|*}
     * @param params {*}
     * @param payload {*}
     * @return void
     */
    execute(handleSuccess, handleFail, action, params, payload) {
        switch (action) {
            case ADD_ROW:
                this.__api.addRow(params, payload, handleSuccess, handleFail);
                return;
            case CHANGE_ROW:
                this.__api.changeRow(params, payload, handleSuccess, handleFail);
                return;
            case DELETE_ROW:
                this.__api.deleteRow(params, handleSuccess, handleFail);
                return;
            case MOVE_ROW:
                this.__api.moveRow(params, handleSuccess, handleFail);
                return;
            case MOVE_ROW_TO_BACKUP:
                this.__api.moveRowToBackup(params, handleSuccess, handleFail);
                return;
            default:
                throw new Error(`Undefined action: ${action}`);
        }
    }
}