import {uuidv4} from '../components/DataHelper';

/** @typedef {{
 returnedCount: number,
 id: string,
 command: string,
 args: {},
 payload: {},
 }} QueueMessage */

/** @typedef {function(QueueMessage, Queue): void} PushCallable */


export default class Queue {
    /**
     * @var {QueueMessage[]}
     * @private
     */
    __queue

    /**
     * @var {PushCallable[]}
     * @private
     */
    __pushCallables;

    /**
     * @var {(function(QueueMessage, {}): void) []}
     * @private
     */
    __ackCallables;

    /**
     * @var {(function(QueueMessage, {}): void) []}
     * @private
     */
    __nackCallables;

    /**
     * @var {boolean}
     * @private
     */
    __isLock;

    constructor() {
        this.__queue         = [];
        this.__ackCallables  = [];
        this.__nackCallables = [];
        this.__pushCallables = [];
        this.__isLock        = false;
    }

    /**
     * @param callback {PushCallable}
     * @return void
     */
    addHandlePush(callback) {
        this.__pushCallables.push(callback);
    }

    /**
     * @param callback {function(QueueMessage, {}): void}
     * @return void
     */
    addHandleAck(callback) {
        this.__ackCallables.push(callback);
    }

    /**
     * @param callback {function(QueueMessage, {}): void}
     * @return void
     */
    addHandleNack(callback) {
        this.__nackCallables.push(callback);
    }

    /**
     * @param command {string}
     * @param args {*}
     * @param payload {*}
     * @return string
     */
    push(command, args, payload) {
        const id  = uuidv4();
        const msg = {
            returnedCount: 0,
            id,
            command:       command,
            args:          args,
            payload:       payload
        };
        this.__queue.push(msg);
        this.__handlePush(msg);
        return id;
    }

    /**
     * @return {QueueMessage|null}
     */
    getLast() {
        if (this.__isLock) {
            return null;
        }
        return this.__queue.length > 0 ? this.__queue[0] : null;
    }

    /**
     * @return {number}
     */
    getAllMessageCount() {
        return this.__queue.length;
    }

    /**
     * @param id string
     * @param data {*}
     * @return boolean
     */
    ack(id, data) {
        for (const [key, item] of this.__queue.entries()) {
            if (item.id === id) {
                this.__handleAck(item, data);
                this.__queue.splice(key, 1);
                return true;
            }
        }
        return false;
    }

    /**
     * @param id string
     * @param data {*}
     * @return boolean
     */
    nack(id, data) {
        for (const [key, item] of this.__queue.entries()) {
            if (item.id === id) {
                this.__handleNack(item, data);
                this.__queue[key].returnedCount++;
                return true;
            }
        }
        return false;
    }

    lock() {
        this.__isLock = true;
    }

    unlock() {
        this.__isLock = false;
    }

    /**
     * @param item {QueueMessage}
     * @param data {*}
     * @private
     */
    __handleAck(item, data) {
        for (const callback of this.__ackCallables) {
            callback(item, data);
        }
    }

    /**
     * @param item {QueueMessage}
     * @param data {*}
     * @private
     */
    __handleNack(item, data) {
        for (const callback of this.__nackCallables) {
            callback(item, data);
        }
    }


    /**
     * @param item {QueueMessage}
     * @param data {*}
     * @private
     */
    __handlePush(item) {
        for (const callback of this.__pushCallables) {
            callback(item, this);
        }
    }
}