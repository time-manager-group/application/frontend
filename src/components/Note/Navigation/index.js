import React from 'react';

import '../../../styles/TableItem.css';
import {Badge, Modal, Nav, Navbar, Spinner} from "react-bootstrap";
import {Check, X, Envelope} from "react-bootstrap-icons";
import classes from './style.module.css';
import {observer} from 'mobx-react';
import {toJS} from 'mobx';
import Button from 'react-bootstrap/Button';
import Singleton from '../components/Singleton';
import {getFormattedDate} from '../../../services/TimeHelper';
import MetricWidget from '../../MetricWidget';


/**
 * Навигация.
 */
const Navigation = observer(({store}) => {
    const allMessageCount = toJS(store.allMessageCount);
    const isBroken        = toJS(store.isBroken);
    const statByStorages  = toJS(store.statByStorages);

    Singleton.getInstance('note-register-beforeunload-listener', () => {
        window.addEventListener('beforeunload', (event) => {
            if (store.allMessageCount === 0 || store.isBroken) {
                return;
            }
            const confirmationMessage = 'Warning: Wait for synchronization to close the window..';
            // noinspection JSDeprecatedSymbols
            event.returnValue         = confirmationMessage; // Для старых браузеров
            return confirmationMessage; // Для современных браузеров
        });
    });

    /**
     * @type ReportApi
     */
    const reportApi = Singleton.getInstance('common-report-api');

    const statStorageBadge = (title, statByStorage, variant) => {
        // noinspection JSUnresolvedReference
        const sizeMb = (statByStorage.sizeByte / 8 / 1024 / 1024).toFixed(3);
        // noinspection JSUnresolvedReference
        const ft     = statByStorage.rowOdlTime ? getFormattedDate(Number(statByStorage.rowOdlTime) * 1000, true) : 'none';
        // noinspection JSUnresolvedReference
        const lt     = statByStorage.rowLastTime ? getFormattedDate(Number(statByStorage.rowLastTime) * 1000, true) : 'none';

        // noinspection JSUnresolvedReference
        return (<Badge variant={variant} title={'ft: ' + ft + ', lt: ' + lt}>
            {title}: s: {sizeMb} Mb, c: {statByStorage.rowsCount}
        </Badge>);
    };

    return (
        <>
            <Navbar bg="light" variant="light" sticky="top">
                <Nav className="mr-auto">
                    <Nav.Link
                        disabled={true}
                        title={''}
                        onClick={() => {
                        }}
                    ></Nav.Link>
                    <Nav.Link
                        title={'Send report'}
                        onClick={() => reportApi.sendDailyReport()}
                    >Send report <Envelope/></Nav.Link>
                    <Navbar.Text style={{display: 'flex', gap: 3}}>
                        {statByStorages['group_action_log_storage'] ? statStorageBadge('Action log', statByStorages['group_action_log_storage'], 'success') : ''}
                        {statByStorages['change_log_storage'] ? statStorageBadge('Change log', statByStorages['change_log_storage'], 'info') : ''}
                        {statByStorages['group_backup_row_storage'] ? statStorageBadge('Row backup', statByStorages['group_backup_row_storage'], 'secondary') : ''}
                    </Navbar.Text>
                    <MetricWidget/>
                </Nav>
                <Nav className={classes['nav-right']}>
                    {isBroken ? <Button variant="danger" disabled>
                        <X
                            size="20"
                        />
                        <span className="visually-hidden"> Sync failed</span>
                    </Button> : (allMessageCount > 0 ? <>
                        <Button variant="warning" disabled>
                            <Spinner
                                as="span"
                                animation="border"
                                size="sm"
                                role="status"
                                aria-hidden="true"
                            />
                            <span className="visually-hidden"> Sync {allMessageCount}...</span>
                        </Button>
                    </> : <Button variant="success" disabled>
                        <Check
                            size="20"
                        />
                        <span className="visually-hidden"> Synced</span>
                    </Button>)}
                </Nav>
            </Navbar>
            <Modal show={isBroken} onHide={() => {
            }}>
                <Modal.Header>
                    <Modal.Title>Synchronization failed</Modal.Title>
                </Modal.Header>
                <Modal.Body>Synchronization failed with server. Failed to sync last <b>{allMessageCount}</b> events. Please, reload this page</Modal.Body>
                <Modal.Footer>
                    <Button variant="primary" onClick={() => window.location.reload()}>
                        Reload page
                    </Button>
                </Modal.Footer>
            </Modal>
        </>
    )
});

export default Navigation;