import React from 'react';

import '../../styles/TableItem.css';
import Button from 'react-bootstrap/Button';
import {Reception0, Reception2, Reception4} from 'react-bootstrap-icons';
import {ButtonGroup} from 'react-bootstrap';

// noinspection JSUnusedLocalSymbols
/**
 *
 */
const TrackTypes = (props) => {
    const state = {
        useless:   {
            variant:  'outline-info',
            disabled: false
        },
        undefined: {
            variant:  'outline-secondary',
            disabled: false
        },
        useful:    {
            variant:  'outline-success',
            disabled: false
        }
    };

    const changeTypes = (event, typeList) => {
        for (const typeName in typeList) {
            // noinspection JSUnfilteredForInLoop
            let type    = typeList[typeName];
            let variant = type.variant.replace('outline-', '');

            if (type === event) {
                type.disabled = true;
                type.variant  = variant;
            } else {
                type.disabled = false;
                type.variant  = 'outline-' + variant;
            }
        }
    };

    if (props.hasOwnProperty('focus') && state.hasOwnProperty(props.focus)) {
        changeTypes(state[props.focus], state);
    } else {
        changeTypes(state.undefined, state);
    }

    const handleChangeTypes = (event) => {
        if (props.hasOwnProperty('onChange')) {
            let currentTypeName = '';

            for (const typeName in state) {
                if (state[typeName] === event) {
                    currentTypeName = typeName;

                    break;
                }
            }

            props.onChange(currentTypeName);
        }
    };

    return (
        <ButtonGroup>
            <Button size="sm"
                    title={'Бесполезный трек'}
                    variant={state.useless.variant}
                    onClick={() => handleChangeTypes(state.useless)}
                    disabled={state.useless.disabled}
            >
                <Reception0/>
            </Button>
            <Button size="sm"
                    title={'Неопределенный трек'}
                    variant={state.undefined.variant}
                    onClick={() => handleChangeTypes(state.undefined)}
                    disabled={state.undefined.disabled}
            >
                <Reception2/>
            </Button>
            <Button size="sm"
                    title={'Полезный трек'}
                    variant={state.useful.variant}
                    onClick={() => handleChangeTypes(state.useful)}
                    disabled={state.useful.disabled}
            >
                <Reception4/>
            </Button>
            {props.afterGroup}
        </ButtonGroup>
    )

}

export default TrackTypes;
