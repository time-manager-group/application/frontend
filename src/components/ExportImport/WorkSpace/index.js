import React from 'react';
import {Col, Container, Row} from 'react-bootstrap';
import Button from 'react-bootstrap/Button';
import ReportList from '../ReportList';
import ReportImportExportStore from '../../../stores/ReportImportExportStore';
import useQuery from '../../../services/UseQuery';


const WorkSpace = () => {
    let query = useQuery();

    ReportImportExportStore.getInstance().loadPage(parseInt(query.get('page')));

    return (
        <div style={{'padding-top': '15px'}}>
            <Container>
                <Row style={{'padding-bottom': '1em'}}>
                    <Col md={{span: 6, offset: 3}}>
                        <Button variant="primary">Загрузить</Button>
                    </Col>
                </Row>
                <ReportList store={ReportImportExportStore.getInstance()}/>
            </Container>
        </div>
    )
};

export default WorkSpace;

