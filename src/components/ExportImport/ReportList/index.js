import React from 'react';
import {Badge, ButtonGroup, Col, Container, ListGroup, Row} from 'react-bootstrap';
import Button from 'react-bootstrap/Button';
import Pagination from "react-js-pagination";
import {observer} from 'mobx-react';

const ReportList = observer(({store}) => {
    return (
        <>
            <Row style={{'padding-bottom': '1em'}}>
                <Col md={{span: 6, offset: 3}}>
                    <ListGroup>
                        {store.items.map((item) =>
                            <ListGroup.Item action>
                                <Row>
                                    <Col md={3
                                    }>
                                        <h3>
                                            <Badge bg="secondary">{item.name}</Badge>
                                        </h3>
                                    </Col>
                                    <Col md={{span: 4, offset: 4}} style={{'text-align': 'right'}}>
                                        <ButtonGroup>
                                            <Button variant="primary" data-id={item.id}>Загрузить</Button>
                                            <Button variant="success" data-id={item.id} onClick={(event) => {
                                                store.export(item.id);
                                            }}>Выгрузить</Button>
                                        </ButtonGroup></Col>
                                </Row>
                            </ListGroup.Item>
                        )}
                    </ListGroup>

                </Col>
            </Row>
            <Row style={{'padding-bottom': '1em'}}>
                <Col md={{span: 6, offset: 3}}>
                    <div style={{'padding-left': '18px'}}>
                        <Pagination
                            itemClass="page-item"
                            linkClass="page-link"
                            activePage={store.pagination.page}
                            itemsCountPerPage={store.pagination.per_page}
                            totalItemsCount={store.pagination.count}
                            pageRangeDisplayed={10}
                            onChange={(page) => {
                                window.history.pushState('page' + page, 'page ' + page, document.location.pathname + '?page=' + page);
                                store.loadPage(page);
                            }}
                        />
                    </div>
                </Col>
            </Row>
        </>
    )
});

export default ReportList;

