import React, {memo} from 'react';
import {Accordion, Button, Card, Collapse, ListGroup} from 'react-bootstrap';
import "react-datetime/css/react-datetime.css";
import TimeFormat from '../../services/TimeFormat';
import '../../styles/TableItem.css';

import '../../styles/Animation.css';
import "react-datetime/css/react-datetime.css";
import ReportTaskStore from "../../stores/ReportTaskStore";
import TrackReportListItemLight from "../TrackReportListItemLight";


/**
 *
 */
const TrackReportListLight = (props) => {
    const number = props.i + 1;

    const handleAccordion = () => {
        ReportTaskStore.getInstance(props.type).setOpenTask(props.i, !props.isOpen);
    };


    return (
        <tr className={props.isActive ? "primary" : ""}>
            <td>{number}</td>
            <td>{props.name}</td>
            <td>
                <div>
                    <Accordion>
                        <Card>
                            <Card.Header>
                                <Accordion.Toggle
                                    as={Button}
                                    variant="link"
                                    eventKey="1"
                                    onClick={handleAccordion}
                                >
                                    {!props.isOpen ? 'Показать' : 'Скрыть'}
                                </Accordion.Toggle>
                            </Card.Header>
                            <Collapse in={props.isOpen}>
                                <Card.Body>
                                    <ListGroup variant="flush">
                                        {
                                            props.tracks.map((track, i) => (
                                                <TrackReportListItemLight
                                                    i={i}
                                                    isActive={track.isActive}
                                                    dateStart={track.dateStart}
                                                    dateEnd={track.dateEnd}
                                                />
                                            ))
                                        }
                                    </ListGroup></Card.Body>
                            </Collapse>
                        </Card>
                    </Accordion>
                </div>
            </td>
            <td>{new TimeFormat(props.sumTime).format()}</td>
        </tr>
    )
};

export default memo(TrackReportListLight);

