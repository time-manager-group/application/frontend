import React from 'react';
import EventToasterWrapper from "../EventToasterWrapper";
import {observer} from "mobx-react";

// noinspection JSUnusedLocalSymbols
/**
 *
 */
const EventToaster = observer(({store}) => {
    const removeEvent = (item) => {
        store.removeEvent(item);
    };


    return (
        <>
            <div
                aria-live="polite"
                aria-atomic="true"
                style={{
                    position: 'relative',
                    minHeight: '200px',
                }}
            >
                <div
                    style={{
                        position: 'fixed',
                        bottom: '25px',
                        zIndex: 99999,
                        right: 0,
                    }}
                >
                    {
                        store.events.map((element) =>
                            <EventToasterWrapper
                                item={element}
                                title={element.title}
                                timeTitle={element.timeTitle}
                                isAutoRemove={element.isAutoRemove}
                                text={element.text}
                                handleClose={removeEvent}
                            />
                        )}
                </div>
            </div>
        </>
    );
});

export default EventToaster;
