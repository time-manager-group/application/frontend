import React from 'react';

import '../../styles/TableItem.css';
import classes from './style.module.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import ScrollMenu from "react-horizontal-scrolling-menu";
import {Button} from "react-bootstrap";
import {observer} from "mobx-react";

/**
 * Горизонтальный scroll-список.
 *
 * @param props
 * @returns {JSX.Element}
 * @constructor
 */
const NavReportScroll = observer(({observer, onSelect}) => {
    const MenuItem = ({text, selected}) => {
        return <Button
            variant={'outline-secondary'}
            className={classes['menu-item']}
            active={selected}
        >{text}</Button>;
    };

    const Menu = (list, selected) =>
        list.map(el => {
            const {name, id} = el;

            return <MenuItem text={name} key={id} selected={selected}/>;
        });

    const menuItems = Menu(observer.items, observer.selectedId);
    const menuScrollingRef = React.createRef();

    let autoActiveScrollTimeout = null;


    const handleMouseOver = () => {
        if (document.documentElement.scrollHeight <= document.body.scrollHeight) {
            document.body.style.overflow = "hidden";
            document.body.style.paddingRight = "15px";
        }

        if (autoActiveScrollTimeout) {
            clearInterval(autoActiveScrollTimeout);
        }
    }

    const handleMouseLeave = () => {
        autoActiveScrollTimeout = setTimeout(() => {
            if (menuScrollingRef.current) {
                menuScrollingRef.current.scrollTo(observer.selectedId);

            }
        }, 10000);

        document.body.style.overflow = null;
        document.body.style.paddingRight = null;
    }

    return (
        <div className={classes['scroll-menu']}
             onMouseOver={handleMouseOver}
             onMouseLeave={handleMouseLeave}
        >
            <ScrollMenu
                data={menuItems}
                selected={observer.selectedId}
                onSelect={onSelect}
                scrollToSelected={true}
                ref={menuScrollingRef}
                itemClass={classes['scroll-menu-item-wrapper']}
                menuClass={classes['scroll-menu-class']}
                inertiaScrolling={true}
                transition={2}
                inertiaScrollingSlowdown={0.1}
                scrollBy={1}
            />
        </div>
    );
});


export default NavReportScroll;
