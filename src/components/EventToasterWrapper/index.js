import React from 'react';
import {Toast} from "react-bootstrap";

// noinspection JSUnusedLocalSymbols
/**
 *
 */
const EventToasterWrapper = (props) => {
    setTimeout(() => {
        if (
            props &&
            props.hasOwnProperty('isAutoRemove') &&
            props.isAutoRemove &&
            props.hasOwnProperty('handleClose')
        ) {
            props.handleClose(props.item);
        }
    }, 10000);

    return (
        <Toast onClose={() => {
            if (props && props.hasOwnProperty('handleClose')) {
                props.handleClose(props.item);
            }
        }}>
            <Toast.Header>
                <strong className="mr-auto">{props.title}</strong>
                <small>{props.timeTitle}</small>
            </Toast.Header>
            <Toast.Body>{props.text}</Toast.Body>
        </Toast>
    );

}

export default EventToasterWrapper;
