import React from 'react';
import classes from './style.module.css';
import {Spinner} from "react-bootstrap";
import {observer} from "mobx-react";


/**
 * Главный прелодер.
 */
const TrackWorkSpacePreloader = observer(({store}) => {

    return (
        <div className={classes['preloader'] + ' ' + (+!store.isShow ? 'opacity-hide' : classes['preloader-z'])}>
            <div className={classes['preloader-container']}>
                    <Spinner className={classes['preloader-spinner-style']} animation="border" variant="secondary"/>
            </div>
        </div>
    )
});


export default TrackWorkSpacePreloader;
