import React from 'react';
import {Alert, Badge, ProgressBar, Table} from 'react-bootstrap';
import "react-datetime/css/react-datetime.css";
import TimeFormat from '../../services/TimeFormat';
import '../../styles/TableItem.css';
import classes from './style.module.css';
import '../../styles/Animation.css';
import "react-datetime/css/react-datetime.css";
import {observer} from "mobx-react";
import TrackReportListLight from "../TrackReportListLight";


/**
 *
 */
const TrackReportLight = observer((
    {
        store,
        className,
        variant,
        text,
        limitSeconds,
        autoHidden
    }) => {


    return (
        <div className={ (autoHidden && !store.sumTypeTime) ? (className + ' ' + classes['hidden']) : className}>
            <Alert className={classes['alert-container'] + ' ' +
            ((limitSeconds && limitSeconds < store.sumTypeTime) ? 'animate-pulse' : '')
            } variant={variant}>
                <div className={classes['alert-text']}>
                    {text}
                    <ProgressBar animated={store.isActive}
                                 className={
                                     !limitSeconds ? classes['hidden'] : classes['progress']
                                 }
                                 striped variant={variant} now={
                        (store.sumTypeTime / limitSeconds) * 100
                    }/>
                </div>
                <div>
                    <Badge className={
                        !store.sumTypeTime ? classes['hidden'] : classes['badge']
                    }
                           variant={variant}>
                        Затрачено: {new TimeFormat(store.sumTypeTime).format()}
                    </Badge>
                    <Badge className={
                        !limitSeconds ? classes['hidden'] : classes['badge']
                    } variant={variant}>
                        {limitSeconds < store.sumTypeTime ? 'Перевыполнено' : 'Осталось'}
                        : {new TimeFormat(Math.abs(limitSeconds - store.sumTypeTime)).format()}
                    </Badge>
                </div>
            </Alert>
            <Table striped bordered hover size="sm">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Задача</th>
                    <th>Треки</th>
                    <th>Затрачено</th>
                </tr>
                </thead>
                <tbody>
                {
                    store.tasks.map((groupElement, i) => (
                        <TrackReportListLight
                            i={i}
                            name={groupElement.name}
                            sumTime={groupElement.sumTime}
                            type={groupElement.type}
                            isActive={groupElement.isActive}
                            isOpen={groupElement.isOpen}
                            tracks={groupElement.tracks}
                        />
                    ))
                }
                </tbody>
            </Table>
        </div>
    )
});

export default TrackReportLight;
