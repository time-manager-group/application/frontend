import React from 'react';
import {Alert, Badge, ListGroup, ProgressBar, Table} from 'react-bootstrap';
import "react-datetime/css/react-datetime.css";
import TimeDropper from '../../services/TimeDropper';
import TimeFormat from '../../services/TimeFormat';
import '../../styles/TableItem.css';
import moment from 'moment/moment';
import TrackReportList from '../TrackReportList';
import classes from './style.module.css';
import '../../styles/Animation.css';

/**
 *
 */
export class TrackReport extends React.Component {
    lastTrackItem;
    lastTrackItemName;
    lastTrackItemActive;
    lastTrackItemType;
    lastCount;

    constructor(props) {
        super(props);

        this.state = {
            reportList: [],
            currentTrack: null,
            resetCollapse: false,
            sumTypeTime: null,
            show: !this.props.autoShow,
        };
    }

    // noinspection JSCheckFunctionSignatures
    componentWillReceiveProps(props) {
        const newReportList = [];

        let setCurrentTrack = null;
        let resetCollapse = false;
        let lastTrackItem = null;
        let lastCount = 0;
        let sumTypeTime = 0;

        props.reportState.map((item) => {
            if (item.type === this.props.type) {
                lastCount++;
                const task = this.getTask(newReportList, item.name);

                const taskSumTime = new TimeDropper(item.dateStart, item.dateEnd).getDiffSeconds();

                task.sumTime += taskSumTime;
                sumTypeTime += taskSumTime;

                setCurrentTrack = {
                    dateStart: item.dateStart,
                    dateEnd: item.dateEnd,
                    isActive: item.isActive
                };

                task.trackList.push(setCurrentTrack);
            }

            lastTrackItem = item;
        });

        if (props.reportState.length > 0 && (
            (this.lastTrackItem ? this.lastTrackItemActive !== lastTrackItem.isActive : false) ||
            (this.lastTrackItem ? this.lastTrackItemType !== lastTrackItem.type : false) ||
            (this.lastTrackItem ? this.lastTrackItem !== lastTrackItem : false) ||
            (this.lastTrackItem ? this.lastTrackItemName !== lastTrackItem.name : false)
        )
        ) {
            resetCollapse = true;
        }

        this.lastTrackItem = lastTrackItem;
        this.lastTrackItemName = lastTrackItem ? lastTrackItem.name : null;
        this.lastTrackItemActive = lastTrackItem ? lastTrackItem.isActive : null;
        this.lastTrackItemType = lastTrackItem ? lastTrackItem.type : null;
        this.lastCount = lastCount;

        this.setState({
            reportList: newReportList,
            currentTrack: setCurrentTrack,
            resetCollapse: resetCollapse,
            sumTypeTime: sumTypeTime,
            show: this.props.autoShow ? lastCount > 0 : true,
        });
    }

    clearFill(value) {
        return value.trim().replace(/\s+/gui, ' ').toLowerCase();
    }

    getTask(reportList, name) {
        for (const reportListElement of reportList) {
            if (this.clearFill(reportListElement.name)
                === this.clearFill(name)) {
                return reportListElement;
            }
        }

        const newReportListElement = {
            name: name,
            sumTime: 0,
            trackList: []
        };

        reportList.push(newReportListElement);

        return newReportListElement;
    }

    render() {
        const items = [];

        this.state.reportList.map((item, i) => {
            const trackList = [];
            let isOpen = false;
            // noinspection JSUnresolvedVariable
            item.trackList.map((trackItem, trackI) => {
                    if (trackItem === this.state.currentTrack && trackItem.isActive) {
                        isOpen = true;
                    }
                    const isPrimaryTrack = item.trackList.length === trackI + 1 && isOpen;
                    // noinspection JSUnresolvedVariable
                    const isDifferentDate = ((trackItem.dateEnd.getTime() / 1000) - (trackItem.dateStart.getTime() / 1000)) > 3600 * 24;

                    trackList.push(<ListGroup.Item className={isPrimaryTrack ? "primary" : ""}>
                        [{trackI + 1}] {
                        moment(trackItem.dateStart).format('YYYY.MM.DD')} <b>{
                            moment(trackItem.dateStart).format('HH:mm:ss')
                    } -</b> {isDifferentDate ? moment(trackItem.dateEnd).format('YYYY.MM.DD') : ''} <b>{
                        moment(trackItem.dateEnd).format('HH:mm:ss')}</b> (
                        {
                            new TimeDropper(trackItem.dateStart, trackItem.dateEnd).format()
                        })
                    </ListGroup.Item>);
                }
            );
            const key = i + 1;
            // noinspection JSUnresolvedVariable
            items.push(
                <tr className={isOpen ? "primary" : ""}>
                    <td>{key}</td>
                    <td>{item.name}</td>
                    <td>
                        <TrackReportList
                            trackList={trackList}
                            isOpen={isOpen}
                           /* setIsOpen={this.state.resetCollapse}*/
                        />
                    </td>
                    <td>{new TimeFormat(item.sumTime).format()}</td>
                </tr>
            );
        });

        return (
            <div className={!this.state.show ? (this.props.className + ' ' + classes['hidden']) : this.props.className}>
                <Alert className={classes['alert-container'] + ' ' +
                ((this.props.limitSeconds && this.props.limitSeconds < this.state.sumTypeTime) ? 'animate-pulse' : '')
                } variant={this.props.variant}>
                    <div className={classes['alert-text']}>
                        {this.props.text}
                        <ProgressBar animated={
                            this.state.currentTrack ? this.state.currentTrack.isActive : false
                        } className={
                            !this.props.limitSeconds ? classes['hidden'] : classes['progress']
                        }
                                     striped variant={this.props.variant} now={
                            (this.state.sumTypeTime / this.props.limitSeconds) * 100
                        }/>
                    </div>
                    <div>
                        <Badge className={
                            !this.state.sumTypeTime ? classes['hidden'] : classes['badge']
                        }
                               variant={this.props.variant}>
                            Затрачено: {new TimeFormat(this.state.sumTypeTime).format()}
                        </Badge>
                        <Badge className={
                            !this.props.limitSeconds ? classes['hidden'] : classes['badge']
                        } variant={this.props.variant}>
                            {this.props.limitSeconds < this.state.sumTypeTime ? 'Перевыполнено' : 'Осталось'}
                            : {new TimeFormat(Math.abs(this.props.limitSeconds - this.state.sumTypeTime)).format()}
                        </Badge>
                    </div>
                </Alert>
                <Table striped bordered hover size="sm">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Задача</th>
                        <th>Треки</th>
                        <th>Затрачено</th>
                    </tr>
                    </thead>
                    <tbody>
                    {
                        items.map(item => (item))
                    }
                    </tbody>
                </Table>
            </div>
        )
    }
}

export default TrackReport;
