import React from 'react';
import {Table} from 'react-bootstrap';
import "react-datetime/css/react-datetime.css";
import TrackItem from '../TrackItem';
import TrackItemLight from "../TrackItemLight";

/**
 *
 */
export class TrackList extends React.Component {
    constructor(props) {
        super(props);
        this.handleChangeTrackList = this.handleChangeTrackList.bind(this);
        this.handleRemove = this.handleRemove.bind(this);
        this.state = {
            text: []
        };
    }

    // noinspection JSCheckFunctionSignatures
    componentWillReceiveProps(props) {
        return this.setState({text: props.reportState ?? []});
    }

    handleChangeTrackList(item, state) {
        this.state.text.map((subItem) => {
            if (subItem === item) {
                subItem.isActive = state.isActive;
                subItem.name = state.name;
                subItem.dateStart = state.dateStart;
                subItem.type = state.type;
                subItem.dateEnd = state.dateEnd;
            }
        });

        this.props.listChange(this.state.text);

        return this.setState(this.state);
    }

    handleRemove(item) {
        const array = [...this.state.text];

        const index = array.indexOf(item);
        if (index !== -1) {
            array.splice(index, 1);
            const newTracks = {text: array};
            this.props.listChange(newTracks.text);

            return this.setState(newTracks);
        }
    }

    render() {
        const items = [];

        this.state.text.map((item, i) => {
         /*   items.push(
                <TrackItem
                    number={i + 1}
                    name={item.name}
                    dateStart={item.dateStart}
                    dateEnd={item.dateEnd}
                    isActive={item.isActive}
                    type={item.type}
                    item={item}
                    onChange={this.handleChangeTrackList}
                    handleRemove={this.handleRemove}
                />
            );*/
            items.push(
                <TrackItemLight
                    number={i + 1}
                    name={item.name}
                    dateStart={item.dateStart}
                    dateEnd={item.dateEnd}
                    isActive={item.isActive}
                    type={item.type}
                    item={{}}
                    id={item.id}
                    onChange={() => {}}
                    handleRemove={() => {}}
                />
            );
        });

        return (
            <Table striped bordered hover size="sm">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Трек</th>
                    <th>Старт</th>
                    <th>Стоп</th>
                    <th>Затрачено</th>
                    <th>Тип трека</th>
                    <th>Действие</th>
                </tr>
                </thead>
                <tbody>
                {
                    items.map(item => (item))
                }
                </tbody>
            </Table>
        )
    }
}

export default TrackList;
