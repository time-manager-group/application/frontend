import React from 'react';

import {Badge, Navbar} from "react-bootstrap";
import {observer} from "mobx-react";
import {Speedometer, Speedometer2} from "react-bootstrap-icons";
import {createDateTimeWithAppendTime, getFormattedDate, getTrackWithMaximalTime, getTrackWithMinimalTime, isSameDay, typeSumter} from '../../services/TimeHelper';
import {toJS} from 'mobx';
import TimeFormat from '../../services/TimeFormat';

const effectTimeSeconds = 5 * 60 * 60;
const allTimeSeconds    = 9 * 60 * 60;

const effectingTimeFactor = effectTimeSeconds / (allTimeSeconds);


const getVariant = (percent) => {
    if (percent <= 40) {
        return 'danger';
    }
    if (percent > 40 && percent <= 64) {
        return 'warning';
    }
    if (percent > 64 && percent <= 84) {
        return 'secondary';
    }
    if (percent > 84 && percent <= 94) {
        return 'primary';
    }
    if (percent > 94 && percent <= 109) {
        return 'success';
    }
    if (percent > 109 && percent <= 124) {
        return 'warning';
    }
    if (percent > 124) {
        return 'danger';
    }

    return 'info';
}

const existsActiveTracks = (items) => {
    for (const item of items) {
        if (item.isActive) {
            return true;
        }
    }
    return false;
}

const QualityMetric = observer(({store}) => {
    const items = toJS(store.items);

    const minTrack = getTrackWithMinimalTime(items);
    const maxTrack = getTrackWithMaximalTime(items);

    let sumUseless   = typeSumter(items, 'useless');
    let sumUndefined = typeSumter(items, 'undefined');
    let sumUseful    = typeSumter(items, 'useful');
    let sumAll       = Math.min(allTimeSeconds, sumUseless + sumUndefined + sumUseful); //Максимально выше установленного времени не должен быть расчет

    let actualEffectingTimeFactor = sumAll ? (sumUseful / sumAll) : 0;


    let actualEffectingNotUselessTimeFactor = sumAll ? ((sumUseful + sumUndefined) / sumAll) : 0;

    let percentQualityUsefulTime     = ((actualEffectingTimeFactor / effectingTimeFactor) * 100).toFixed(2);
    let percentQualityNotUselessTime = ((actualEffectingNotUselessTimeFactor / effectingTimeFactor) * 100).toFixed(2);

    let calculatedDateFinishUsefulTime     = null;
    let calculatedDateFinishNotUselessTime = null;

    if (minTrack) {
        if (percentQualityUsefulTime > 0) {
            calculatedDateFinishUsefulTime = createDateTimeWithAppendTime(minTrack.dateStart, effectTimeSeconds / actualEffectingTimeFactor);
        } else {
            calculatedDateFinishUsefulTime = createDateTimeWithAppendTime(maxTrack.dateEnd, allTimeSeconds);
        }
        if (percentQualityNotUselessTime > 0) {
            calculatedDateFinishNotUselessTime = createDateTimeWithAppendTime(minTrack.dateStart, effectTimeSeconds / actualEffectingNotUselessTimeFactor);
        } else {
            calculatedDateFinishNotUselessTime = createDateTimeWithAppendTime(maxTrack.dateEnd, allTimeSeconds);
        }
    }

    if (!existsActiveTracks(items)) { //Если отчет закрыт
        if (sumUseful < effectTimeSeconds) {
            percentQualityUsefulTime = ((sumUseful / effectTimeSeconds) * 100).toFixed(2);
        }
        if ((sumUseful + sumUndefined) < effectTimeSeconds) {
            percentQualityNotUselessTime = (((sumUseful + sumUndefined) / effectTimeSeconds) * 100).toFixed(2);
        }
    }

    if (percentQualityUsefulTime * 100 === 0) {
        percentQualityUsefulTime = -((sumAll / effectTimeSeconds) * 100).toFixed(2);
    }

    if (percentQualityNotUselessTime * 100 === 0) {
        percentQualityNotUselessTime = -((sumAll / effectTimeSeconds) * 100).toFixed(2);
    }

    //Определяем насколько мы не дорабатываем времени и сколько у нас есть в запасе отдыха--------------------------//
    const shiftNotUselessTime = ((sumUseful + sumUndefined) - (effectingTimeFactor * sumAll)) / (1 - effectingTimeFactor);
    const shiftUsefulTime     = (sumUseful - (effectingTimeFactor * sumAll)) / (1 - effectingTimeFactor);

    const shiftNotUselessFormatTime = new TimeFormat(Math.abs(shiftNotUselessTime)).formatShort();
    const shiftNotUselessPole       = shiftNotUselessTime >= 0 ? '+' : '-';

    const shiftUsefulFormatTime = new TimeFormat(Math.abs(shiftUsefulTime)).formatShort();
    const shiftUsefulPole       = shiftUsefulTime >= 0 ? '+' : '-';


    const shiftUNotUselessContent = Math.abs(shiftNotUselessTime) >= 1 ? <> [{shiftNotUselessPole}{shiftNotUselessFormatTime}]</> : '';
    const shiftUsefulContent      = Math.abs(shiftUsefulTime) >= 1 ? <> [{shiftUsefulPole}{shiftUsefulFormatTime}]</> : '';
    //-------------------------------------------------------------------------------------------------------------//

    return (
        <Navbar.Text style={{display: 'flex', gap: 3}}>
            <Badge title={'Процент эффективности неопределенного и полезного времени'}
                   variant={getVariant(percentQualityNotUselessTime)}>не и по
                                                                      : {percentQualityNotUselessTime}%{shiftUNotUselessContent} {' '}<Speedometer/></Badge>
            {calculatedDateFinishNotUselessTime && <Badge title={'Конец в какое время предположительно по эффективности неопределенного и полезного времени'}
                                                          variant={'secondary'}>{getFormattedDate(calculatedDateFinishNotUselessTime, !isSameDay(minTrack.dateStart, calculatedDateFinishNotUselessTime))}</Badge>}
            <Badge title={'Процент полезного времени'} variant={getVariant(percentQualityUsefulTime)}>по
                                                                                                      : {percentQualityUsefulTime}%{shiftUsefulContent} {' '}<Speedometer2/></Badge>
            {calculatedDateFinishUsefulTime && <Badge title={'Конец в какое время предположительно по эффективности полезного времени'}
                                                      variant={'success'}>{getFormattedDate(calculatedDateFinishUsefulTime, !isSameDay(minTrack.dateStart, calculatedDateFinishUsefulTime))}</Badge>}
        </Navbar.Text>
    )
});

export default QualityMetric;
