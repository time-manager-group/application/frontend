import React from 'react';


import classes from './style.module.css';
import TrackProgress from "../TrackProgress";
import TrackTasks from "../TrackTasks";
import TrackInput from "../TrackInput";
import {observer} from "mobx-react";
import {toJS} from "mobx";
import TrackListLight from "../TrackListLight";
import TrackReportLight from "../TrackReportLight";
import ReportTaskStore from "../../stores/ReportTaskStore";
import ReportStore from "../../stores/ReportStore";
import HeaderStatusChecker from "../../services/HeaderStatusChecker";


/**
 * Область с треками отчета.
 */
const TrackRuntime = observer(({store}) => {
    const appendTrack = (value) => {
        const reportId = ReportStore.getInstance().getSelectedReportId();

        ReportTaskStore.removeAllTasksState();
        store.appendTrack(reportId, value);
    };

    const items = toJS(store.items);

    ReportTaskStore.fillAllTracks(items);
    HeaderStatusChecker.checkStatus(items);

    const reportUselessStore = ReportTaskStore.getInstance('useless');
    const reportUndefinedStore = ReportTaskStore.getInstance('undefined');
    const reportUsefulStore = ReportTaskStore.getInstance('useful');

    return (
        <div className={store.isShow ? 'opacity-show' : classes['hide']}>
            <TrackListLight
                items={items}
            />
            <TrackInput
                reportState={items}
                taskTemplateList={store.taskTemplateList}
                maxCount={5}
                onGreet={appendTrack}
            />
            <div className={classes['table-group-container']}>
                <TrackTasks
                    reportState={items}
                    taskTemplateList={store.taskTemplateList}
                    onClick={appendTrack}
                />
            </div>
            <div className={classes['table-group-container']}>
                <TrackProgress
                    reportState={items}
                    limitSecondsAll={9 * 60 * 60}
                    limitSecondsUseless={4 * 60 * 60}
                    limitSecondsUndefined={null}
                    limitSecondsUseful={5 * 60 * 60}
                />
            </div>
            <div className={classes['table-group-container']}>
                <TrackReportLight
                    className={classes['table-container']}
                    store={reportUselessStore}
                    text={'Бесполезные задачи'}
                    limitSeconds={4 * 60 * 60}
                    autoHidden={false}
                    variant={'info'}
                />
                <TrackReportLight
                    className={classes['table-container']}
                    store={reportUndefinedStore}
                    text={'Неопределенные задачи'}
                    limitSeconds={null}
                    autoHidden={true}
                    variant={'secondary'}
                />
                <TrackReportLight
                    className={classes['table-container']}
                    store={reportUsefulStore}
                    text={'Полезные задачи'}
                    limitSeconds={5 * 60 * 60}
                    autoHidden={false}
                    variant={'success'}
                />
                {/*                <TrackReport
                    className={classes['table-container']}
                    reportState={items}
                    type={'useless'}
                    text={'Бесполезные задачи'}
                    limitSeconds={4 * 60 * 60}
                    autoShow={false}
                    variant={'info'}
                />
                <TrackReport
                    className={classes['table-container']}
                    reportState={items}
                    text={'Неопределенные задачи'}
                    type={'undefined'}
                    limitSeconds={null}
                    autoShow={true}
                    variant={'secondary'}
                />
                <TrackReport
                    className={classes['table-container']}
                    reportState={items}
                    text={'Полезные задачи'}
                    type={'useful'}
                    limitSeconds={5 * 60 * 60}
                    autoShow={false}
                    variant={'success'}
                />*/}
            </div>
        </div>
    )
});

export default TrackRuntime;
