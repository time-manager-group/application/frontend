import React from 'react';
import {observer} from 'mobx-react';
import {Bar} from 'react-chartjs-2';
import Constants from '../../../services/Constants';
import {Badge, Container, Navbar} from 'react-bootstrap';
import classes from './style.module.css';
import {toJS} from 'mobx';
import TimeFormat from '../../../services/TimeFormat';

const TrackRating = observer(({store}) => {
    /*
        const usefulType    = store.items.filter(x => x['type'] === 'useful');
        const undefinedType = store.items.filter(x => x['type'] === 'undefined');
        const uselessType   = store.items.filter(x => x['type'] === 'useless');*/

    const data = {
        labels:   store.items.map((x, i) => [`[${i + 1}] ` + x['name'], ' (' + x['timeFormat'] + ')']),
        datasets: [
            {
                label:           '# Количество секунд',
                data:            store.items.map(x => x['allSeconds']),
                backgroundColor: store.items.map(x => Constants.RATING_BACKGROUND_COLORS.get(x['type'])),
                borderColor:     store.items.map(x => Constants.RATING_BORDER_COLORS.get(x['type'])),
                borderWidth:     1
            }
        ]
    };

    const options = {
        scales:              {
            yAxes: [
                {
                    barThickness: 1,
                    ticks:        {
                        autoSkip: false
                    }
                }

            ]
        },
        maintainAspectRatio: false,
        responsive:          true,
        indexAxis:           'y',
        elements:            {
            bar: {
                borderWidth: 2
            }
        },
        onClick:             function(evt, element) {
            if (element.length > 0) {
                store.setClickLabel(store.items[element[0].index].name);
            }
        },
        plugins:
                             {
                                 legend: {
                                     position: 'bottom',
                                     display:  false

                                 },
                                 title:  {
                                     display: true,
                                     text:    'Рейтинг затраченного времени по задачам'
                                 }

                             }
    };


    const flatArrayUseless   = [];
    const flatArrayUseful    = [];
    const flatArrayUndefined = [];

    for (const item of store.items) {
        let taskName       = item.name;
        const tasks        = taskName.replace(/[.|:]/g, ':').split(':').map(e => e.trim());
        const itemWithFlat = {item: toJS(item), tasks};
        switch (item.type) {
            case 'useful':
                flatArrayUseful.push(itemWithFlat);
                break;
            case 'useless':
                flatArrayUseless.push(itemWithFlat);
                break;
            default:
                flatArrayUndefined.push(itemWithFlat);
                break;
        }
    }

    function buildTree(flatArray) {
        const tree = [];

        flatArray.forEach(item => {
            let {tasks, item: taskItem} = item;
            let currentNode             = tree;

            for (let i = 0; i < tasks.length; i++) {
                const node         = currentNode.find(node => node.name === tasks[i]);
                const realTaskItem = tasks.length === i + 1 ? taskItem : null;

                if (node) {
                    if (realTaskItem) {
                        currentNode = node.children;
                        currentNode.push({name: tasks[i], isComplete: true, children: [], item: realTaskItem})
                    } else {
                        currentNode = node.children;
                    }
                } else {
                    let newNode = {name: tasks[i], isComplete: false, children: [], item: realTaskItem};
                    if (realTaskItem) {
                        newNode = {name: tasks[i], isComplete: false, children: [{name: tasks[i], isComplete: true, children: [], item: realTaskItem}], item: null};
                    }
                    currentNode.push(newNode);
                    currentNode = newNode.children;
                }
            }
        });

        return tree;
    }

    const calcChildrenSeconds = (nodeList) => {
        if (nodeList.length === 0) {
            return 0;
        }

        let seconds = 0;
        for (const node of nodeList) {
            seconds += node.item !== null ? node.item.allSeconds : 0;
            seconds += calcChildrenSeconds(node.children);
        }

        return seconds;
    }

    const collapseNode = (node) => {
        let seconds = node.item !== null ? node.item.allSeconds : 0;
        if (node.children.length === 1 && node.children[0].isComplete) {
            return collapseNode({...node.children[0], seconds, isComplete: false, name: node.name});
        }
        if (node.children.length === 1) {
            seconds += node.children[0].item !== null ? node.children[0].item.allSeconds : 0;
            return collapseNode({
                name:       node.name + ': ' + node.children[0].name,
                children:   node.children[0].children,
                item:       node.children[0].item,
                seconds:    seconds + calcChildrenSeconds(node.children[0].children),
                isComplete: node.children[0].isComplete
            });
        }
        if (node.children.length === 0) {
            return {...node, seconds};
        }
        const newNode = {name: node.name, children: [], item: node.item, seconds: seconds + calcChildrenSeconds(node.children), isComplete: node.isComplete};
        for (const child of node.children) {
            newNode.children.push(collapseNode(child));
        }
        return newNode;
    }

    const collapseNodeList = (nodeList) => {
        const collapsedNodeList = [];
        for (const node of nodeList) {
            collapsedNodeList.push(collapseNode(node));
        }
        return collapsedNodeList;
    }

    const treeUseless   = collapseNodeList(buildTree(flatArrayUseless));
    const treeUseful    = collapseNodeList(buildTree(flatArrayUseful));
    const treeUndefined = collapseNodeList(buildTree(flatArrayUndefined));
    const renderTree    = (tree, level = 1, parentLi = '') => {

        tree.sort(function(a, b) {

            if (a.isComplete < b.isComplete) return 1;
            if (a.isComplete > b.isComplete) return -1;
            return 0;
        });


        let i = 0;
        return tree.map((node) => {
                i++;
                // const currentLi = parentLi ? `[${parentLi}] ${i}` : `${i}`;
                const currentLi = `${i}`;
                let className   = 'list-fake';
                if (node.item) {
                    switch (node.item.type) {
                        case 'useful':
                            className = 'list-useful';
                            break;
                        case 'useless':
                            className = 'list-useless';
                            break;
                        default:
                            className = 'list-undefined';
                            break;
                    }
                }
                return <div className={classes[className] + (level > 3 ? (' ' + classes['list-hidden']) : '')}>
                    <h4><Badge>{currentLi}</Badge>{!node.isComplete ? (node.name + ' ') : <Badge>[Основная задача]</Badge>} <Badge
                        variant={'secondary'}>{new TimeFormat(node.seconds).formatShort()}</Badge></h4>
                    {renderTree(node.children, level + 1, parentLi ? parentLi + '.' + String(i) : String(i))}
                </div>
            }
        );
    }
    // console.log(treeUseful);
    return (
        <>
            <div className={classes['root-list']}>
                {
                    renderTree([...treeUseful, ...treeUndefined, ...treeUseless])
                }
            </div>
            <Navbar bg="light">
                <Container>
                    <Navbar.Brand href="#">{store.clickLabel}</Navbar.Brand>
                </Container>
            </Navbar>
            <div style={{height: 80 * store.items.length}}>
                <Bar data={data} options={options}/>
            </div>
        </>
    );
});

export default TrackRating;
