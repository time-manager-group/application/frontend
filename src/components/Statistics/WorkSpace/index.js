import React from 'react';
import TrackRatingStore from '../../../stores/TrackRatingStore';
import TrackRating from '../TrackRating';
import StatisticsWorkSpaceLoader from '../../../scripts/StatisticsWorkSpaceLoader';
import TableSumRating from '../TableSumRating';
import TableSumRatingStore from '../../../stores/TableSumRatingStore';
import TaskFilter from '../TaskFilter';
import TaskFilterStore from '../../../stores/TaskFilterStore';


const WorkSpace = () => {
    const trackRatingStore    = TrackRatingStore.getInstance();
    const tableSumRatingStore = TableSumRatingStore.getInstance();
    const taskFilterStore     = TaskFilterStore.getInstance();
    StatisticsWorkSpaceLoader.getInstance().load();

    return (
        <>
            <div style={{
                display:         'flex',
                margin:          '10px',
                'border-radius': '3px'
            }} class={"border border-secondary"}>
                <div style={{
                    flex:    'auto',
                    padding: '10px'
                }}>
                    <TaskFilter store={taskFilterStore}/>
                </div>
                <div style={{
                    width:         '22%',
                    'margin-left': '-1px'
                }}>
                    <TableSumRating store={tableSumRatingStore}/>
                </div>
            </div>
            <TrackRating store={trackRatingStore}/>
        </>
    )
};

export default WorkSpace;
