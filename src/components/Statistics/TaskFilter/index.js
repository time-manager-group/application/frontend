import React from 'react';
import {Col, Form, Row} from 'react-bootstrap';
import Constants from '../../../services/Constants';
import Datetime from 'react-datetime';
import Button from 'react-bootstrap/Button';
import {observer} from 'mobx-react';

/**
 * @type {function({store: TaskFilterStore})}
 */
const TaskFilter = observer(({store}) => {
    return (
        <Form>
            <fieldset>
                <Form.Group as={Row} className="mb-3">
                    <Form.Label as="legend" column sm={2}>
                        Тип задач
                    </Form.Label>
                    <Row>
                        <Form.Check
                            type="checkbox"
                            checked={store.types.useful}
                            label={Constants.RATING_TYPE_NAMES.get('useful')}
                            onChange={(event) => {
                                store.setType('useful', event.target.checked);
                            }}
                            style={{paddingLeft: '2.25rem'}}
                        />
                        <Form.Check
                            type="checkbox"
                            checked={store.types.undefined}
                            label={Constants.RATING_TYPE_NAMES.get('undefined')}
                            onChange={(event) => {
                                store.setType('undefined', event.target.checked);
                            }}
                            style={{paddingLeft: '2.25rem'}}
                        />
                        <Form.Check
                            type="checkbox"
                            checked={store.types.useless}
                            label={Constants.RATING_TYPE_NAMES.get('useless')}
                            onChange={(event) => {
                                store.setType('useless', event.target.checked);
                            }}
                            style={{paddingLeft: '2.25rem'}}
                        />
                    </Row>
                </Form.Group>
            </fieldset>
            <Form.Group as={Row} className="mb-3">
                <Form.Label column sm="2">
                    Поисковое вхождение
                </Form.Label>
                <Col sm="8" style={{paddingLeft: '0px'}}>

                    <Form.Control
                        type="text"
                        value={store.searchText}
                        onChange={(event) => {
                            store.setSearchText(event.target.value);
                        }}
                    />
                </Col>
                <Col sm="2">
                    <Form.Check
                        type="checkbox"
                        checked={store.activeSearchOperators}
                        label={'Операторы `!`, `&`, `|`'}
                        onChange={(event) => {
                            store.setActiveSearchOperators(event.target.checked);
                        }}
                        style={{paddingLeft: '2.25rem'}}
                    />
                </Col>
            </Form.Group>
            <Row className="mb-3">
                <Form.Label column sm="2">
                    Период дат
                </Form.Label>
                <Form.Group as={Col} md="4" style={{paddingLeft: '0px'}}>
                    <Form.Label>От даты</Form.Label>
                    <Datetime
                        value={store.dateFrom}
                        dateFormat="DD.MM.YYYY"
                        timeFormat="HH:mm:ss"
                        onChange={(value) => {
                            store.setDateFrom(value);
                        }}
                    />
                </Form.Group>
                <Form.Group as={Col} md="4">
                    <Form.Label>До даты</Form.Label>
                    <Datetime
                        value={store.dateTo}
                        dateFormat="DD.MM.YYYY"
                        timeFormat="HH:mm:ss"
                        onChange={(value) => {
                            store.setDateTo(value);
                        }}
                    />
                </Form.Group>
            </Row>
            <Form.Group as={Row} className="mb-3">
                <Form.Label column sm="2">
                    Лимит
                </Form.Label>
                <Col sm="2" style={{paddingLeft: '0px'}}>
                    <Form.Control
                        as="select"
                        defaultValue="5"
                        value={store.limit}
                        onChange={(event) => {
                            store.setLimit(event.target.value);
                        }}
                    >
                        <option value="5">5</option>
                        <option value="10">10</option>
                        <option value="15">15</option>
                        <option value="30">30</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                        <option value="150">150</option>
                        <option value="all">Все</option>
                    </Form.Control>
                </Col>
            </Form.Group>
            <Button variant="primary" onClick={() => {
                store.filter();
            }}>
                Показать
            </Button>
        </Form>
    );
});

export default TaskFilter;