import React from 'react';
import {observer} from 'mobx-react';
import {Badge, Table} from 'react-bootstrap';
import Constants from '../../../services/Constants';

const TableSumRating = observer(({store}) => {

    return (
        <>
            <Table responsive="sm">
                <tbody>
                {
                    store.items.map((item) =>
                        <tr style={{'color': Constants.RATING_BACKGROUND_COLORS.get(item.type)}}>
                            <th>
                                    {Constants.RATING_TYPE_NAMES.get(item.type)}
                            </th>

                            <th>
                                {item.tasksCount}
                            </th>
                            <th>
                                {item.timeSum}
                            </th>
                        </tr>
                    )
                }
                </tbody>
            </Table>

        </>
    );
});

export default TableSumRating;

