import React, {memo} from 'react';
import {ListGroup} from 'react-bootstrap';
import "react-datetime/css/react-datetime.css";
import '../../styles/TableItem.css';
import moment from 'moment/moment';
import '../../styles/Animation.css';
import "react-datetime/css/react-datetime.css";
import OnlineTime from "../OnlineTime";

/**
 *
 */
const TrackReportListItemLight = (props) => {
    const number = props.i + 1;

    const startDate = moment(props.dateStart).format('YYYY.MM.DD');
    const startTime = moment(props.dateStart).format('HH:mm:ss');

    const isDifferentDate = ((props.dateEnd.getTime() / 1000) - (props.dateStart.getTime() / 1000)) > 3600 * 24;

    const endDate = isDifferentDate ? moment(props.dateEnd).format('YYYY.MM.DD') : '';
    const endTime = moment(props.dateEnd).format('HH:mm:ss');

    return (
        <ListGroup.Item className={props.isActive ? "primary" : ""}>
            [{number}] {startDate} <b>{startTime} -</b> {endDate} <b>{endTime}</b>{' '}
            <OnlineTime
                dateStart={props.dateStart}
                dateEnd={props.dateEnd}
            />
        </ListGroup.Item>
    )
};

export default memo(TrackReportListItemLight);

