import React from 'react';
import classes from './style.module.css';
import {Spinner} from "react-bootstrap";
import {observer} from "mobx-react";


/**
 * Главный прелодер.
 */
const Preloader = observer(({store}) => {

    return (
        <div className={classes['preloader'] + ' ' + (+!store.isShow ? 'opacity-hide' : classes['preloader-z'])}>
            <div className={classes['preloader-container']}>
                <div className={classes['preloader-name']}>
                    Time Manager
                </div>
                <div className={classes['preloader-container-spinner']}>
                    <Spinner className={classes['preloader-spinner-style']} animation="border" variant="info"/>
                    <Spinner className={classes['preloader-spinner-style']} animation="border" variant="secondary"/>
                    <Spinner className={classes['preloader-spinner-style']} animation="border" variant="success"/>
                </div>
            </div>
        </div>
    )
});


export default Preloader;
