import React, {memo} from 'react';
import TimeDropper from "../../services/TimeDropper";


const OnlineTime = (props) => {
    const currentTime = new TimeDropper(
        props.dateStart,
        props.dateEnd)
        .format();

    return (
        <>
            {currentTime}
        </>
    );
};

export default memo(OnlineTime);
