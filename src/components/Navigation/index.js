import React from 'react';

import '../../styles/TableItem.css';
import {Nav, Navbar} from "react-bootstrap";
import {ArrowRight, Plus, Download, Upload, Envelope} from "react-bootstrap-icons";
import classes from './style.module.css';
import NavReportScroll from "../NavReportScroll";
import ReportStore from "../../stores/ReportStore";
import ReportModalStore from "../../stores/ReportModalStore";
import LocalReportLoader from "../../scripts/LocalReportLoader";
import HeaderStatus from "../HeaderStatus";
import HeaderStatusStore from "../../stores/HeaderStatusStore";
import Report from '../../services/Report';
import {downloadFile, uploadFile} from '../../services/FileOperator';
import TrackStore from '../../stores/TrackStore';
import QualityMetric from '../QualityMetric';
import Singleton from '../Note/components/Singleton';
import MetricWidget from '../MetricWidget';


// noinspection JSUnusedLocalSymbols
/**
 * Навигация.
 */
const Navigation = (props) => {
    const reportStore       = ReportStore.getInstance();
    const reportModalStore  = ReportModalStore.getInstance();
    const headerStatusStore = HeaderStatusStore.getInstance();
    const trackStore        = TrackStore.getInstance();

    const localReportLoader = new LocalReportLoader();

    const trackReportApi = new Report();

    /**
     * @type ReportApi
     */
    const reportApi = Singleton.getInstance('common-report-api');

    return (
        <Navbar bg="light" variant="light" sticky="top">
            <HeaderStatus
                store={headerStatusStore}
            />
            <Nav className="mr-auto">
                <Nav.Link
                    title={'Создать новый отчет'}
                    onClick={() => {
                        reportModalStore.show(false, localReportLoader.makeNewReport);
                    }}
                >Новый <Plus/></Nav.Link>
                <Nav.Link
                    title={'Выгрузить текущий отчет'}
                    onClick={() => downloadFile(trackReportApi.getExportFileUrl(reportStore.getSelectedReportId()))}
                >Выгрузить <Download/></Nav.Link>
                <Nav.Link
                    title={'Загрузить отчет'}
                    onClick={() => uploadFile('importReport', (file, removeUpload) => {
                        trackReportApi.importReport(file, null, () => window.location.reload());
                        removeUpload()
                    })}
                >Загрузить <Upload/></Nav.Link>
                <Nav.Link
                    title={'Загрузить в текущий отчет'}
                    onClick={() => uploadFile('importCurrentReport', (file, removeUpload) => {
                        trackReportApi.importReport(file, reportStore.getSelectedReportId(), () => window.location.reload());
                        removeUpload()
                    })}
                >Загрузить в текущий <Upload/> <Plus/> </Nav.Link>
                <Nav.Link
                    title={'Отправить отчет'}
                    onClick={() => reportApi.sendDailyReport()}
                >Отправить отчет <Envelope/></Nav.Link>
                <MetricWidget/>
            </Nav>

            <Nav className={classes['nav-right']}>
                <QualityMetric
                    store={trackStore}
                />
                <Nav.Link
                    onClick={localReportLoader.loadActiveReport}
                >Перейти к активному <ArrowRight/></Nav.Link>
            </Nav>
            <NavReportScroll
                observer={reportStore}
                onSelect={localReportLoader.loadReport}
            />
        </Navbar>
    )
}

export default Navigation;