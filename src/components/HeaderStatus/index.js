import React from 'react';

import {Badge, Navbar} from "react-bootstrap";
import {observer} from "mobx-react";
import {Calendar, Calendar2, Calendar2Check} from "react-bootstrap-icons";


const HeaderStatus = observer(({store}) => {
    return (
        <Navbar.Brand>Time Manager{' '}
            {store.status === 'active' ? (
                <Badge variant="info">active{' '}<Calendar2/></Badge>
            ) : ''}
            {store.status === 'empty' ? (
                <Badge variant="secondary">empty{' '}<Calendar/></Badge>
            ) : ''}
            {store.status === 'stopped' ? (
                <Badge variant="success">stopped{' '}<Calendar2Check/></Badge>
            ) : ''}
        </Navbar.Brand>
    )
});

export default HeaderStatus;
