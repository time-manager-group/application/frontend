import TrackField from "../TrackField";
import TrackTime from "../TrackTime";
import TrackType from "../TrackTypes";
import Button from "react-bootstrap/Button";
import {StopFill} from "react-bootstrap-icons";
import TrackRemoveButton from "../TrackRemoveButton";
import React, {memo} from 'react';
import TimeDropper from "../../services/TimeDropper";
import EventToasterStore from "../../stores/EventToasterStore";
import TrackStore from "../../stores/TrackStore";
import OnlineTime from "../OnlineTime";
import ReportTaskStore from "../../stores/ReportTaskStore";
import ReportStore from "../../stores/ReportStore";


const TrackItemLight =
    /**
     *
     * @param props {
     * {
     *  number:number,
     *  name:string,
     *  dateStart:Date,
     *  dateEnd:Date,
     *  isActive:boolean,
     *  type:string,
     *  id:number,
     *  onChange:callback,
     *  handleRemove:callback
     *  }
     * }
     */

        props => {
        const reportId = ReportStore.getInstance().getSelectedReportId();
        const store = TrackStore.getInstance();

        const handleNameChange = (value) => {
            const isOpenBefore = ReportTaskStore.getInstance(props.type).getTaskStateIsOpen(props.name, false);
            ReportTaskStore.getInstance(props.type).setIsOpenTaskStateByName(value, isOpenBefore);

            if (ReportTaskStore.isActiveTaskByName(value)) {
                ReportTaskStore.setIsOpenTaskStateByNameAllTypes(false);
                ReportTaskStore.getInstance(props.type).setIsOpenTaskStateByName(props.name, true);
            }

            store.updateTrack(reportId, props.id, {name: value});
        };

        const handleNameChangeFixed = (value, beforeValue) => {
            store.updateTrack(reportId, props.id, {name: value}, true, (track) => {
                track.name = beforeValue
            });
        };

        const handleChangeStart = (date) => {
            if (new TimeDropper(
                date,
                props.dateEnd).getDiffSecondsWithoutAbs() < 0) {
                validateTimeEvent();

                return false;
            }

            store.updateTrack(reportId, props.id, {dateStart: date});

            return true;
        };

        const handleChangeStartFixed = (date, beforeDate) => {
            store.updateTrack(reportId, props.id, {dateStart: date}, true, (track) => {
                track.dateStart = beforeDate;
            });
        };

        const handleChangeEnd = (date) => {
            if (new TimeDropper(
                props.dateStart,
                date).getDiffSecondsWithoutAbs() < 0) {
                validateTimeEvent();

                return false;
            }

            store.updateTrack(reportId, props.id, {dateEnd: date});

            return true;
        };

        const handleChangeEndFixed = (date, beforeDate) => {
            store.updateTrack(reportId, props.id, {dateEnd: date}, true, (track) => {
                track.dateEnd = beforeDate;
            });
        };

        const validateTimeEvent = () => {
            EventToasterStore.getInstance().appendEvent(
                {
                    title: 'Ошибка изменения границ трека "' + props.name + '"',
                    text: 'Невозможно изменить дату трека в отрицательную разность.'
                }
            );
        };

        const handleStop = () => {
            ReportTaskStore.getInstance(props.type).setIsOpenTaskStateByName(props.name, false);

            store.updateTrack(reportId, props.id, {isActive: false}, true, (track) => {
                track.isActive = true;
            });
        }

        const handleRemove = () => {
            ReportTaskStore.getInstance(props.type).setIsOpenTaskStateByName(props.name, false);
        };

        const handleChangeType = (type) => {
            const isOpenBefore = ReportTaskStore.getInstance(props.type).getTaskStateIsOpen(props.name, false);
            ReportTaskStore.getInstance(type).setIsOpenTaskStateByName(props.name, isOpenBefore);

            if (props.isActive) {
                ReportTaskStore.setIsOpenTaskStateByNameAllTypes(false);
                ReportTaskStore.getInstance(type).setIsOpenTaskStateByName(props.name, true);
            }

            const beforeType = props.type;

            store.updateTrack(reportId, props.id, {type: type}, true, (track) => {
                track.type = beforeType;
            });
        };

        return (
            <tr className={props.isActive ? 'primary' : ''}>
                <td>
                    {props.number}
                </td>
                <td>
                    <TrackField
                        value={props.name}
                        onChange={handleNameChange}
                        onChangeFixed={handleNameChangeFixed}
                    />
                </td>
                <td>
                    <TrackTime date={props.dateStart}
                               onChange={handleChangeStart}
                               onChangeFixed={handleChangeStartFixed}
                    />
                </td>
                <td hidden={props.isActive}>
                    <TrackTime date={props.dateEnd}
                               onChange={handleChangeEnd}
                               onChangeFixed={handleChangeEndFixed}
                    />
                </td>
                <td colSpan={props.isActive ? 2 : 1}>
                    <OnlineTime
                        dateStart={props.dateStart}
                        dateEnd={props.dateEnd}
                    />
                </td>
                <td>
                    <TrackType
                        focus={props.type}
                        onChange={handleChangeType}
                    />
                </td>
                <td>
                    <Button
                        hidden={!props.isActive}
                        variant="secondary"
                        size="sm"
                        title={'Остановить'}
                        onClick={handleStop}
                    >
                        <StopFill/>
                    </Button>{' '}
                    <TrackRemoveButton
                        id={props.id}
                        number={props.number}
                        name={props.name}
                        dateStart={props.dateStart}
                        dateEnd={props.dateEnd}
                        handleRemove={handleRemove}
                    />
                </td>
            </tr>
        );
    };


export default memo(TrackItemLight);
