import React, {useEffect, useState} from 'react';
import {Navbar} from "react-bootstrap";
import Singleton from '../Note/components/Singleton';
import {getFormattedDate} from '../../services/TimeHelper';

const MetricWidget = () => {
    const dict              = {
        'cron:sendDailyReport:before': 'с:dr:b',
        'cron:sendDailyReport:after':  'с:dr:a',
        'http:sendDailyReport:before': 'h:dr:b',
        'http:sendDailyReport:after':  'h:dr:a',
        'cron:everyMinute':            'c:ping'
    };
    const [state, setState] = useState([]);

    /**
     * @type ReportApi
     */
    const reportApi = Singleton.getInstance('common-report-api');

    const callMetrics = () => {
        reportApi.getMetrics({}, {}, (result) => {
            const data = [];
            for (const key in result.data) {
                data.push({
                    'key':   key,
                    'name':  dict.hasOwnProperty(key) ? dict[key] : key,
                    'value': result.data[key]
                });
            }
            setState(data);
        });
    };

    useEffect(() => {
        callMetrics(); // Initial call
        const intervalId = setInterval(callMetrics, 3000); // Call every second

        return () => clearInterval(intervalId); // Cleanup interval on unmount
    }, []); // Empty dependency array means this runs once on mount

    const text = state.map((item) => `${item.name}: ${item.value ? getFormattedDate(Number(item.value) * 1000, true) : '[none]'}`);

    return (
        <Navbar.Text style={{display: 'inline-block', gap: 3, paddingLeft: '10px'}}>
            {text.join(', ')}
        </Navbar.Text>
    );
};

export default MetricWidget;