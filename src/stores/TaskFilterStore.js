import {makeAutoObservable} from "mobx";
import StatisticsWorkSpaceLoader from '../scripts/StatisticsWorkSpaceLoader';

export default class TaskFilterStore {
    static __instance;

    static getInstance() {
        if (!this.__instance) {
            this.__instance = new this();
        }

        return this.__instance;
    }

    constructor() {
        this.types                 = {
            useful:    false,
            useless:   false,
            undefined: false
        };
        this.dateFrom              = null;
        this.dateTo                = null;
        this.searchText            = null;
        this.activeSearchOperators = false;
        this.limit                 = 'all';

        makeAutoObservable(this);
    }

    setType(type, state) {
        this.types[type] = state;
    }

    setDateFrom(date) {
        if (date instanceof Object) {
            this.dateFrom = date;
        } else {
            this.dateFrom = null;
        }
    }

    setDateTo(date) {
        if (date instanceof Object) {
            this.dateTo = date;
        } else {
            this.dateTo = null;
        }
    }

    setSearchText(searchText) {
        this.searchText = searchText.trim() ? searchText.trim() : null;
    }

    setActiveSearchOperators(activeSearchOperators) {
        this.activeSearchOperators = activeSearchOperators;
    }

    setLimit(limit) {
        this.limit = limit;
    }

    filter() {
        const filter = {...this};

        if (filter.limit === 'all') {
            filter.limit = null;
        }

        if (filter.dateFrom) {
            filter.dateFrom = filter.dateFrom.toDate().toUTCString();
        }

        if (filter.dateTo) {
            filter.dateTo = filter.dateTo.toDate().toUTCString();
        }

        filter.activeSearchOperators = filter.activeSearchOperators ? '1' : null;

        StatisticsWorkSpaceLoader.getInstance().filter(filter);
    }
}
