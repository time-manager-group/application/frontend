import {makeAutoObservable} from "mobx";
import Report from "../services/Report";

export default class ReportModalStore {
    static __instance;

    static getInstance() {
        if (!this.__instance) {
            this.__instance = new this();
        }

        return this.__instance;
    }

    /**
     * @private
     */
    constructor() {
        this.isShow = false;
        this.isRequiredStep = false;
        this.isReady = false;
        this.isPermissionCreateReport = false;
        this.reportName = '';
        this.callableCreate = null;

        makeAutoObservable(this);

        this.__api = new Report();
        this.__initModal = this.__initModal.bind(this);
    }

    show(requiredStep = false, callableCreate = null) {
        this.isRequiredStep = requiredStep;
        this.callableCreate = callableCreate;
        this.isShow = true;
        this.__initForm();
    }

    reportNameChange(name) {
        this.reportName = name.trim().replace(/\s+/gui, ' ');
        this.isPermissionCreateReport = !!this.reportName;
    }

    close() {
        if (!this.isRequiredStep) {
            this.isShow = false;
        }
    }

    make() {
        this.isShow = false;

        if (this.callableCreate) {
            this.callableCreate(this.reportName);
        }
    }

    __initForm() {
        this.isReady = false;
        const $this = this;

        this.__api.getNameForCreate(this.__initModal,() => {
            $this.__initModal('');
        });
    }

    __initModal(name) {
        this.reportNameChange(name);
        this.isReady = true;
    }
}
