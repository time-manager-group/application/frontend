import {makeAutoObservable} from "mobx";
import moment from "moment/moment";

export default class EventToasterStore {
    static __instance;

    static getInstance() {
        if (!this.__instance) {
            this.__instance = new this();
        }

        return this.__instance;
    }

    constructor() {
        this.events = [];
        this.isAutoRemove = true;

        makeAutoObservable(this);
    }

    /**
     *
     * @param isAutoRemove {boolean}
     */
    autoRemove(isAutoRemove = true) {
        this.isAutoRemove = isAutoRemove;
    }

    /**
     * Добавить уведомление.
     *
     * @param value {{
     *     title:string,
     *     text:string,
     *     timeTitle?:string|null,
     *     isAutoRemove?:boolean|null
     * }}
     */
    appendEvent(value) {
        if (!value.hasOwnProperty('timeTitle') || !value.timeTitle) {
            value.timeTitle = moment().format('HH:mm:ss');
        }

        if (!value.hasOwnProperty('isAutoRemove') || !value.isAutoRemove) {
            value.isAutoRemove = this.isAutoRemove;
        }


        this.events.push(value);
    }

    /**
     * Удалить уведомление.
     *
     * @param item {*}
     */
    removeEvent(item) {
        const index = this.events.indexOf(item);
        if (index !== -1) {
            this.events.splice(index, 1);
        }
    }
}