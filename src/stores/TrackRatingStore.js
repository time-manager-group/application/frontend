import {makeAutoObservable} from "mobx";
import TimeFormat from '../services/TimeFormat';

export default class TrackRatingStore {
    static __instance;

    static getInstance() {
        if (!this.__instance) {
            this.__instance = new this();
        }

        return this.__instance;
    }

    constructor() {
        this.items      = [];
        this.clickLabel = null;

        makeAutoObservable(this);
    }

    setClickLabel(clickLabel) {
        this.clickLabel = clickLabel ? clickLabel : null;
    }

    /**
     *
     * @param items {[{
     *     name: string,
     *     all_seconds: number,
     *     type: string
     * }]}
     */
    fillItems(items) {
        this.items      = [];
        this.clickLabel = null;

        for (const item of items) {
            this.items.push({
                name:       item.name,
                allSeconds: +item.all_seconds,
                timeFormat: new TimeFormat(item.all_seconds).format(),
                type:       item.type
            });
        }
    }
}
