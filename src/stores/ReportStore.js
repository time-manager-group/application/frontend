import {makeAutoObservable} from "mobx";

export default class ReportStore {
    static __instance;

    static getInstance() {
        if (!this.__instance) {
            this.__instance = new this();
        }

        return this.__instance;
    }

    constructor() {
        this.items = [];
        this.selectedId = '';

        makeAutoObservable(this);
    }

    /**
     *
     * @param id {number|string}
     */
    onSelect(id) {
        this.selectedId = id + '';
    }

    /**
     *
     * @returns {number}
     */
    getSelectedReportId() {
        return parseInt(this.selectedId);
    }

    /**
     *
     * @param item {{
     *     name: string,
     *     id: number|string
     * }}
     */
    createAndActiveItem(item) {
        this.items.push({
            name: item.name,
            id: item.id + '',
        });

        this.onSelect(item.id);
    }

    /**
     *
     * @param items {[{
     *     name: string,
     *     id: number|string
     * }]}
     */
    fillItems(items) {
        this.items = [];

        for (const item of items) {
            this.items.push({
                name: item.name,
                id: item.id + '',
            });
        }
    }
}
