import {makeAutoObservable} from "mobx";
import TrackGrouper from "../services/TrackGrouper";
import TimeDropper from "../services/TimeDropper";

export default class ReportTaskStore {
    static __instances = new Map();
    static activeTaskName = null;

    static getInstances() {
        return this.__instances;
    }

    static getInstance(type) {
        if (!this.__instances.has(type)) {
            this.__instances.set(type, new this());
        }

        return this.__instances.get(type);
    }

    constructor() {
        this.tasks = [];
        this.sumTypeTime = 0;
        this.isActive = false;

        makeAutoObservable(this);

        this.tasksState = [];
    }

    static fillAllTracks(items) {
        const groups = new Map();
        this.activeTaskName = null;

        for (const item of items) {
            const type = item.type;

            if (!groups.has(type)) {
                groups.set(type,
                    {
                        sumTypeTime: 0,
                        isActive: false,
                        activeTaskName: null,
                        tasks: []
                    });
            }

            const group = groups.get(type);

            // заполнение задач и отчасти групп
            const groupElement = TrackGrouper.getGroupElement(group.tasks, item);

            if (item.isActive) {
                group.isActive = true;
                groupElement.isOpen = true;
                groupElement.isActive = true;

                this.activeTaskName = item.name;
            }

            const taskSumTime = new TimeDropper(item.dateStart, item.dateEnd).getDiffSeconds();

            groupElement.sumTime += taskSumTime;
            group.sumTypeTime += taskSumTime;

            //заполнение треками.
            groupElement.tracks.push(TrackGrouper.cloneTrack(item));
        }

        // заполнение инстансев
        for (const [type, instance] of this.getInstances()) {
            if (groups.has(type)) {
                const group = groups.get(type);
                instance.fillTasks(group.tasks);
                instance.setSumTypeTime(group.sumTypeTime);
                instance.setActive(group.isActive);
            } else {
                instance.fillTasks([]);
                instance.setSumTypeTime(0);
                instance.setActive(false);
            }
        }
    }

    fillTasks(tasks) {
        // пересвоение состояния открытости/закрытости аккордеона.
        for (const task of tasks) {
            const currentTask = TrackGrouper.getTaskByName(this.tasksState, task.name);

            if (currentTask !== null) {
                task.isOpen = currentTask.isOpen;
            }
        }

        this.tasks = tasks;
    }

    setSumTypeTime(sumTypeTime) {
        this.sumTypeTime = sumTypeTime;
    }

    setActive(isActive) {
        this.isActive = isActive;
    }

    setOpenTask(key, isOpen) {
        this.tasks[key].isOpen = isOpen;

        const taskName = this.tasks[key].name;

        const taskState = TrackGrouper.getTaskByName(this.tasksState, taskName);

        if (taskState !== null) {
            taskState.isOpen = isOpen;
        } else {
            this.tasksState.push({
                name: taskName,
                isOpen: isOpen,
            });
        }
    }

    static setIsOpenTaskStateByNameAllTypes(isOpen) {
        for (const instance of this.getInstances()) {
            for (const taskState of instance[1].tasksState) {
                taskState.isOpen = isOpen;
            }
        }
    }

    static isActiveTaskByName(taskName) {
        return this.activeTaskName !== null ? TrackGrouper.equalsTaskName(this.activeTaskName, taskName) : false;
    }

    setIsOpenTaskStateByName(taskName, isOpen) {
        const taskState = TrackGrouper.getTaskByName(this.tasksState, taskName);

        if (taskState !== null) {
            taskState.isOpen = isOpen;
        } else {
            this.tasksState.push({
                name: taskName,
                isOpen: isOpen,
            });
        }
    }

    getTaskStateIsOpen(taskName, defaultValue = null) {
        const taskState = TrackGrouper.getTaskByName(this.tasksState, taskName);

        if (taskState !== null) {
            return taskState.isOpen;
        }

        return defaultValue;
    }

    static removeAllTasksState() {
        for (const instance of this.getInstances()) {
            instance[1].tasksState = [];
        }
    }
}
