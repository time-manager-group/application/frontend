import {makeAutoObservable} from "mobx";

export default class TrackWorkSpacePreloaderStore {
    static __instance;

    static getInstance() {
        if (!this.__instance) {
            this.__instance = new this();
        }

        return this.__instance;
    }

    /**
     * @private
     */
    constructor() {
        this.isShow = false;

        makeAutoObservable(this);
    }

    show(isShow = true) {
        this.isShow = isShow;
    }
}
