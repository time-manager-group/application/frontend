import {makeAutoObservable} from "mobx";
import {configure} from "mobx"
import TrackGrouper from "../services/TrackGrouper";
import Report from "../services/Report";
import EventToasterStore from "./EventToasterStore";

configure({
    enforceActions: "never",
})

export default class TrackStore {
    static __instance;

    static getInstance() {
        if (!this.__instance) {
            this.__instance = new this();
        }

        return this.__instance;
    }

    constructor() {
        this.items = [];
        this.isShow = true;
        this.__fakeLastId = -1;

        this.__api = new Report();

        this.taskTemplateList = [
            {
                name: 'Отдых',
                type: 'useless',
            },
            {
                name: 'Обед',
                type: 'useless',
            },
            {
                name: 'Дейлик',
                type: 'undefined',
            },
            {
                name: 'Ужин',
                type: 'useless',
            },
            {
                name: 'Планирование',
                type: 'undefined',
            },
        ];

        makeAutoObservable(this);
    }

    /**
     *
     * @param isShow {boolean}
     */
    show(isShow = true) {
        this.isShow = isShow;
    }

    /**
     *
     * @param items
     */
    fillTracks(items) {
        this.items = [];

        for (const item of items) {
            this.items.push(TrackGrouper.cloneTrack(item));
        }
    }

    /**
     * @param reportId {number}
     * @param value {{
     *      value: string,
     *      type: string,
     *      }}
     */
    appendTrack(reportId, value) {
        const lastTrack = this.items.length > 0 ? this.items[this.items.length - 1] : null;

        const newTrack = {
            id: this.__fakeLastId,
            name: value.value,
            dateStart: new Date(),
            dateEnd: new Date(),
            type: value.type,
            isActive: true
        };

        if (lastTrack !== null) {
            if (!this.__isFakeId(lastTrack.id)) {
                lastTrack.isActive = false;

                this.__fakeLastId--;
                this.items.push(newTrack);

                const $this = this;

                this.__api.updateTrack(reportId, lastTrack.id, lastTrack, () => {
                        $this.__createTrack(reportId, newTrack);
                    },
                    () => {
                        lastTrack.isActive = true;
                    });

            }
        } else {
            this.__fakeLastId--;
            this.items.push(newTrack);
            this.__createTrack(reportId, newTrack);
        }
    }

    /**
     * @param reportId {number}
     * @param newTrack {{
     *      id: number
     *      name: string,
     *      dateStart: Date,
     *      dateEnd: Date,
     *      type: string,
     *      isActive: boolean
     *      }}
     * @private
     */
    __createTrack(reportId, newTrack) {
        const $this = this;

        this.__api.createTrack(reportId, newTrack, (data) => {
            // noinspection JSCheckFunctionSignatures
            $this.__updateTrack(newTrack.id, {
                id: data.id
            });
        }, () => {
            $this.__removeTrack(newTrack.id);
        });
    }

    /**
     * @param reportId {number}
     * @param id {number}
     * @param value {{
     *      name: string,
     *      dateStart: Date,
     *      dateEnd: Date,
     *      type: string,
     *      isActive: boolean
     *      }}
     * @param sendApi {boolean}
     * @param handleFail {$Call}
     */
    updateTrack(reportId, id, value,
                sendApi = false,
                handleFail = null
    ) {
        // noinspection JSCheckFunctionSignatures
        this.__updateTrack(id, value);

        const $this = this;

        if (sendApi) {
            const track = this.__getTrackById(id);

            if (!this.__isFakeId(track.id)) {
                this.__api.updateTrack(reportId, track.id, track, null, () => {
                    handleFail($this.__getTrackById(track.id));
                });
            } else {
                handleFail($this.__getTrackById(track.id));
            }
        }
    }

    /**
     *
     * @param id {number}
     * @param value {{
     *      id: number,
     *      name: string,
     *      dateStart: Date,
     *      dateEnd: Date,
     *      type: string,
     *      isActive: boolean
     *      }}
     */
    __updateTrack(id, value) {
        for (const item of this.items) {
            if (item.id === id) {
                for (const propertyName in value) {
                    item[propertyName] = value[propertyName];
                }

                break;
            }
        }
    }

    /**
     *
     * @param id
     * @returns {null|*}
     * @private
     */
    __getTrackById(id) {
        for (const item of this.items) {
            if (item.id === id) {
                return item;
            }
        }

        return null;
    }

    /**
     * @param reportId {number}
     * @param id {number}
     * @param handleSuccess {$Call}
     * @param handleFail {$Call}
     */
    removeTrack(reportId, id, handleSuccess, handleFail) {
        if (!this.__isFakeId(id)) {
            this.__api.deleteTrack(reportId, id, () => {
                    this.__removeTrack(id);
                    handleSuccess();
                },
                () => {
                    handleFail();
                });
        }
    }

    /**
     *
     * @param id {number}
     * @private
     */
    __removeTrack(id) {
        let key = 0;

        for (const item of this.items) {
            if (item.id === id) {
                this.items.splice(key, 1);

                break;
            }

            key++;
        }
    }

    /**
     * Проверка на фейковый id.
     *
     * @param id  {number}
     * @returns {boolean}
     * @private
     */
    __isFakeId(id) {
        if (id < 0) {
            this.__initQueryBlockEvent();

            return true;
        }

        return false;
    }

    /**
     * Выбросить блокировку в тостер.
     *
     * @private
     */
    __initQueryBlockEvent() {
        EventToasterStore.getInstance().appendEvent({
            title: 'Блокировка запроса',
            text: 'Запрос был отклонен, так как идентификатор в ожидании присваивания.',
        });
    }
}
