import {makeAutoObservable} from "mobx";
import TimeFormat from '../services/TimeFormat';

export default class TableSumRatingStore {
    static __instance;

    static getInstance() {
        if (!this.__instance) {
            this.__instance = new this();
        }

        return this.__instance;
    }

    constructor() {
        this.items = [];

        makeAutoObservable(this);
    }

    /**
     *
     * @param items {[{
     *     tasks_count: number,
     *     all_seconds: number,
     *     type: string
     * }]}
     */
    fillItems(items) {
        this.items = [];

        let allCount   = 0;
        let allSeconds = 0;

        for (const item of items) {
            allCount += +item.tasks_count;
            allSeconds += +item.all_seconds;
            this.items.push({
                type:       item.type,
                tasksCount: +item.tasks_count,
                timeSum:    new TimeFormat(item.all_seconds).format()
            });
        }

        this.items.push({
            type:       null,
            tasksCount: allCount,
            timeSum:    new TimeFormat(allSeconds).format()
        });
    }
}
