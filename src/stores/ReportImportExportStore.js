import {makeAutoObservable} from "mobx";
import Report from '../services/Report';
import {downloadFile} from '../services/FileOperator';

export default class ReportImportExportStore {
    static __instance;

    static getInstance() {
        if (!this.__instance) {
            this.__instance = new this();
        }

        return this.__instance;
    }

    constructor() {
        this.__api    = new Report();
        this.fillPage = this.fillPage.bind(this);

        this.items      = [];
        this.pagination = {
            page:     1,
            per_page: 15,
            count:    null
        };

        makeAutoObservable(this);
    }

    loadPage(page) {
        this.__api.getReportList(this.fillPage, null,
            {
                filter: null,
                sort:   {
                    oder_by:        null,
                    oder_direction: 'desc'
                },
                pagination:
                        {
                            page: page,
                            per_page:
                                  this.pagination.per_page
                        }
            }
        );
    }

    /**
     *
     * @param data {
     * list:{[
     * {
     *     name: string,
     *     id: number,
     * }
     * ]},
     * pagination:{
     * page:number,
     * per_page:number
     * }}
     */
    fillPage(data) {
        this.items = [];

        this.pagination = data.pagination;

        for (const item of data.list) {
            this.items.push({
                id:   item.id,
                name: item.name
            });
        }
    }

    export(reportId) {
        downloadFile(this.__api.getExportFileUrl(reportId));
    }
}
