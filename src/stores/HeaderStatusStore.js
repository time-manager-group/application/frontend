import {makeAutoObservable} from "mobx";


export default class HeaderStatusStore {
    static __instance;

    static getInstance() {
        if (!this.__instance) {
            this.__instance = new this();
        }

        return this.__instance;
    }

    /**
     * @private
     */
    constructor() {
        this.status = null;

        makeAutoObservable(this);
    }

    setActive() {
        this.__setStatus('active');
    }

    setEmpty() {
        this.__setStatus('empty');
    }

    setStopped() {
        this.__setStatus('stopped');
    }

    unset() {
        this.__setStatus(null);
    }

    /**
     *
     * @param status {string|null}
     * @private
     */
    __setStatus(status) {
        if (this.status !== status) {
            this.status = status;
        }
    }
}
