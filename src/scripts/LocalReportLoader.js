import Report from "../services/Report";
import ReportStore from "../stores/ReportStore";
import TrackWorkSpacePreloaderStore from "../stores/TrackWorkSpacePreloaderStore";
import TrackStore from "../stores/TrackStore";
import ReportTaskStore from "../stores/ReportTaskStore";
import EventToasterStore from "../stores/EventToasterStore";

/**
 * Сценарий создания нового отчета локально.
 */
export default class LocalReportLoader {
    constructor() {
        this.__api = new Report();

        this.makeNewReport = this.makeNewReport.bind(this);
        this.loadReport = this.loadReport.bind(this);
        this.loadActiveReport = this.loadActiveReport.bind(this);
        this.__makeNewReportSuccess = this.__makeNewReportSuccess.bind(this);
        this.__fillReportList = this.__fillReportList.bind(this);
        this.__fillTrackList = this.__fillTrackList.bind(this);
        this.__ready = this.__ready.bind(this);
    }

    loadActiveReport() {
        TrackStore.getInstance().show(false);
        TrackWorkSpacePreloaderStore.getInstance().show(true);

        const $this = this;

        this.__api.getActive((id) => {
            const reportId = parseInt(id);

            if (reportId) {
                if (reportId !== ReportStore.getInstance().getSelectedReportId()) {
                    $this.__currentReportId = reportId;
                    $this.__api.getTrackList(
                        $this.__currentReportId,
                        this.__fillTrackList,
                        this.__ready);
                } else {
                    EventToasterStore.getInstance().appendEvent({
                        title: 'Активные отчеты',
                        text: 'Текущий отчет является активным.',
                    });
                }
            } else {
                EventToasterStore.getInstance().appendEvent({
                    title: 'Активные отчеты',
                    text: 'Нет активных отчетов.',
                });
            }

            this.__ready();
        }, this.__ready);
    }

    /**
     *
     * @param id {number|string}
     * @private
     */
    loadReport(id) {
        if (parseInt(id) !== ReportStore.getInstance().getSelectedReportId()) {
            TrackStore.getInstance().show(false);
            TrackWorkSpacePreloaderStore.getInstance().show(true);
            this.__currentReportId = id;

            this.__api.getTrackList(
                this.__currentReportId,
                this.__fillTrackList,
                this.__ready);
        }
    }

    /**
     *
     * @param name {string}
     * @private
     */
    makeNewReport(name) {
        this.__currentReportId = null;

        TrackStore.getInstance().show(false);
        TrackWorkSpacePreloaderStore.getInstance().show(true);
        this.__api.create(
            name,
            this.__makeNewReportSuccess,
            this.__ready);
    }

    /**
     *
     * @param id
     * @private
     */
    __makeNewReportSuccess(id) {
        this.__currentReportId = id;
        this.__api.getReportList(
            this.__fillReportList,
            this.__ready);
    }

    /**
     *
     * @param data
     * @private
     */
    __fillReportList(data) {
        ReportStore.getInstance().fillItems(data.list);
        this.__api.getTrackList(
            this.__currentReportId,
            this.__fillTrackList,
            this.__ready);
    }

    /**
     *
     * @param items
     * @private
     */
    __fillTrackList(items) {
        ReportTaskStore.removeAllTasksState();
        TrackStore.getInstance().fillTracks(items);
        ReportTaskStore.fillAllTracks(items);
        ReportStore.getInstance().onSelect(this.__currentReportId);

        this.__ready();
    }

    /**
     *
     * @private
     */
    __ready() {
        TrackWorkSpacePreloaderStore.getInstance().show(false);
        TrackStore.getInstance().show(true);
    }

}
