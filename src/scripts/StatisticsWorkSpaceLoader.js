import Statistics from '../services/Statistics';
import TrackRatingStore from '../stores/TrackRatingStore';
import TableSumRatingStore from '../stores/TableSumRatingStore';

/**
 * Сценарий запуска рабочей области.
 */
export default class StatisticsWorkSpaceLoader {
    static __instance;

    static getInstance() {
        if (!this.__instance) {
            this.__instance = new this();
        }

        return this.__instance;
    }

    constructor() {
        this.__api                 = new Statistics();
        this.__fillTrackRatingList = this.__fillTrackRatingList.bind(this);
        this.__fillTableSumAllList = this.__fillTableSumAllList.bind(this);
    }

    /**
     *
     */
    load() {
        //  EventToasterStore.getInstance().autoRemove(false);
        //    PreloaderStore.getInstance().show(true);
        //     ApplicationStore.getInstance().show(false);
        if (!this.isLoaded) {
            this.__api.getTrackRating(this.__fillTrackRatingList);
            this.__api.getSumAll(this.__fillTableSumAllList);
            this.isLoaded = true;
        }
    }

    filter(filter) {
        this.__api.getTrackRating(this.__fillTrackRatingList, null, filter);
        this.__api.getSumAll(this.__fillTableSumAllList, null, filter);
    }

    /**
     *
     * @param items
     * @private
     */
    __fillTrackRatingList(items) {
        TrackRatingStore.getInstance().fillItems(items);

        //  this.__ready();
    }

    /**
     *
     * @param items
     * @private
     */
    __fillTableSumAllList(items) {
        TableSumRatingStore.getInstance().fillItems(items);

        //     this.__ready();
    }

    /**
     *
     * @private
     */
    __ready() {
        //    EventToasterStore.getInstance().autoRemove(true);
        //     PreloaderStore.getInstance().show(false);
    }
}
