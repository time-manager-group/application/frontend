import PreloaderStore from "../stores/PreloaderStore";
import ApplicationStore from "../stores/ApplicationStore";
import Report from "../services/Report";
import ReportModalStore from "../stores/ReportModalStore";
import ReportStore from "../stores/ReportStore";
import EventToasterStore from "../stores/EventToasterStore";
import ReportTaskStore from "../stores/ReportTaskStore";
import TrackStore from "../stores/TrackStore";

/**
 * Сценарий запуска рабочей области.
 */
export default class WorkSpaceLoader {
    constructor() {
        this.__api = new Report();

        this.__checkLastReportId = this.__checkLastReportId.bind(this);
        this.__checkActiveReportId = this.__checkActiveReportId.bind(this);
        this.__makeNewReport = this.__makeNewReport.bind(this);
        this.__makeNewReportSuccess = this.__makeNewReportSuccess.bind(this);
        this.__fillReportList = this.__fillReportList.bind(this);
        this.__fillTrackList = this.__fillTrackList.bind(this);
    }

    /**
     *
     */
    load() {
        EventToasterStore.getInstance().autoRemove(false);
        PreloaderStore.getInstance().show(true);
        ApplicationStore.getInstance().show(false);

        this.__api.getLast(this.__checkLastReportId);
    }

    /**
     *
     * @param id
     * @private
     */
    __checkLastReportId(id) {
        if (!id) {
            this.__api.getActive(this.__checkActiveReportId);
        } else {
            this.__initWorkSpace(id);
        }
    }

    /**
     *
     * @param id
     * @private
     */
    __checkActiveReportId(id) {
        if (!id) {
            this.__initRequireCreateReportModal();
        } else {
            this.__initWorkSpace(id);
        }
    }

    /**
     *
     * @private
     */
    __initRequireCreateReportModal() {
        PreloaderStore.getInstance().show(false);
        ReportModalStore.getInstance().show(true, this.__makeNewReport);
    }

    /**
     *
     * @param name
     * @private
     */
    __makeNewReport(name) {
        PreloaderStore.getInstance().show(true);
        this.__reportNewName = name;
        this.__api.create(name, this.__makeNewReportSuccess)
    }

    /**
     *
     * @param id
     * @private
     */
    __makeNewReportSuccess(id) {
       this.__initWorkSpace(id);
    }

    /**
     *
     * @param id
     * @private
     */
    __initWorkSpace(id) {
        this.__currentReportId = id;
        this.__api.getReportList(this.__fillReportList);
    }

    /**
     *
     * @param data
     * @private
     */
    __fillReportList(data) {
        ReportStore.getInstance().fillItems(data.list);
        ReportStore.getInstance().onSelect(this.__currentReportId);
        this.__api.getTrackList(this.__currentReportId, this.__fillTrackList);
    }

    /**
     *
     * @param items
     * @private
     */
    __fillTrackList(items) {
        ReportTaskStore.removeAllTasksState();
        TrackStore.getInstance().fillTracks(items);
        ReportTaskStore.fillAllTracks(items);

        this.__ready();
    }

    /**
     *
     * @private
     */
    __ready() {
        EventToasterStore.getInstance().autoRemove(true);
        PreloaderStore.getInstance().show(false);
        ApplicationStore.getInstance().show(true);
    }
}
